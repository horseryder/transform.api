﻿using Microsoft.EntityFrameworkCore;
using Transform.Entities.Entities;
using Transform.Entities.Mappings;

namespace Transform.Entities
{
    public class TransformContext : DbContext
    {
        public TransformContext(DbContextOptions<TransformContext> options)
            : base(options)
        {
        }

        public DbSet<Organizations> Organizations { get; set; }
        public DbSet<Users> Users { get; set; }
        public DbSet<UserRoles> UserRoles { get; set; }
        public DbSet<AdminPackages> AdminPackages { get; set; }
        public DbSet<AdminGroups> AdminGroups { get; set; }
        public DbSet<UserPackages> UserPackages { get; set; }
        public DbSet<UserOrganizations> UserOrganizations { get; set; }
        public DbSet<Menus> Menus { get; set; }
        public DbSet<UserGroupMenu> UserGroupMenu { get; set; }
        public DbSet<AdminGroupMenu> AdminGroupMenu { get; set; }
        public DbSet<UserEmployees> UserEmployees { get; set; }
        public DbSet<ClientResponse> ClientResponse { get; set; }
        public DbSet<EnquiryForm> EnquiryForm { get; set; }
        public DbSet<UserGroups> UserGroups { get; set; }
        public DbSet<Programs> Programs { get; set; }
        public DbSet<UserClients> UserClients { get; set; }
        public DbSet<Batch> Batch { get; set; }

        public DbSet<UsersAttendance> UsersAttendance { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<Equipments> Equipments { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder
                .ApplyConfiguration(new OrganizationsMapping())
                .ApplyConfiguration(new UsersMapping())
                .ApplyConfiguration(new UserRolesMapping())
                .ApplyConfiguration(new UserPackagesMapping())
                .ApplyConfiguration(new UserOrganizationsMapping())
                .ApplyConfiguration(new UserGroupMenuMapping())
                .ApplyConfiguration(new AdminGroupMenuMapping())
                .ApplyConfiguration(new MenusMapping())
                .ApplyConfiguration(new UserEmployeesMapping())
                .ApplyConfiguration(new ClientResponseMapping())
                .ApplyConfiguration(new EnquiryFormMapping())
                .ApplyConfiguration(new AdminPackagesMapping())
                .ApplyConfiguration(new AdminGroupMapping())
                .ApplyConfiguration(new BatchMapping())
                .ApplyConfiguration(new ProgramMapping())
                .ApplyConfiguration(new UserClientsMapping())
                .ApplyConfiguration(new UsersAttendanceMapping())
                .ApplyConfiguration(new CategoryMapping())
                .ApplyConfiguration(new EquipmentsMapping())
                .ApplyConfiguration(new UserGroupsMapping());

        }

    }
}
