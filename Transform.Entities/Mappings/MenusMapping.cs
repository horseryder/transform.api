﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Transform.Common.Constants;
using Transform.Entities.Entities;

namespace Transform.Entities.Mappings
{
    public class MenusMapping : IEntityTypeConfiguration<Menus>
    {
        public void Configure(EntityTypeBuilder<Menus> builder)
        {
            builder.ToTable(TableConstants.Menus);

            builder.HasKey(t => t.MenuId);
            builder.Property(t => t.MenuName);
            builder.Property(t => t.MenuSequence);
            builder.Property(t => t.RoutePath);
            builder.Property(t => t.Icon);
            builder.Property(t => t.ParentMenuId);
            builder.Property(t => t.IsActive);

            builder.Property(t => t.CreatedBy);
            builder.Property(t => t.CreatedOn);
            builder.Property(t => t.ModifiedBy);
            builder.Property(t => t.ModifiedOn);
        }
    }
}
