﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Transform.Common.Constants;
using Transform.Entities.Entities;

namespace Transform.Entities.Mappings
{
    public class UserEmployeesMapping : IEntityTypeConfiguration<UserEmployees>
    {
        public void Configure(EntityTypeBuilder<UserEmployees> builder)
        {
            builder.ToTable(TableConstants.UserEmployees);

            builder.HasKey(key => key.EmployeeId);
            builder.Property(t => t.UserId);
            builder.Property(t => t.GroupId);
            builder.Property(t => t.RoleId);
            builder.Property(t => t.IsActive);

            builder.Property(t => t.CreatedBy);
            builder.Property(t => t.CreatedOn);
            builder.Property(t => t.ModifiedBy);
            builder.Property(t => t.ModifiedOn);
        }
    }
}
