﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Transform.Common.Constants;
using Transform.Entities.Entities;

namespace Transform.Entities.Mappings
{
    public class EnquiryFormMapping : IEntityTypeConfiguration<EnquiryForm>
    {
        public void Configure(EntityTypeBuilder<EnquiryForm> builder)
        {
            builder.ToTable(TableConstants.EnquiryForm);

            builder.HasKey(key => key.EnquiryFormId);
            builder.Property(t => t.FirstName);
            builder.Property(t => t.LastName);
            builder.Property(t => t.Mobile);
            builder.Property(t => t.Email);
            builder.Property(t => t.Gender);
            builder.Property(t => t.Address);
            builder.Property(t => t.TrailDate);
            builder.Property(t => t.FollowUpDate);
            builder.Property(t => t.ReferBy);
            builder.Property(t => t.PackageId);
            builder.Property(t => t.ResponseId);
            builder.Property(t => t.OrganizationId);
            builder.Property(t => t.IsActive);

            builder.Property(t => t.CreatedBy);
            builder.Property(t => t.CreatedOn);
            builder.Property(t => t.ModifiedBy);
            builder.Property(t => t.ModifiedOn);
        }
    }
}
