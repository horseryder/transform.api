﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Transform.Entities.Entities;

namespace Transform.Entities.Mappings
{
    public class UsersAttendanceMapping : IEntityTypeConfiguration<UsersAttendance>
    {
        public void Configure(EntityTypeBuilder<UsersAttendance> builder)
        {
            builder.ToTable(nameof(UsersAttendance));

            builder.HasKey(key => key.AttendanceId);
            builder.Property(key => key.UserId);
            builder.Property(key => key.OrganizationId);
            builder.Property(t => t.ClockDateTime);
            builder.Property(t => t.IsClockIn);
        }
    }
}
