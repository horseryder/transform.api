﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Transform.Common.Constants;
using Transform.Entities.Entities;

namespace Transform.Entities.Mappings
{
    public class OrganizationsMapping : IEntityTypeConfiguration<Organizations>
    {
        public void Configure(EntityTypeBuilder<Organizations> builder)
        {
            builder.ToTable(TableConstants.Organizations);

            builder.HasKey(key => key.OrganizationId);
            builder.Property(t => t.Name);
            builder.Property(t => t.Address);
            builder.Property(t => t.Logo);
            builder.Property(t => t.IsActive);
            builder.Property(t => t.PackageId);
            builder.Property(t => t.IsMaster);
            builder.Property(t => t.ParentOrganizationId);

            builder.Property(t => t.CreatedBy);
            builder.Property(t => t.CreatedOn);
            builder.Property(t => t.ModifiedBy);
            builder.Property(t => t.ModifiedOn);
        }
    }
}
