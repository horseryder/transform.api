﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Transform.Entities.Entities;

namespace Transform.Entities.Mappings
{
    public class UserGroupMenuMapping : IEntityTypeConfiguration<UserGroupMenu>
    {
        public void Configure(EntityTypeBuilder<UserGroupMenu> builder)
        {
            builder.ToTable(nameof(UserGroupMenu));

            builder.HasKey(key => key.GroupMenuId);
            builder.Property(t => t.GroupId);
            builder.Property(t => t.MenuId);
            builder.Property(t => t.OrganizationId);
            builder.Property(t => t.IsActive);

            builder.Property(t => t.CreatedBy);
            builder.Property(t => t.CreatedOn);
            builder.Property(t => t.ModifiedBy);
            builder.Property(t => t.ModifiedOn);
        }
    }
}
