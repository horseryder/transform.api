﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Transform.Entities.Entities;

namespace Transform.Entities.Mappings
{
    public class CategoryMapping : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.ToTable(nameof(Category));

            builder.HasKey(key => key.CategoryId);
            builder.Property(t => t.Name);
            builder.Property(t => t.Description);
            builder.Property(t => t.OrderNumber);
            builder.Property(t => t.CategoryType);
            builder.Property(t => t.OrganizationId);
            builder.Property(t => t.IsActive);

            builder.Property(t => t.CreatedBy);
            builder.Property(t => t.CreatedOn);
            builder.Property(t => t.ModifiedBy);
            builder.Property(t => t.ModifiedOn);
        }
    }
}
