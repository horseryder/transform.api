﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Transform.Common.Constants;
using Transform.Entities.Entities;

namespace Transform.Entities.Mappings
{
    public class EquipmentsMapping : IEntityTypeConfiguration<Equipments>
    {
        public void Configure(EntityTypeBuilder<Equipments> builder)
        {
            builder.ToTable(nameof(Equipments));

            builder.HasKey(key => key.EquipmentId);
            builder.Property(t => t.Name);
            builder.Property(t => t.Description);
            builder.Property(t => t.Price);
            builder.Property(t => t.InStock);
            builder.Property(t => t.CategoryId);
            builder.Property(t => t.OrganizationId);
            builder.Property(t => t.IsActive);

            builder.Property(t => t.CreatedBy);
            builder.Property(t => t.CreatedOn);
            builder.Property(t => t.ModifiedBy);
            builder.Property(t => t.ModifiedOn);
        }
    }
}
