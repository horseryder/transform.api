﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Transform.Common.Constants;
using Transform.Entities.Entities;

namespace Transform.Entities.Mappings
{
    public class UsersMapping : IEntityTypeConfiguration<Users>
    {
        public void Configure(EntityTypeBuilder<Users> builder)
        {
            builder.ToTable(TableConstants.Users);

            builder.HasKey(t => t.UserId);
            builder.Property(t => t.FirstName);
            builder.Property(t => t.LastName);
            builder.Property(t => t.Mobile);
            builder.Property(t => t.Email);
            builder.Property(t => t.Password);
            builder.Property(t => t.IsMaster);
            builder.Property(t => t.OrganizationId);
            builder.Property(t => t.ProfilePic);
            builder.Property(t => t.Gender);
            builder.Property(t => t.Address);
            builder.Property(t => t.IsActive);

            builder.Property(t => t.CreatedBy);
            builder.Property(t => t.CreatedOn);
            builder.Property(t => t.ModifiedBy);
            builder.Property(t => t.ModifiedOn);
        }
    }
}
