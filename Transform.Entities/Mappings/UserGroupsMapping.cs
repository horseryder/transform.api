﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Transform.Entities.Entities;


namespace Transform.Entities.Mappings
{
    class UserGroupsMapping : IEntityTypeConfiguration<UserGroups>
    {
        public void Configure(EntityTypeBuilder<UserGroups> builder)
        {
            builder.ToTable(nameof(UserGroups));

            builder.HasKey(key => key.GroupId);
            builder.Property(t => t.GroupName);
            builder.Property(t => t.IsActive);
            builder.Property(t => t.OrganizationId);

            builder.Property(t => t.CreatedBy);
            builder.Property(t => t.CreatedOn);
            builder.Property(t => t.ModifiedBy);
            builder.Property(t => t.ModifiedOn);
        }
    }
}
