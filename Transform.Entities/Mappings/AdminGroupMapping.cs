﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Transform.Entities.Entities;

namespace Transform.Entities.Mappings
{
    public class AdminGroupMapping : IEntityTypeConfiguration<AdminGroups>
    {
        public void Configure(EntityTypeBuilder<AdminGroups> builder)
        {
            builder.ToTable(nameof(AdminGroups));

            builder.HasKey(key => key.GroupId);
            builder.Property(t => t.GroupName);
            builder.Property(t => t.IsActive);

            builder.Property(t => t.CreatedBy);
            builder.Property(t => t.CreatedOn);
            builder.Property(t => t.ModifiedBy);
            builder.Property(t => t.ModifiedOn);
        }
    }
}
