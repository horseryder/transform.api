﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Transform.Entities.Entities;

namespace Transform.Entities.Mappings
{
    class AdminGroupMenuMapping : IEntityTypeConfiguration<AdminGroupMenu>
    {
        public void Configure(EntityTypeBuilder<AdminGroupMenu> builder)
        {
            builder.ToTable(nameof(AdminGroupMenu));

            builder.HasKey(key => key.GroupMenuId);
            builder.Property(t => t.GroupId);
            builder.Property(t => t.MenuId);
            builder.Property(t => t.IsActive);

            builder.Property(t => t.CreatedBy);
            builder.Property(t => t.CreatedOn);
            builder.Property(t => t.ModifiedBy);
            builder.Property(t => t.ModifiedOn);
        }
    }
}
