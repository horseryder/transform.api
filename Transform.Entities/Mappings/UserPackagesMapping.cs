﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Transform.Common.Constants;
using Transform.Entities.Entities;

namespace Transform.Entities.Mappings
{
    public class UserPackagesMapping : IEntityTypeConfiguration<UserPackages>
    {
        public void Configure(EntityTypeBuilder<UserPackages> builder)
        {
            builder.ToTable(TableConstants.UserPackages);

            builder.HasKey(key => key.PackageId);
            builder.Property(t => t.PackageName);
            builder.Property(t => t.OrganizationId);
            builder.Property(t => t.PackagePrice);
            builder.Property(t => t.Duration);
            builder.Property(t => t.Description);
            builder.Property(t => t.IsActive);
            builder.Property(t => t.IsArchive);

            builder.Property(t => t.CreatedBy);
            builder.Property(t => t.CreatedOn);
            builder.Property(t => t.ModifiedBy);
            builder.Property(t => t.ModifiedOn);
        }
    }
}
