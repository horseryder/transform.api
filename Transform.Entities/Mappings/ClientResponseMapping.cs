﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Transform.Common.Constants;
using Transform.Entities.Entities;

namespace Transform.Entities.Mappings
{
    public class ClientResponseMapping : IEntityTypeConfiguration<ClientResponse>
    {
        public void Configure(EntityTypeBuilder<ClientResponse> builder)
        {
            builder.ToTable(TableConstants.ClientResponse);

            builder.HasKey(key => key.ResponseId);
            builder.Property(t => t.ResponseName);
            builder.Property(t => t.IsActive);
        }
    }
}
