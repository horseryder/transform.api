﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Transform.Common.Constants;
using Transform.Entities.Entities;

namespace Transform.Entities.Mappings
{
    public class UserClientsMapping : IEntityTypeConfiguration<UserClients>
    {
        public void Configure(EntityTypeBuilder<UserClients> builder)
        {
            builder.ToTable(TableConstants.UserClients);

            builder.HasKey(key => key.ClientId);
            builder.Property(t => t.UserId);
            builder.Property(t => t.PackageId);
            builder.Property(t => t.BatchId);
            builder.Property(t => t.ProgramId);
            builder.Property(t => t.PackagePrice);
            builder.Property(t => t.Discount);
            builder.Property(t => t.TotalPrice);
            builder.Property(t => t.AmountPaid);
            builder.Property(t => t.StartDate);
            builder.Property(t => t.EndDate);
            builder.Property(t => t.PaymentCompleted);

            builder.Property(t => t.CreatedBy);
            builder.Property(t => t.CreatedOn);
            builder.Property(t => t.ModifiedBy);
            builder.Property(t => t.ModifiedOn);
        }
    }
}
