﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Transform.Common.Constants;
using Transform.Entities.Entities;

namespace Transform.Entities.Mappings
{
    public class AdminPackagesMapping : IEntityTypeConfiguration<AdminPackages>
    {
        public void Configure(EntityTypeBuilder<AdminPackages> builder)
        {
            builder.ToTable(TableConstants.AdminPackages);

            builder.HasKey(key => key.PackageId);
            builder.Property(t => t.PackageName);
            builder.Property(t => t.PackagePrice);
            builder.Property(t => t.Validity);
            builder.Property(t => t.Description);
            builder.Property(t => t.SubBranches);
            builder.Property(t => t.SubBranchCount);
            builder.Property(t => t.IsActive);

            builder.Property(t => t.CreatedBy);
            builder.Property(t => t.CreatedOn);
            builder.Property(t => t.ModifiedBy);
            builder.Property(t => t.ModifiedOn);
        }
    }
}
