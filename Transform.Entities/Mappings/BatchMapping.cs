﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Transform.Common.Constants;
using Transform.Entities.Entities;

namespace Transform.Entities.Mappings
{
    public class BatchMapping : IEntityTypeConfiguration<Batch>
    {
        public void Configure(EntityTypeBuilder<Batch> builder)
        {
            builder.ToTable(TableConstants.Batch);

            builder.HasKey(key => key.BatchId);
            builder.Property(t => t.BatchName);
            builder.Property(t => t.StartTime);
            builder.Property(t => t.EndTime);
            builder.Property(t => t.OrganizationId);
            builder.Property(t => t.IsActive);

            builder.Property(t => t.CreatedBy);
            builder.Property(t => t.CreatedOn);
            builder.Property(t => t.ModifiedBy);
            builder.Property(t => t.ModifiedOn);
        }
    }
}
