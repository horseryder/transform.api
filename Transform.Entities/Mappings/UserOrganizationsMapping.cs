﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Transform.Common.Constants;
using Transform.Entities.Entities;

namespace Transform.Entities.Mappings
{
    public class UserOrganizationsMapping : IEntityTypeConfiguration<UserOrganizations>
    {
        public void Configure(EntityTypeBuilder<UserOrganizations> builder)
        {
            builder.ToTable(TableConstants.UserOrganizations);

            builder.HasKey(t => t.UserOrganizationId);
            builder.Property(t => t.UserId);
            builder.Property(t => t.OrganizationId);
            builder.Property(t => t.RoleId);
            builder.Property(t => t.GroupId);
            builder.Property(t => t.IsActive);

            builder.Property(t => t.CreatedBy);
            builder.Property(t => t.CreatedOn);
            builder.Property(t => t.ModifiedBy);
            builder.Property(t => t.ModifiedOn);
        }
    }
}
