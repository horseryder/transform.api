﻿namespace Transform.Entities.Entities
{
    public class UserEmployees : Base
    {
        public int EmployeeId { get; set; }
        public int UserId { get; set; }
        public int GroupId { get; set; }
        public int RoleId { get; set; }
        public bool IsActive { get; set; }
    }
}
