﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transform.Entities.Entities
{
    public class AdminGroups : Base
    {
        public int GroupId { get; set; }

        public string GroupName { get; set; }

        public bool IsActive { get; set; }
    }
}
