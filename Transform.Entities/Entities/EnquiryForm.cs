﻿using System;

namespace Transform.Entities.Entities
{
    public class EnquiryForm : Base
    {
        public int EnquiryFormId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public DateTime? TrailDate { get; set; }
        public DateTime? FollowUpDate { get; set; }
        public int? ReferBy { get; set; }
        public int? PackageId { get; set; }
        public int ResponseId { get; set; }  
        public int? OrganizationId { get; set; }
        public char Gender { set; get; }
        public string Address { set; get; }
        public bool IsActive { set; get; }
    }
}
