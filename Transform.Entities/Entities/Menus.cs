﻿namespace Transform.Entities.Entities
{
    public class Menus : Base
    {
        public int MenuId { get; set; }
        public string MenuName { get; set; }
        public int MenuSequence { get; set; }
        public string RoutePath { get; set; }
        public string Icon { get; set; }
        public int? ParentMenuId { get; set; }
        public bool IsActive { get; set; }
    }
}
