﻿namespace Transform.Entities.Entities
{
    public class UserOrganizations : Base
    {
        public int UserOrganizationId { get; set; }
        public int UserId { get; set; }
        public int OrganizationId { get; set; }
        public int GroupId { get; set; }
        public int RoleId { get; set; }
        public bool IsActive { get; set; }
    }
}
