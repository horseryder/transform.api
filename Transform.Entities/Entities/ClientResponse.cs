﻿namespace Transform.Entities.Entities
{
    public class ClientResponse
    {
        public int ResponseId { get; set; }
        public string ResponseName { get; set; }
        public bool IsActive { get; set; }
    }
}
