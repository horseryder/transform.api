﻿using System;

namespace Transform.Entities.Entities
{
    public class UsersAttendance 
    {
        public int AttendanceId { get; set; }
        public int UserId { get; set; }
        public int OrganizationId { get; set; }

        public DateTime ClockDateTime { get; set; }
        public Boolean IsClockIn { get; set; }
    }
}
