﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transform.Entities.Entities
{
    public class Equipments : Base
    {
        public int EquipmentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal? Price { get; set; }
        public int? InStock { get; set; }
        public int? CategoryId { get; set; }
        public int? OrganizationId { set; get; }
        public bool IsActive { set; get; }
    }
}
