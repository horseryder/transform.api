﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transform.Entities.Entities
{
    public class UserClients : Base
    {
        public int ClientId { get; set; }
        public int UserId { get; set; }
        public int PackageId { get; set; }
        public int BatchId { get; set; }
        public int ProgramId { get; set; }
        public decimal PackagePrice { get; set; }
        public decimal Discount { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal AmountPaid { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool PaymentCompleted { get; set; }
    }
}
