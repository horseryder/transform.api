﻿namespace Transform.Entities.Entities
{
    public class UserGroups : Base
    {
        public int GroupId { get; set; }

        public string GroupName { get; set; }

        public bool IsActive { get; set; }

        public int? OrganizationId { get; set; }
    }
}
