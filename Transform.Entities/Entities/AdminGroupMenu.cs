﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transform.Entities.Entities
{
    public class AdminGroupMenu : Base
    {
        public int GroupMenuId { get; set; }
        public int GroupId { get; set; }
        public int MenuId { get; set; }
        public bool IsActive { get; set; }
        public bool? IsIndeterminate { get; set; }
    }
}
