﻿namespace Transform.Entities.Entities
{
    public class Users : Base
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Password { set; get; }
        public bool? IsMaster { set; get; }
        public int? OrganizationId { get; set; }
        public string ProfilePic { set; get; }
        public char? Gender { set; get; }
        public string Address { set; get; }
        public bool IsActive { set; get; }
        public bool? IsSysAdmin { set; get; }
        public bool IsEmployee { set; get; }
    }
}
