﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transform.Entities.Entities
{
    public class Category : Base
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? OrderNumber { get; set; }
        public int? CategoryType { get; set; }
        public int? OrganizationId { set; get; }
        public bool IsActive { set; get; }
    }
}
