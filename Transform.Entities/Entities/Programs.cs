﻿namespace Transform.Entities.Entities
{
    public class Programs : Base
    {
        public int ProgramId { get; set; }
        public string Name { get; set; }
        public int OrganizationId { get; set; }
        public bool IsActive { get; set; }
    }
}
