﻿using System;
namespace Transform.Entities.Entities
{
    public class Batch : Base
    {
        public int BatchId { get; set; }
        public string BatchName { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int OrganizationId { get; set; }
        public bool IsActive { get; set; }
    }
}
