﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transform.Entities.Entities
{
    public class UserPackages : Base
    {
        public int PackageId { get; set; }
        public string PackageName { get; set; }
        public string Description { get; set; }
        public int OrganizationId { get; set; }
        public decimal PackagePrice { get; set; }
        public int Duration { get; set; }
        public bool IsActive { get; set; }
        public bool IsArchive { get; set; }
    }
}
