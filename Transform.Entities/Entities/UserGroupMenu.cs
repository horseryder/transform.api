﻿namespace Transform.Entities.Entities
{
    public class UserGroupMenu : Base
    {
        public int GroupMenuId { get; set; }
        public int GroupId { get; set; }
        public int? OrganizationId { get; set; }
        public int? MenuId { get; set; }
        public bool IsActive { get; set; }
        public bool? IsIndeterminate { get; set; }
    }
}
