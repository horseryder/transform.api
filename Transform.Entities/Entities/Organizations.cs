﻿namespace Transform.Entities.Entities
{
    public class Organizations: Base
    {
        public int OrganizationId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public int? PackageId { get; set; }
        public string Logo { get; set; }
        public bool IsMaster { get; set; }
        public int? ParentOrganizationId { get; set; }
        public bool IsActive { get; set; }
    }
}
