﻿namespace Transform.Entities.Entities
{
    public class AdminPackages : Base
    {
        public int PackageId { get; set; }
        public int GroupId { get; set; }
        public string PackageName { get; set; }
        public decimal PackagePrice { get; set; }
        public int Validity { get; set; }
        public string Description { get; set; }
        public bool SubBranches { get; set; }
        public int? SubBranchCount { get; set; }
        public bool IsActive { get; set; }
    }
}
