﻿namespace Transform.Entities.Entities
{
    public class UserRoles : Base
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public int OrganizationId { get; set; }
        public bool IsActive { get; set; }
    }
}
