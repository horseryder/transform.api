﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transform.Models.Client
{
    public class ClientListModel
    {
        public int UserId { get; set; }
        public string FullName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string ProfilePic { set; get; }
        public string Gender { set; get; }
        public string Address { set; get; }
        public bool IsActive { set; get; }
    }
}
