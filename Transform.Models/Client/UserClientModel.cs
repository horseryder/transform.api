﻿using Transform.Models.Users;

namespace Transform.Models.Client
{
    public class UserClientModel
    {
        public UsersModel UserDetails { get; set; }

        public ClientsPackageModel ClientPackages { get; set; }
    }
}
