﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transform.Models
{
    public class SmtpSettings
    {
        public string SmtpServer { get; set; }

        public string SmtpPort { get; set; }

        public string SmtpEmail { get; set; }

        public string SmtpPassword { get; set; }

        public string DisplayName { get; set; }
    }
}
