﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transform.Models.Menu
{
    public class MenuLevel
    {
        public int MenuId { get; set; }
        public string MenuName { get; set; }
        public int MenuSequence { get; set; }
        public string RoutePath { get; set; }
        public string Icon { get; set; }
        public int? ParentMenuId { get; set; }
        public bool IsActive { get; set; }
        public IEnumerable<MenuLevel> SubMenus { get; set; }
    }

}
