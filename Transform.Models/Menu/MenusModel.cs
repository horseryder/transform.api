﻿using System.ComponentModel.DataAnnotations;

namespace Transform.Models.Menu
{
    public class MenusModel
    {
        public int MenuId { get; set; }

        [Required]
        public string MenuName { get; set; }
        public int MenuSequence { get; set; }
        public string RoutePath { get; set; }
        public string Icon { get; set; }
        public int? ParentMenuId { get; set; }
        public bool IsActive { get; set; }
    }
}
