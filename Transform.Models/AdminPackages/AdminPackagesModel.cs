﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Transform.Models.AdminPackages
{
    public class AdminPackagesModel
    {
        public int PackageId { get; set; }

        public int GroupId { get; set; }

        [Required(ErrorMessage = "Enter Package Name")]
        public string PackageName { get; set; }

        public decimal PackagePrice { get; set; }

        public int Validity { get; set; }

        public string Description { get; set; }

        public bool SubBranches { get; set; }

        public int? SubBranchCount { get; set; }

        public bool IsActive { get; set; }
    }
}
