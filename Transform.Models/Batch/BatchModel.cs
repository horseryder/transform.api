﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Transform.Models.Batch
{
    public class BatchModel
    {
        public int BatchId { get; set; }

        [Required(ErrorMessage = "Enter Name")]
        public string BatchName { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public bool IsActive { get; set; }
    }
}
