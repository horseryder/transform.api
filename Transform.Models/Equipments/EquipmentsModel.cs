﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transform.Models.Equipments
{
    public class EquipmentsModel
    {
        public int EquipmentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal? Price { get; set; }
        public string UnitName { get; set; }
        public int? InStock { get; set; }
        public int? CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int? OrganizationId { set; get; }
        public bool IsActive { set; get; }
    }
}
