﻿using System.Collections.Generic;
using Transform.Models.Menu;

namespace Transform.Models.GroupMenu
{
    public class GroupMenuEntityModel 
    {
        public int GroupMenuId { get; set; }
        public int MenuId { get; set; }
        public bool? IsIndeterminate { get; set; }
    }

    public class GroupLevelMenus : MenuLevel
    {
        public bool IsChecked { get; set; }
        public int? GroupMenuId { get; set; }
        public bool? IsIndeterminate { get; set; }

    }

}
