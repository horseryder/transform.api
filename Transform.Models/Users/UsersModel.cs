﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Transform.Models.Users
{
    public class UsersModel : IValidatableObject
    {
        public int UserId { get; set; }

        [Required(ErrorMessage = "Enter First Name")]
        public string FirstName { get; set; }

        public string LastName { get; set; }

        [Required(ErrorMessage = "Mobile no. is required")]
        [RegularExpression("^(?!0+$)(\\+\\d{1,3}[- ]?)?(?!0+$)\\d{10,15}$", ErrorMessage = "Please enter valid phone no.")]

        public string Mobile { get; set; }

        public string Email { get; set; }

        public string Password { set; get; }

        public bool? IsMaster { set; get; }

        public bool? IsEmployee { set; get; }

        public int? OrganizationId { get; set; }

        public string ProfilePic { set; get; }

        public char? Gender { set; get; }

        public string Address { set; get; }

        public bool IsActive { set; get; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            bool isemployee = this.IsEmployee ?? false;
            if (isemployee && this.Password == null)
            {
                yield return new ValidationResult("Password is required");
            }

            if (!string.IsNullOrEmpty(Password))
            {
                if (this.Password.Length < 8)
                {
                    yield return new ValidationResult("Password should contain minimum of 8 characters");
                }
                else
                {
                    Regex reg = new Regex(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{4,}$");
                    if (!reg.IsMatch(this.Password))
                    {
                        yield return new ValidationResult("Password should contain atleast one uppercase, lowercase, digit and a special character");
                    }
                }
            }

            if (!string.IsNullOrEmpty(this.Email))
            {
                Regex reg = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
                if (!reg.IsMatch(this.Email))
                {
                    yield return new ValidationResult("Invalid Email Address");
                }
            }
        }
    }
}
