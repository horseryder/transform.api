﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transform.Models.Users
{
    public class UserInfoModel : UsersModel
    {
        public string RoleName { get; set; }
    }
}
