﻿using Transform.Models.Organizations;

namespace Transform.Models.Users
{
    public class UserSetupModel
    {
        public UsersModel UsersModel { get; set; }
        public OrganizationsModel OrganizationModel { get; set; }
    }
}
