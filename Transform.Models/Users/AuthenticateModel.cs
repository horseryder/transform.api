﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transform.Models.Users
{
    public class AuthenticateModel
    {
        public string UserName { get; set; }

        public string Password { get; set; }
    }
}
