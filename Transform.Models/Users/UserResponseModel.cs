﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transform.Models.Users
{
    public class UserResponseModel
    {
        public int UserId { get; set; }

        public string UserName { get; set; }

        public string AccessToken { get; set; }

        public string RefreshToken { get; set; }
    }
}
