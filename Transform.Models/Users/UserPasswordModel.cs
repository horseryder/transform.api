﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Transform.Models.Users
{
    public class UserPasswordModel
    {
        [Required(ErrorMessage = "Enter Current Password")]
        [DataType(DataType.Password)]
        public string CurrentPassword { set; get; }

        [Required(ErrorMessage = "Enter New Password")]
        [DataType(DataType.Password)]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+]).{8,100}$")]
        public string NewPassword { set; get; }

        [Required(ErrorMessage = "Enter Confirm Password")]
        [DataType(DataType.Password)]
        [Compare(nameof(NewPassword))]
        public string ConfirmPassword { get; set; }
    }
}
