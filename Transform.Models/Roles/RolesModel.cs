﻿namespace Transform.Models.Roles
{
    public class RolesModel
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public bool IsActive { set; get; }
    }
}
