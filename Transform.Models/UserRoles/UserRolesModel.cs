﻿namespace Transform.Models.UserRoles
{
    public class UserRolesModel
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public bool IsActive { get; set; }
    }
}
