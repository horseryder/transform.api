﻿namespace Transform.Models.UserPackages
{
    public class UserPackagesModel
    {
        public int PackageId { get; set; }
        public string PackageName { get; set; }
        public string Description { get; set; }
        public int OrganizationId { get; set; }
        public decimal PackagePrice { get; set; }
        public int Duration { get; set; }
        public bool IsActive { get; set; }

    }
}
