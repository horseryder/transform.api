﻿using System.ComponentModel.DataAnnotations;

namespace Transform.Models.Program
{
    public class ProgramModel
    {
        public int ProgramId { get; set; }

        [Required(ErrorMessage = "Enter Name")]
        public string Name { get; set; }

        public int OrganizationId { get; set; }

        public bool IsActive { get; set; }
    }
}
