﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transform.Models.Authentication
{
    public class TokenManagement
    {
        public string Issuer { get; set; }

        public string Audience { get; set; }

        public string Key { get; set; }

        public double AccessExpiration { get; set; }

        public string RefreshExpiration { get; set; }
    }
}
