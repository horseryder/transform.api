﻿namespace Transform.Models.UserGroups
{
    public class GroupsModel
    {
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public bool IsActive { get; set; }
    }
}
