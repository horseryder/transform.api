﻿using System.Collections.Generic;

namespace Transform.Models.EmployeesPermission
{
    public class EmployeesPermissionModel
    {
        public int EmployeeId { get; set; }
        public int UserId { get; set; }
        public int GroupId { get; set; }
        public int RoleId { get; set; }
        public bool IsActive { get; set; }
        public int PrimaryOrganizationId { get; set; }
        public List<EmployeeOrganizationModel> OrganizationList { get; set; }

    }

    public class EmployeeOrganizationModel
    {
        public int GroupId { get; set; }
        public int RoleId { get; set; }
        public int OrganizationId { get; set; }
    }
}
