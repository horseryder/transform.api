﻿using System;
using System.Collections.Generic;
using System.Text;
using Transform.Models.Users;
using Transform.Models.EmployeesPermission;

namespace Transform.Models.UserEmployees
{
    public class UserEmployeeModel
    {
        public UsersModel UserDetails { get; set; }
        public EmployeesPermissionModel EmployeesPermission { get; set; }
    }
}
