﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transform.Models.UserEmployees
{
    public class EmployeeListModel
    {
        public int UserId { get; set; }
        public string FullName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string ProfilePic { set; get; }
        public string Gender { set; get; }
        public string Address { set; get; }
        public bool IsActive { set; get; }
        public int GroupId { get; set; }
        public int RoleId { get; set; }
        public string GroupName { get; set; }
        public string RoleName { get; set; }

    }
}
