﻿using System.ComponentModel.DataAnnotations;

namespace Transform.Models.EnquiryForm
{
    public class EnquiryFormModel
    {
        [Required]
        public int EnquiryFormId { get; set; }

        [Required(ErrorMessage = "Enter First Name")]
        public string FirstName { get; set; }

        public string LastName { get; set; }

        [Required(ErrorMessage = "Mobile no. is required")]
        [RegularExpression("^(?!0+$)(\\+\\d{1,3}[- ]?)?(?!0+$)\\d{10,15}$", ErrorMessage = "Please enter valid phone no.")]
        public string Mobile { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        public string TrailDate { get; set; }

        public string FollowUpDate { get; set; }

        public int? ReferBy { get; set; }

        public int? PackageId { get; set; }

        [Required]
        public int ResponseId { get; set; }

        public int? OrganizationId { get; set; }

        [Required]
        public char Gender { set; get; }

        public string Address { set; get; }

        [Required]
        public bool IsActive { set; get; }
    }

    public class EnquiryFormListModel : EnquiryFormModel
    {
        public string ResponseName { get; set; }
        public string FullName { get; set; }
    }
}
