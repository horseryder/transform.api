﻿namespace Transform.Models.Category
{
    public class CategoryModel
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? OrderNumber { get; set; }
        public int? CategoryType { get; set; }
        public string CategoryTypeName { get; set; }
        public int? OrganizationId { set; get; }
        public bool IsActive { set; get; }
    }
}
