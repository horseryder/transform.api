﻿namespace Transform.Models.UserOrganizationsModel
{
    public class UserOrganizationsModel
    {
        public int UserOrganizationId { get; set; }
        public int UserId { get; set; }
        public int OrganizationId { get; set; }
        public bool IsActive { get; set; }
    }
}
