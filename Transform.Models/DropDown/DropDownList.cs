﻿using System.Linq;

namespace Transform.Models.DropDown
{
    public class DropDownList
    {
        public IQueryable<DropDownModel> Roles { get; set; }
        public IQueryable<DropDownModel> Packages { get; set; }
        public IQueryable<DropDownModel> UserRoles { get; set; }
        public IQueryable<DropDownModel> UserGroups { get; set; }
        public IQueryable<DropDownModel> AdminGroups { get; set; }
        public IQueryable<DropDownModel> SubOrganization { get; set; }
        public IQueryable<DropDownModel> ClientResponse { get; set; }
        public IQueryable<DropDownModel> UserPackages { get; set; }
        public IQueryable<DropDownModel> Program { get; set; }
        public IQueryable<DropDownModel> Batch { get; set; }

    }
}
