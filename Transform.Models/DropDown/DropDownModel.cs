﻿namespace Transform.Models.DropDown
{
    public class DropDownModel
    {
        public int Id { get; set; }
        public string Value { get; set; }

    }
}
