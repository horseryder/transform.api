﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transform.Models.ContextModel
{
    public class UserContext
    {
        public int UserId { get; set; }
        public int? GroupId { get; set; }

        public bool IsSysAdmin { set; get; }
        public bool IsMaster { set; get; }
    }
}
