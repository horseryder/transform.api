﻿namespace Transform.Models.ContextModel
{
    public class DataContext
    {
        public OrganizationContext organizationContext { get; set; }

        public UserContext userContext { get; set; }
    }
}
