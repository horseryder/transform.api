﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transform.Models.ContextModel
{
    public class OrganizationContext
    {
        public int OrganizationId { get; set; }

        public int? ParentOrganizationId { get; set; }

        public int PackageId { get; set; }

        public bool IsMaster { get; set; }
    }
}
