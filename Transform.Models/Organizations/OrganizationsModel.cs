﻿namespace Transform.Models.Organizations
{
    public class OrganizationsModel
    {
        public int OrganizationId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public int? PackageId { get; set; }
        public string Logo { get; set; }
        public bool IsActive { get; set; }
        public bool? IsMaster { get; set; }
        public int? ParentOrganizationId { get; set; }

        public string PackageName { get; set; }
    }
}
