﻿SET IDENTITY_INSERT [dbo].[ClientResponse] ON

GO

INSERT [dbo].[ClientResponse]
       ([ResponseId],
        [ResponseName],
        [IsActive])
VALUES (2,
        N'Call Me Later',
        1)

GO

INSERT [dbo].[ClientResponse]
       ([ResponseId],
        [ResponseName],
        [IsActive])
VALUES (5,
        N'Done',
        1)

GO

INSERT [dbo].[ClientResponse]
       ([ResponseId],
        [ResponseName],
        [IsActive])
VALUES (4,
        N'Joined Other',
        1)

GO

INSERT [dbo].[ClientResponse]
       ([ResponseId],
        [ResponseName],
        [IsActive])
VALUES (3,
        N'Not Intersted',
        1)

GO

INSERT [dbo].[ClientResponse]
       ([ResponseId],
        [ResponseName],
        [IsActive])
VALUES (1,
        N'Pending',
        1)

GO

SET IDENTITY_INSERT [dbo].[ClientResponse] OFF

GO 
