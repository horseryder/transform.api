CREATE TABLE [dbo].[Users]
  (
     [UserId]         [INT] IDENTITY(1, 1) PRIMARY KEY,
     [FirstName]      [NVARCHAR](200) NOT NULL,
     [LastName]       [NVARCHAR](200) NULL,
     [Mobile]         [NVARCHAR](10) UNIQUE NOT NULL,
     [Email]          [NVARCHAR](100),
     [Password]       [NVARCHAR](100) NULL,
     [IsMaster]       [BIT] NULL,
     [IsSysAdmin]     [BIT] NULL,
     [OrganizationId] INT NULL,
     [ProfilePic]     [NVARCHAR](max) NULL,
     [Gender]         [CHAR](1) NULL,
     [Address]        [NVARCHAR](max) NULL,
     [IsActive]       [BIT] NOT NULL,
     CreatedBy        INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     CreatedOn        [DATETIME] NULL,
     [ModifiedBy]     INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn]     [DATETIME] NULL
  )

GO

CREATE TABLE [dbo].[AdminGroups]
  (
     GroupId      INT IDENTITY(1, 1) PRIMARY KEY,
     GroupName    NVARCHAR(150) NOT NULL,
     IsActive     BIT NOT NULL,
     CreatedBy    INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     CreatedOn    [DATETIME] NULL,
     [ModifiedBy] INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn] [DATETIME] NULL
  )

GO

CREATE TABLE [dbo].[AdminPackages]
  (
     [PackageId]      [INT] IDENTITY(1, 1) PRIMARY KEY,
     [PackageName]    [VARCHAR](1000) NOT NULL,
     [PackagePrice]   [MONEY] NOT NULL,
     [Validity]       [INT] NOT NULL,
     [Description]    [NVARCHAR](max) NULL,
     [SubBranches]    [BIT] NULL,
     [SubBranchCount] [INT] NULL,
     GroupId          INT FOREIGN KEY REFERENCES [dbo].[AdminGroups](GroupId) NOT NULL,
     [IsActive]       [BIT] NOT NULL,
     CreatedBy        INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     CreatedOn        [DATETIME] NULL,
     [ModifiedBy]     INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn]     [DATETIME] NULL
  )

GO

CREATE TABLE [dbo].[Organizations]
  (
     [OrganizationId]       [INT] IDENTITY(1, 1) PRIMARY KEY,
     [Name]                 [NVARCHAR](1000) NOT NULL,
     [Address]              [NVARCHAR](max) NULL,
     [PackageId]            [INT] FOREIGN KEY REFERENCES [dbo].[AdminPackages] ([PackageId]) NULL,
     [logo]                 [NVARCHAR](max) NULL,
     [IsMaster]             [BIT] NOT NULL,
     [ParentOrganizationId] INT FOREIGN KEY REFERENCES [dbo].[Organizations] ([OrganizationId]),
     [IsActive]             [BIT] NOT NULL,
     CreatedBy              INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     CreatedOn              [DATETIME] NULL,
     [ModifiedBy]           INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn]           [DATETIME] NULL
  )

GO

CREATE TABLE [dbo].[AdminRoles]
  (
     [RoleId]     [INT] IDENTITY(1, 1) PRIMARY KEY,
     [RoleName]   [NVARCHAR](100) NOT NULL,
     [IsActive]   [BIT] NOT NULL,
     CreatedBy    INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     CreatedOn    [DATETIME] NULL,
     [ModifiedBy] INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn] [DATETIME] NULL
  )

GO

CREATE TABLE [dbo].[ClientResponse]
  (
     ResponseId   INT IDENTITY(1, 1) PRIMARY KEY,
     ResponseName VARCHAR(100),
     IsActive     [BIT] NOT NULL
  )

CREATE TABLE [dbo].[UserGroups]
  (
     GroupId        INT IDENTITY(1, 1) PRIMARY KEY,
     GroupName      NVARCHAR(150) NOT NULL,
     OrganizationId INT FOREIGN KEY REFERENCES [dbo].[Organizations] ([OrganizationId]),
     IsActive       BIT NOT NULL,
     CreatedBy      INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     CreatedOn      [DATETIME] NULL,
     [ModifiedBy]   INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn]   [DATETIME] NULL
  )

GO

CREATE TABLE [dbo].[Menus]
  (
     MenuId         INT IDENTITY(1, 1) PRIMARY KEY,
     MenuName       NVARCHAR(150) NOT NULL,
     MenuSequence   INT NOT NULL,
     RoutePath      NVARCHAR(200),
     Icon           NVARCHAR(100),
     ParentMenuId   INT FOREIGN KEY REFERENCES [dbo].[Menus] ([MenuId]) NULL,
     OrganizationId INT FOREIGN KEY REFERENCES [dbo].[Organizations] ([OrganizationId]),
     IsActive       BIT NOT NULL,
     CreatedBy      INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     CreatedOn      [DATETIME] NULL,
     [ModifiedBy]   INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn]   [DATETIME] NULL
  )

GO

CREATE TABLE [dbo].[AdminGroupMenu]
  (
     [GroupMenuId] [INT] IDENTITY(1, 1) PRIMARY KEY,
     [GroupId]     [INT] FOREIGN KEY REFERENCES [dbo].[AdminGroups] ([GroupId]) NOT NULL,
     [MenuId]      INT FOREIGN KEY REFERENCES [dbo].[Menus] ([MenuId]) NOT NULL,
	 [IsIndeterminate] [BIT] NULL,
     [IsActive]    [BIT] NOT NULL,
     CreatedBy     INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     CreatedOn     [DATETIME] NULL,
     [ModifiedBy]  INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn]  [DATETIME] NULL
  )

GO

CREATE TABLE [dbo].[UserGroupMenu]
  (
     [GroupMenuId]    [INT] IDENTITY(1, 1) PRIMARY KEY,
     [GroupId]        [INT] FOREIGN KEY REFERENCES [dbo].[UserGroups] ([GroupId]) NOT NULL,
     [OrganizationId] [INT] FOREIGN KEY REFERENCES [dbo].[Organizations] ([OrganizationId]) NOT NULL,
     [MenuId]         INT FOREIGN KEY REFERENCES [dbo].[Menus] ([MenuId]) NOT NULL,
	 [IsIndeterminate] [BIT] NOT NULL,
     [IsActive]       [BIT] NOT NULL,
     CreatedBy        INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     CreatedOn        [DATETIME] NULL,
     [ModifiedBy]     INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn]     [DATETIME] NULL
  )

GO

CREATE TABLE [dbo].[Roles]
  (
     RoleId         INT IDENTITY(1, 1) PRIMARY KEY,
     RoleName       NVARCHAR(150) NOT NULL,
     OrganizationId INT FOREIGN KEY REFERENCES [dbo].[Organizations] ([OrganizationId]),
     IsMaster       BIT NOT NULL,
     IsActive       BIT NOT NULL,
     CreatedBy      INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     CreatedOn      [DATETIME] NULL,
     [ModifiedBy]   INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn]   [DATETIME] NULL
  )

GO

CREATE TABLE [dbo].[UserEmployees]
  (
     EmployeeId   INT IDENTITY(1, 1) PRIMARY KEY,
     UserId       INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]) UNIQUE NOT NULL,
     GroupId      INT FOREIGN KEY REFERENCES [dbo].[UserGroups] ([GroupId]) NOT NULL,
     RoleId       INT FOREIGN KEY REFERENCES [dbo].[Roles] ([RoleId]) NOT NULL,
     IsActive     BIT NOT NULL,
     CreatedBy    INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     CreatedOn    [DATETIME] NULL,
     [ModifiedBy] INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn] [DATETIME] NULL
  )

GO

CREATE TABLE [dbo].[UserOrganizations]
  (
     [UserOrganizationId] [INT] IDENTITY(1, 1) PRIMARY KEY,
     [UserId]             [INT] NULL,
     [OrganizationId]     [INT] FOREIGN KEY REFERENCES [dbo].[Organizations] ([OrganizationId]),
     [IsActive]           [BIT] NOT NULL,
     CreatedBy            INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     CreatedOn            [DATETIME] NULL,
     [ModifiedBy]         INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn]         [DATETIME] NULL
  )

GO

CREATE TABLE [dbo].[UserPackages]
  (
     [PackageId]      [INT] IDENTITY(1, 1) PRIMARY KEY,
     [PackageName]    [NVARCHAR](250) NULL,
     [Description]    [NVARCHAR](max) NULL,
     [OrganizationId] [INT] FOREIGN KEY REFERENCES [dbo].[Organizations] ([OrganizationId]),
     [PackagePrice]   [MONEY] NULL,
     [Duration]       [INT] NULL,
     [IsArchive]      [BIT] NOT NULL DEFAULT 0,
     [IsActive]       [BIT] NOT NULL DEFAULT 1,
     CreatedBy        INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     CreatedOn        [DATETIME] NULL,
     [ModifiedBy]     INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn]     [DATETIME] NULL
  )

GO

CREATE TABLE [dbo].[EnquiryForm]
  (
     EnquiryFormId  INT IDENTITY(1, 1) PRIMARY KEY,
     FirstName      [VARCHAR](200) NOT NULL,
     LastName       [VARCHAR](200) NULL,
     Mobile         [VARCHAR](10) NOT NULL,
     Email          [VARCHAR](100),
     Gender         [CHAR](1) NULL,
     Address        [NVARCHAR](1000) NULL,
     TrailDate      DATETIME NULL,
     FollowUpDate   DATETIME NULL,
     ReferBy        INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]) NULL,
     PackageId      INT FOREIGN KEY REFERENCES [dbo].[UserPackages] ([PackageId]) NULL,
     ResponseId     INT FOREIGN KEY REFERENCES [dbo].[ClientResponse] ([ResponseId]) NOT NULL,
     OrganizationId [INT] FOREIGN KEY REFERENCES [dbo].[Organizations] ([OrganizationId]) NOT NULL,
     IsActive       [BIT] NOT NULL,
     CreatedBy      INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     CreatedOn      [DATETIME] NULL,
     [ModifiedBy]   INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn]   [DATETIME] NULL
  )

GO

ALTER TABLE [dbo].[Users]
  WITH CHECK ADD FOREIGN KEY([OrganizationId]) REFERENCES [dbo].[Organizations] ([OrganizationId])

GO

INSERT INTO [dbo].[Users]
            ([FirstName],
             [LastName],
             [Mobile],
             [Email],
             [Password],
             [IsMaster],
             [IsSysAdmin],
             [OrganizationId],
             [ProfilePic],
             [Gender],
             [Address],
             [IsActive],
             [CreatedBy],
             [CreatedOn],
             [ModifiedBy],
             [ModifiedOn])
VALUES      ('Admin',
             NULL,
             9030456164,
             'admin@transform.com',
             '1234',
             1,
             1,
             NULL,
             NULL,
             'M',
             NULL,
             1,
             NULL,
             Getdate(),
             NULL,
             NULL)

GO 

SET IDENTITY_INSERT [dbo].[Menus] ON 
GO
INSERT [dbo].[Menus]
       ([MenuId],
        [MenuName],
        [MenuSequence],
        [RoutePath],
        [Icon],
        [ParentMenuId],
        [OrganizationId],
        [IsActive],
        [CreatedBy],
        [CreatedOn],
        [ModifiedBy],
        [ModifiedOn])
VALUES (1,
        N'Adminstration',
        1,
        N'',
        N'fa fa-users',
        NULL,
        NULL,
        1,
        1,
        Cast(N'2020-07-09T22:43:47.297' AS DATETIME),
        NULL,
        NULL)

INSERT [dbo].[Menus]
       ([MenuId],
        [MenuName],
        [MenuSequence],
        [RoutePath],
        [Icon],
        [ParentMenuId],
        [OrganizationId],
        [IsActive],
        [CreatedBy],
        [CreatedOn],
        [ModifiedBy],
        [ModifiedOn])
VALUES (2,
        N'Package List',
        1,
        N'/admin/package-list',
        N'fa fa-sitemap',
        1,
        NULL,
        1,
        1,
        Cast(N'2020-07-09T22:44:30.647' AS DATETIME),
        NULL,
        NULL)

INSERT [dbo].[Menus]
       ([MenuId],
        [MenuName],
        [MenuSequence],
        [RoutePath],
        [Icon],
        [ParentMenuId],
        [OrganizationId],
        [IsActive],
        [CreatedBy],
        [CreatedOn],
        [ModifiedBy],
        [ModifiedOn])
VALUES (3,
        N'Sub Organizations',
        2,
        N'/admin/suborganizations-list',
        N'fa fa-sitemap',
        1,
        NULL,
        1,
        1,
        Cast(N'2020-07-09T22:44:57.870' AS DATETIME),
        NULL,
        NULL)

INSERT [dbo].[Menus]
       ([MenuId],
        [MenuName],
        [MenuSequence],
        [RoutePath],
        [Icon],
        [ParentMenuId],
        [OrganizationId],
        [IsActive],
        [CreatedBy],
        [CreatedOn],
        [ModifiedBy],
        [ModifiedOn])
VALUES (4,
        N'Roles',
        3,
        N'/admin/userroles',
        N'',
        1,
        NULL,
        1,
        1,
        Cast(N'2020-07-09T22:45:26.427' AS DATETIME),
        NULL,
        NULL)

INSERT [dbo].[Menus]
       ([MenuId],
        [MenuName],
        [MenuSequence],
        [RoutePath],
        [Icon],
        [ParentMenuId],
        [OrganizationId],
        [IsActive],
        [CreatedBy],
        [CreatedOn],
        [ModifiedBy],
        [ModifiedOn])
VALUES (5,
        N'Groups',
        4,
        N'/admin/usergroups',
        N'fa fa-users',
        1,
        NULL,
        1,
        1,
        Cast(N'2020-07-09T22:45:54.177' AS DATETIME),
        NULL,
        NULL)

INSERT [dbo].[Menus]
       ([MenuId],
        [MenuName],
        [MenuSequence],
        [RoutePath],
        [Icon],
        [ParentMenuId],
        [OrganizationId],
        [IsActive],
        [CreatedBy],
        [CreatedOn],
        [ModifiedBy],
        [ModifiedOn])
VALUES (6,
        N'Group Menus',
        5,
        N'/admin/groupmenus',
        N'fa fa-users',
        1,
        NULL,
        1,
        1,
        Cast(N'2020-07-09T22:46:20.713' AS DATETIME),
        NULL,
        NULL)

INSERT [dbo].[Menus]
       ([MenuId],
        [MenuName],
        [MenuSequence],
        [RoutePath],
        [Icon],
        [ParentMenuId],
        [OrganizationId],
        [IsActive],
        [CreatedBy],
        [CreatedOn],
        [ModifiedBy],
        [ModifiedOn])
VALUES (7,
        N'Employees',
        6,
        N'/admin/employees',
        N'fa fa-users',
        1,
        NULL,
        1,
        1,
        Cast(N'2020-07-09T22:46:48.517' AS DATETIME),
        NULL,
        NULL)

INSERT [dbo].[Menus]
       ([MenuId],
        [MenuName],
        [MenuSequence],
        [RoutePath],
        [Icon],
        [ParentMenuId],
        [OrganizationId],
        [IsActive],
        [CreatedBy],
        [CreatedOn],
        [ModifiedBy],
        [ModifiedOn])
VALUES (8,
        N'Clients',
        2,
        N'',
        N'fa fa-walking',
        NULL,
        NULL,
        1,
        1,
        Cast(N'2020-07-09T22:47:16.063' AS DATETIME),
        NULL,
        NULL)

INSERT [dbo].[Menus]
       ([MenuId],
        [MenuName],
        [MenuSequence],
        [RoutePath],
        [Icon],
        [ParentMenuId],
        [OrganizationId],
        [IsActive],
        [CreatedBy],
        [CreatedOn],
        [ModifiedBy],
        [ModifiedOn])
VALUES (9,
        N'Enquiry Form',
        1,
        N'/admin/enquiryformlist',
        N'',
        8,
        NULL,
        1,
        1,
        Cast(N'2020-07-09T22:47:34.953' AS DATETIME),
        NULL,
        NULL) 
		GO
		SET IDENTITY_INSERT [dbo].[Menus] OFF
