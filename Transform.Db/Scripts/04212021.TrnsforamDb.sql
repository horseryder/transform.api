USE [master]
GO
/****** Object:  Database [retailsy_transform]    Script Date: 4/21/2021 10:50:09 AM ******/
CREATE DATABASE [retailsy_transform]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'retailsy_transform', FILENAME = N'D:\Program Files\MSSQL\DATA\retailsy_transform.mdf' , SIZE = 8192KB , MAXSIZE = 1048576KB , FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'retailsy_transform_log', FILENAME = N'D:\Program Files\MSSQL\DATA\retailsy_transform_log.ldf' , SIZE = 8192KB , MAXSIZE = 1048576KB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [retailsy_transform] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [retailsy_transform].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [retailsy_transform] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [retailsy_transform] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [retailsy_transform] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [retailsy_transform] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [retailsy_transform] SET ARITHABORT OFF 
GO
ALTER DATABASE [retailsy_transform] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [retailsy_transform] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [retailsy_transform] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [retailsy_transform] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [retailsy_transform] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [retailsy_transform] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [retailsy_transform] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [retailsy_transform] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [retailsy_transform] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [retailsy_transform] SET  ENABLE_BROKER 
GO
ALTER DATABASE [retailsy_transform] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [retailsy_transform] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [retailsy_transform] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [retailsy_transform] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [retailsy_transform] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [retailsy_transform] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [retailsy_transform] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [retailsy_transform] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [retailsy_transform] SET  MULTI_USER 
GO
ALTER DATABASE [retailsy_transform] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [retailsy_transform] SET DB_CHAINING OFF 
GO
ALTER DATABASE [retailsy_transform] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [retailsy_transform] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [retailsy_transform] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [retailsy_transform] SET QUERY_STORE = OFF
GO
USE [retailsy_transform]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [retailsy_transform]
GO
/****** Object:  User [retailsy_sa]    Script Date: 4/21/2021 10:50:10 AM ******/
CREATE USER [retailsy_sa] FOR LOGIN [retailsy_sa] WITH DEFAULT_SCHEMA=[retailsy_sa]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [retailsy_sa]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [retailsy_sa]
GO
ALTER ROLE [db_datareader] ADD MEMBER [retailsy_sa]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [retailsy_sa]
GO
/****** Object:  Schema [retailsy_sa]    Script Date: 4/21/2021 10:50:10 AM ******/
CREATE SCHEMA [retailsy_sa]
GO
/****** Object:  Table [dbo].[AdminGroupMenu]    Script Date: 4/21/2021 10:50:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdminGroupMenu](
	[GroupMenuId] [int] IDENTITY(1,1) NOT NULL,
	[GroupId] [int] NOT NULL,
	[MenuId] [int] NOT NULL,
	[IsIndeterminate] [bit] NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[GroupMenuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AdminGroups]    Script Date: 4/21/2021 10:50:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdminGroups](
	[GroupId] [int] IDENTITY(1,1) NOT NULL,
	[GroupName] [nvarchar](150) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[GroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AdminPackages]    Script Date: 4/21/2021 10:50:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdminPackages](
	[PackageId] [int] IDENTITY(1,1) NOT NULL,
	[PackageName] [varchar](1000) NOT NULL,
	[PackagePrice] [money] NOT NULL,
	[Validity] [int] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[SubBranches] [bit] NULL,
	[SubBranchCount] [int] NULL,
	[GroupId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[PackageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AdminRoles]    Script Date: 4/21/2021 10:50:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdminRoles](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Batch]    Script Date: 4/21/2021 10:50:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Batch](
	[BatchId] [int] IDENTITY(1,1) NOT NULL,
	[BatchName] [varchar](100) NOT NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[OrganizationId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[BatchId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 4/21/2021 10:50:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](150) NOT NULL,
	[Description] [varchar](200) NULL,
	[OrderNumber] [int] NULL,
	[CategoryType] [int] NOT NULL,
	[OrganizationId] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ClientResponse]    Script Date: 4/21/2021 10:50:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClientResponse](
	[ResponseId] [int] IDENTITY(1,1) NOT NULL,
	[ResponseName] [varchar](100) NULL,
	[IsActive] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ResponseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EnquiryForm]    Script Date: 4/21/2021 10:50:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EnquiryForm](
	[EnquiryFormId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](200) NOT NULL,
	[LastName] [varchar](200) NULL,
	[Mobile] [varchar](10) NOT NULL,
	[Email] [varchar](100) NULL,
	[Gender] [char](1) NULL,
	[Address] [nvarchar](1000) NULL,
	[TrailDate] [datetime] NULL,
	[FollowUpDate] [datetime] NULL,
	[ReferBy] [int] NULL,
	[PackageId] [int] NULL,
	[ResponseId] [int] NOT NULL,
	[OrganizationId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EnquiryFormId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Equipments]    Script Date: 4/21/2021 10:50:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Equipments](
	[EquipmentId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](150) NOT NULL,
	[Description] [varchar](1000) NULL,
	[Price] [money] NULL,
	[InStock] [int] NULL,
	[CategoryId] [int] NULL,
	[OrganizationId] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EquipmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Menus]    Script Date: 4/21/2021 10:50:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Menus](
	[MenuId] [int] IDENTITY(1,1) NOT NULL,
	[MenuName] [nvarchar](150) NOT NULL,
	[MenuSequence] [int] NOT NULL,
	[RoutePath] [nvarchar](200) NULL,
	[Icon] [nvarchar](100) NULL,
	[ParentMenuId] [int] NULL,
	[OrganizationId] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[MenuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Organizations]    Script Date: 4/21/2021 10:50:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Organizations](
	[OrganizationId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](1000) NOT NULL,
	[Address] [nvarchar](max) NULL,
	[PackageId] [int] NULL,
	[logo] [nvarchar](max) NULL,
	[IsMaster] [bit] NOT NULL,
	[ParentOrganizationId] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[OrganizationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Program]    Script Date: 4/21/2021 10:50:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Program](
	[ProgramId] [int] IDENTITY(1,1) NOT NULL,
	[NAME] [varchar](500) NOT NULL,
	[OrganizationId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ProgramId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserClients]    Script Date: 4/21/2021 10:50:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserClients](
	[ClientId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[PackageId] [int] NOT NULL,
	[BatchId] [int] NULL,
	[ProgramId] [int] NULL,
	[PackagePrice] [money] NULL,
	[Discount] [money] NULL,
	[TotalPrice] [money] NULL,
	[AmountPaid] [money] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[PaymentCompleted] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ClientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserEmployees]    Script Date: 4/21/2021 10:50:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserEmployees](
	[EmployeeId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[GroupId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EmployeeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserGroupMenu]    Script Date: 4/21/2021 10:50:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserGroupMenu](
	[GroupMenuId] [int] IDENTITY(1,1) NOT NULL,
	[GroupId] [int] NOT NULL,
	[OrganizationId] [int] NOT NULL,
	[MenuId] [int] NOT NULL,
	[IsIndeterminate] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[GroupMenuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserGroups]    Script Date: 4/21/2021 10:50:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserGroups](
	[GroupId] [int] IDENTITY(1,1) NOT NULL,
	[GroupName] [nvarchar](150) NOT NULL,
	[OrganizationId] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[GroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserOrganizations]    Script Date: 4/21/2021 10:50:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserOrganizations](
	[UserOrganizationId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[OrganizationId] [int] NULL,
	[GroupId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserOrganizationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserPackages]    Script Date: 4/21/2021 10:50:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserPackages](
	[PackageId] [int] IDENTITY(1,1) NOT NULL,
	[PackageName] [nvarchar](250) NULL,
	[Description] [nvarchar](max) NULL,
	[OrganizationId] [int] NULL,
	[PackagePrice] [money] NULL,
	[Duration] [int] NULL,
	[IsArchive] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[PackageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRoles]    Script Date: 4/21/2021 10:50:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRoles](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](150) NOT NULL,
	[OrganizationId] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 4/21/2021 10:50:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](200) NOT NULL,
	[LastName] [nvarchar](200) NULL,
	[Mobile] [nvarchar](10) NOT NULL,
	[Email] [nvarchar](100) NULL,
	[Password] [nvarchar](100) NULL,
	[IsMaster] [bit] NULL,
	[IsSysAdmin] [bit] NULL,
	[IsEmployee] [bit] NOT NULL,
	[OrganizationId] [int] NULL,
	[ProfilePic] [nvarchar](max) NULL,
	[Gender] [char](1) NULL,
	[Address] [nvarchar](max) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK__Users__1788CC4C3D2C868C] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ__Users__6FAE078218A40DE0] UNIQUE NONCLUSTERED 
(
	[Mobile] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UsersAttendance]    Script Date: 4/21/2021 10:50:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsersAttendance](
	[AttendanceId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[OrganizationId] [int] NULL,
	[ClockDateTime] [datetime] NOT NULL,
	[IsClockIn] [bit] NOT NULL,
	[IsReportGenerated] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[AttendanceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UsersAttendanceReport]    Script Date: 4/21/2021 10:50:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsersAttendanceReport](
	[AttendanceReportId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[OrganizationId] [int] NULL,
	[Date] [date] NOT NULL,
	[ClockInTime] [datetime] NULL,
	[ClockOutTime] [datetime] NULL,
	[TotalHours] [decimal](4, 2) NULL,
	[TotalMinutes] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[AttendanceReportId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserPackages] ADD  DEFAULT ((0)) FOR [IsArchive]
GO
ALTER TABLE [dbo].[UserPackages] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[UsersAttendance] ADD  DEFAULT ((0)) FOR [IsReportGenerated]
GO
ALTER TABLE [dbo].[AdminGroupMenu]  WITH CHECK ADD  CONSTRAINT [FK__AdminGrou__Creat__6EF57B66] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[AdminGroupMenu] CHECK CONSTRAINT [FK__AdminGrou__Creat__6EF57B66]
GO
ALTER TABLE [dbo].[AdminGroupMenu]  WITH CHECK ADD FOREIGN KEY([GroupId])
REFERENCES [dbo].[AdminGroups] ([GroupId])
GO
ALTER TABLE [dbo].[AdminGroupMenu]  WITH CHECK ADD FOREIGN KEY([MenuId])
REFERENCES [dbo].[Menus] ([MenuId])
GO
ALTER TABLE [dbo].[AdminGroupMenu]  WITH CHECK ADD  CONSTRAINT [FK__AdminGrou__Modif__6FE99F9F] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[AdminGroupMenu] CHECK CONSTRAINT [FK__AdminGrou__Modif__6FE99F9F]
GO
ALTER TABLE [dbo].[AdminGroups]  WITH CHECK ADD  CONSTRAINT [FK__AdminGrou__Creat__4E88ABD4] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[AdminGroups] CHECK CONSTRAINT [FK__AdminGrou__Creat__4E88ABD4]
GO
ALTER TABLE [dbo].[AdminGroups]  WITH CHECK ADD  CONSTRAINT [FK__AdminGrou__Modif__4F7CD00D] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[AdminGroups] CHECK CONSTRAINT [FK__AdminGrou__Modif__4F7CD00D]
GO
ALTER TABLE [dbo].[AdminPackages]  WITH CHECK ADD  CONSTRAINT [FK__AdminPack__Creat__534D60F1] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[AdminPackages] CHECK CONSTRAINT [FK__AdminPack__Creat__534D60F1]
GO
ALTER TABLE [dbo].[AdminPackages]  WITH CHECK ADD FOREIGN KEY([GroupId])
REFERENCES [dbo].[AdminGroups] ([GroupId])
GO
ALTER TABLE [dbo].[AdminPackages]  WITH CHECK ADD  CONSTRAINT [FK__AdminPack__Modif__5441852A] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[AdminPackages] CHECK CONSTRAINT [FK__AdminPack__Modif__5441852A]
GO
ALTER TABLE [dbo].[AdminRoles]  WITH CHECK ADD  CONSTRAINT [FK__AdminRole__Creat__5CD6CB2B] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[AdminRoles] CHECK CONSTRAINT [FK__AdminRole__Creat__5CD6CB2B]
GO
ALTER TABLE [dbo].[AdminRoles]  WITH CHECK ADD  CONSTRAINT [FK__AdminRole__Modif__5DCAEF64] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[AdminRoles] CHECK CONSTRAINT [FK__AdminRole__Modif__5DCAEF64]
GO
ALTER TABLE [dbo].[Batch]  WITH CHECK ADD  CONSTRAINT [FK__Batch__CreatedBy__1C873BEC] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Batch] CHECK CONSTRAINT [FK__Batch__CreatedBy__1C873BEC]
GO
ALTER TABLE [dbo].[Batch]  WITH CHECK ADD  CONSTRAINT [FK__Batch__ModifiedB__1D7B6025] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Batch] CHECK CONSTRAINT [FK__Batch__ModifiedB__1D7B6025]
GO
ALTER TABLE [dbo].[Batch]  WITH CHECK ADD FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organizations] ([OrganizationId])
GO
ALTER TABLE [dbo].[Category]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Category]  WITH CHECK ADD FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[EnquiryForm]  WITH CHECK ADD  CONSTRAINT [FK__EnquiryFo__Creat__151B244E] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[EnquiryForm] CHECK CONSTRAINT [FK__EnquiryFo__Creat__151B244E]
GO
ALTER TABLE [dbo].[EnquiryForm]  WITH CHECK ADD  CONSTRAINT [FK__EnquiryFo__Modif__160F4887] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[EnquiryForm] CHECK CONSTRAINT [FK__EnquiryFo__Modif__160F4887]
GO
ALTER TABLE [dbo].[EnquiryForm]  WITH CHECK ADD FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organizations] ([OrganizationId])
GO
ALTER TABLE [dbo].[EnquiryForm]  WITH CHECK ADD FOREIGN KEY([PackageId])
REFERENCES [dbo].[UserPackages] ([PackageId])
GO
ALTER TABLE [dbo].[EnquiryForm]  WITH CHECK ADD  CONSTRAINT [FK__EnquiryFo__Refer__114A936A] FOREIGN KEY([ReferBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[EnquiryForm] CHECK CONSTRAINT [FK__EnquiryFo__Refer__114A936A]
GO
ALTER TABLE [dbo].[EnquiryForm]  WITH CHECK ADD FOREIGN KEY([ResponseId])
REFERENCES [dbo].[ClientResponse] ([ResponseId])
GO
ALTER TABLE [dbo].[Equipments]  WITH CHECK ADD FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([CategoryId])
GO
ALTER TABLE [dbo].[Equipments]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Equipments]  WITH CHECK ADD FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Menus]  WITH CHECK ADD  CONSTRAINT [FK__Menus__CreatedBy__693CA210] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Menus] CHECK CONSTRAINT [FK__Menus__CreatedBy__693CA210]
GO
ALTER TABLE [dbo].[Menus]  WITH CHECK ADD  CONSTRAINT [FK__Menus__ModifiedB__6A30C649] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Menus] CHECK CONSTRAINT [FK__Menus__ModifiedB__6A30C649]
GO
ALTER TABLE [dbo].[Menus]  WITH CHECK ADD FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organizations] ([OrganizationId])
GO
ALTER TABLE [dbo].[Menus]  WITH CHECK ADD FOREIGN KEY([ParentMenuId])
REFERENCES [dbo].[Menus] ([MenuId])
GO
ALTER TABLE [dbo].[Organizations]  WITH CHECK ADD  CONSTRAINT [FK__Organizat__Creat__59063A47] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Organizations] CHECK CONSTRAINT [FK__Organizat__Creat__59063A47]
GO
ALTER TABLE [dbo].[Organizations]  WITH CHECK ADD  CONSTRAINT [FK__Organizat__Modif__59FA5E80] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Organizations] CHECK CONSTRAINT [FK__Organizat__Modif__59FA5E80]
GO
ALTER TABLE [dbo].[Organizations]  WITH CHECK ADD FOREIGN KEY([PackageId])
REFERENCES [dbo].[AdminPackages] ([PackageId])
GO
ALTER TABLE [dbo].[Organizations]  WITH CHECK ADD FOREIGN KEY([ParentOrganizationId])
REFERENCES [dbo].[Organizations] ([OrganizationId])
GO
ALTER TABLE [dbo].[Program]  WITH CHECK ADD  CONSTRAINT [FK__Program__Created__2610A626] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Program] CHECK CONSTRAINT [FK__Program__Created__2610A626]
GO
ALTER TABLE [dbo].[Program]  WITH CHECK ADD  CONSTRAINT [FK__Program__Modifie__2704CA5F] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Program] CHECK CONSTRAINT [FK__Program__Modifie__2704CA5F]
GO
ALTER TABLE [dbo].[Program]  WITH CHECK ADD FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organizations] ([OrganizationId])
GO
ALTER TABLE [dbo].[UserClients]  WITH CHECK ADD FOREIGN KEY([BatchId])
REFERENCES [dbo].[Batch] ([BatchId])
GO
ALTER TABLE [dbo].[UserClients]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UserClients]  WITH CHECK ADD FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UserClients]  WITH CHECK ADD FOREIGN KEY([PackageId])
REFERENCES [dbo].[UserPackages] ([PackageId])
GO
ALTER TABLE [dbo].[UserClients]  WITH CHECK ADD FOREIGN KEY([ProgramId])
REFERENCES [dbo].[Program] ([ProgramId])
GO
ALTER TABLE [dbo].[UserClients]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UserEmployees]  WITH CHECK ADD  CONSTRAINT [FK__UserEmplo__Creat__395884C4] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UserEmployees] CHECK CONSTRAINT [FK__UserEmplo__Creat__395884C4]
GO
ALTER TABLE [dbo].[UserEmployees]  WITH CHECK ADD FOREIGN KEY([GroupId])
REFERENCES [dbo].[UserGroups] ([GroupId])
GO
ALTER TABLE [dbo].[UserEmployees]  WITH CHECK ADD  CONSTRAINT [FK__UserEmplo__Modif__3A4CA8FD] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UserEmployees] CHECK CONSTRAINT [FK__UserEmplo__Modif__3A4CA8FD]
GO
ALTER TABLE [dbo].[UserEmployees]  WITH CHECK ADD FOREIGN KEY([RoleId])
REFERENCES [dbo].[UserRoles] ([RoleId])
GO
ALTER TABLE [dbo].[UserEmployees]  WITH CHECK ADD  CONSTRAINT [FK__UserEmplo__UserI__367C1819] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UserEmployees] CHECK CONSTRAINT [FK__UserEmplo__UserI__367C1819]
GO
ALTER TABLE [dbo].[UserGroupMenu]  WITH CHECK ADD  CONSTRAINT [FK__UserGroup__Creat__5F7E2DAC] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UserGroupMenu] CHECK CONSTRAINT [FK__UserGroup__Creat__5F7E2DAC]
GO
ALTER TABLE [dbo].[UserGroupMenu]  WITH CHECK ADD FOREIGN KEY([GroupId])
REFERENCES [dbo].[UserGroups] ([GroupId])
GO
ALTER TABLE [dbo].[UserGroupMenu]  WITH CHECK ADD FOREIGN KEY([MenuId])
REFERENCES [dbo].[Menus] ([MenuId])
GO
ALTER TABLE [dbo].[UserGroupMenu]  WITH CHECK ADD  CONSTRAINT [FK__UserGroup__Modif__607251E5] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UserGroupMenu] CHECK CONSTRAINT [FK__UserGroup__Modif__607251E5]
GO
ALTER TABLE [dbo].[UserGroupMenu]  WITH CHECK ADD FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organizations] ([OrganizationId])
GO
ALTER TABLE [dbo].[UserGroups]  WITH CHECK ADD  CONSTRAINT [FK__UserGroup__Creat__2A164134] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UserGroups] CHECK CONSTRAINT [FK__UserGroup__Creat__2A164134]
GO
ALTER TABLE [dbo].[UserGroups]  WITH CHECK ADD  CONSTRAINT [FK__UserGroup__Modif__2B0A656D] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UserGroups] CHECK CONSTRAINT [FK__UserGroup__Modif__2B0A656D]
GO
ALTER TABLE [dbo].[UserGroups]  WITH CHECK ADD FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organizations] ([OrganizationId])
GO
ALTER TABLE [dbo].[UserOrganizations]  WITH CHECK ADD  CONSTRAINT [FK__UserOrgan__Creat__793DFFAF] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UserOrganizations] CHECK CONSTRAINT [FK__UserOrgan__Creat__793DFFAF]
GO
ALTER TABLE [dbo].[UserOrganizations]  WITH CHECK ADD FOREIGN KEY([GroupId])
REFERENCES [dbo].[UserGroups] ([GroupId])
GO
ALTER TABLE [dbo].[UserOrganizations]  WITH CHECK ADD  CONSTRAINT [FK__UserOrgan__Modif__7A3223E8] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UserOrganizations] CHECK CONSTRAINT [FK__UserOrgan__Modif__7A3223E8]
GO
ALTER TABLE [dbo].[UserOrganizations]  WITH CHECK ADD FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organizations] ([OrganizationId])
GO
ALTER TABLE [dbo].[UserOrganizations]  WITH CHECK ADD FOREIGN KEY([RoleId])
REFERENCES [dbo].[UserRoles] ([RoleId])
GO
ALTER TABLE [dbo].[UserPackages]  WITH CHECK ADD  CONSTRAINT [FK__UserPacka__Creat__0D7A0286] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UserPackages] CHECK CONSTRAINT [FK__UserPacka__Creat__0D7A0286]
GO
ALTER TABLE [dbo].[UserPackages]  WITH CHECK ADD  CONSTRAINT [FK__UserPacka__Modif__0E6E26BF] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UserPackages] CHECK CONSTRAINT [FK__UserPacka__Modif__0E6E26BF]
GO
ALTER TABLE [dbo].[UserPackages]  WITH CHECK ADD FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organizations] ([OrganizationId])
GO
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK__Roles__CreatedBy__7A672E12] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UserRoles] CHECK CONSTRAINT [FK__Roles__CreatedBy__7A672E12]
GO
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK__Roles__ModifiedB__7B5B524B] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UserRoles] CHECK CONSTRAINT [FK__Roles__ModifiedB__7B5B524B]
GO
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organizations] ([OrganizationId])
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK__Users__CreatedBy__4AB81AF0] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK__Users__CreatedBy__4AB81AF0]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK__Users__ModifiedB__4BAC3F29] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK__Users__ModifiedB__4BAC3F29]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK__Users__Organizat__17036CC0] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organizations] ([OrganizationId])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK__Users__Organizat__17036CC0]
GO
ALTER TABLE [dbo].[UsersAttendance]  WITH CHECK ADD FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organizations] ([OrganizationId])
GO
ALTER TABLE [dbo].[UsersAttendance]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UsersAttendanceReport]  WITH CHECK ADD FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organizations] ([OrganizationId])
GO
ALTER TABLE [dbo].[UsersAttendanceReport]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
/****** Object:  StoredProcedure [retailsy_sa].[Generateattendancereports]    Script Date: 4/21/2021 10:50:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [retailsy_sa].[Generateattendancereports]
AS
  BEGIN
      DECLARE @currentUtcDate DATE = Dateadd(mi, 330, Getutcdate())
      DECLARE @date DATE
      DECLARE date_cursor CURSOR FOR
        SELECT Cast(ClockDateTime AS DATE)
        FROM   [dbo].[UsersAttendance]
        WHERE  IsReportGenerated = 0
               AND Cast(ClockDateTime AS DATE) ! = @currentUtcDate
        GROUP  BY Cast(ClockDateTime AS DATE)

      OPEN date_cursor

      FETCH NEXT FROM date_cursor INTO @date

      WHILE @@FETCH_STATUS = 0
        BEGIN
            DECLARE @AttendancesRecords TABLE
              (
                 UserId         INT,
                 OrganizationId INT,
                 ClockDateTime  DATETIME,
                 IsClockIn      BIT
              )

            INSERT INTO @AttendancesRecords
            SELECT UserId,
                   OrganizationId,
                   ClockDateTime,
                   IsClockIn
            FROM   [dbo].[UsersAttendance]
            WHERE  IsReportGenerated = 0
                   AND Cast(ClockDateTime AS DATE) = @date

            INSERT INTO [Dbo].[UsersAttendanceReport]
                        (UserId,
                         OrganizationId,
                         Date,
                         ClockInTime,
                         ClockOutTime,
                         TotalHours,
                         TotalMinutes)
            SELECT t.UserId,
                   t.OrganizationId,
                   Cast(t.CheckIn AS DATE),
                   t.CheckIn,
                   t.CheckOut,
                   Cast(Datediff(MINUTE, t.CheckIn, t.CheckOut) / 60. AS DECIMAL(10, 2)),
                   Datediff(MINUTE, t.CheckIn, t.CheckOut)
            FROM   (SELECT t.UserId,
                           t.OrganizationId,
                           CheckIn  = t.ClockDateTime,
                           CheckOut = r.ClockDateTime,
                           RowNum   = Row_number()
                                      OVER (
                                        PARTITION BY t.UserID, r.ClockDateTime
                                        ORDER BY 1/0)
                    FROM   @AttendancesRecords t
                           OUTER APPLY (SELECT TOP 1 *
                                        FROM   @AttendancesRecords t2
                                        WHERE  t2.UserID = t.UserID
                                               AND t2.ClockDateTime > t.ClockDateTime
                                               AND Dateadd(dd, 0, Datediff(dd, 0, t.ClockDateTime)) = Dateadd(dd, 0, Datediff(dd, 0, t2.ClockDateTime))
                                               AND t2.IsClockIn = 0
                                        ORDER  BY t2.ClockDateTime) r
                    WHERE  t.IsClockIn = 1) t
            WHERE  t.RowNum = 1

            UPDATE [dbo].[UsersAttendance]
            SET    IsReportGenerated = 1
            WHERE  IsReportGenerated = 0
                   AND Cast(ClockDateTime AS DATE) = @date

            FETCH NEXT FROM date_cursor INTO @date
        END

      CLOSE date_cursor;

      DEALLOCATE date_cursor;
  END 
GO
USE [master]
GO
ALTER DATABASE [retailsy_transform] SET  READ_WRITE 
GO
