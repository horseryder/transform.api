﻿CREATE TABLE [dbo].[Organizations]
  (
     [OrganizationId]       [INT] IDENTITY(1, 1) PRIMARY KEY,
     [Name]                 [NVARCHAR](1000) NOT NULL,
     [Address]              [NVARCHAR](max) NULL,
     [PackageId]            [INT] FOREIGN KEY REFERENCES [dbo].[AdminPackages] ([PackageId]) NULL,
     [logo]                 [NVARCHAR](max) NULL,
     [IsMaster]             [BIT] NOT NULL,
     [ParentOrganizationId] INT FOREIGN KEY REFERENCES [dbo].[Organizations] ([OrganizationId]),
     [IsActive]             [BIT] NOT NULL,
     CreatedBy              INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     CreatedOn              [DATETIME] NULL,
     [ModifiedBy]           INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn]           [DATETIME] NULL
  )