﻿CREATE TABLE Roles
  (
     RoleId         INT IDENTITY(1, 1) PRIMARY KEY,
     RoleName       NVARCHAR(150) NOT NULL,
     OrganizationId INT FOREIGN KEY REFERENCES [dbo].[Organizations] ([OrganizationId]),
     IsMaster       BIT NOT NULL,
     IsActive       BIT NOT NULL,
     CreatedBy      INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     CreatedOn      [DATETIME] NULL,
     [ModifiedBy]   INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn]   [DATETIME] NULL
  )