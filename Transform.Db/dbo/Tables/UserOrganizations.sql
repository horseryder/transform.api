﻿CREATE TABLE [dbo].[UserOrganizations]
  (
     [UserOrganizationId] [INT] IDENTITY(1, 1) PRIMARY KEY,
     [UserId]             [INT] NULL,
     [OrganizationId]     [INT] FOREIGN KEY REFERENCES [dbo].[Organizations] ([OrganizationId]),
     [GroupId]            INT FOREIGN KEY REFERENCES [dbo].[UserGroups] ([GroupId]) NOT NULL,
     [RoleId]             INT FOREIGN KEY REFERENCES [dbo].[Roles] ([RoleId]) NOT NULL,
     [IsActive]           [BIT] NOT NULL,
     [CreatedBy]          INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [CreatedOn]          [DATETIME] NULL,
     [ModifiedBy]         INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn]         [DATETIME] NULL
  ) 