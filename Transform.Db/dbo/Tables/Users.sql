﻿CREATE TABLE [dbo].[Users]
  (
     [UserId]         [INT] IDENTITY(1, 1) PRIMARY KEY,
     [FirstName]      [NVARCHAR](200) NOT NULL,
     [LastName]       [NVARCHAR](200) NULL,
     [Mobile]         [NVARCHAR](10) UNIQUE NOT NULL,
     [Email]          [NVARCHAR](100),
     [Password]       [NVARCHAR](100) NULL,
     [IsMaster]       [BIT] NULL,
     [IsSysAdmin]     [BIT] NULL,
     [OrganizationId] INT FOREIGN KEY REFERENCES [dbo].[Organizations] ([OrganizationId]) NULL,
     [ProfilePic]     [NVARCHAR](max) NULL,
     [Gender]         [CHAR](1) NULL,
     [Address]        [NVARCHAR](max) NULL,
     [IsActive]       [BIT] NOT NULL,
     CreatedBy        INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     CreatedOn        [DATETIME] NULL,
     [ModifiedBy]     INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn]     [DATETIME] NULL
  )