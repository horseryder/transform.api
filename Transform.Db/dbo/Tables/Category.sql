﻿CREATE TABLE [dbo].[Category]
  (
     [CategoryId]     [INT] IDENTITY(1, 1) PRIMARY KEY,
     [Name]           [VARCHAR](150) NOT NULL,
     [Description]    [VARCHAR](200) NULL,
     [OrderNumber]    [INT] NULL,
     [CategoryType]   [INT] NOT NULL,
     [OrganizationId] [INT] NULL,
     [IsActive]       [BIT] NOT NULL,
     [CreatedBy]      INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [CreatedOn]      [DATETIME] NULL,
     [ModifiedBy]     INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn]     [DATETIME] NULL
  ) 