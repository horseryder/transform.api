﻿CREATE TABLE [dbo].[UserPackages]
  (
     [PackageId]      [INT] IDENTITY(1, 1) PRIMARY KEY,
     [PackageName]    [NVARCHAR](250) NULL,
     [Description]    [NVARCHAR](max) NULL,
     [OrganizationId] [INT] FOREIGN KEY REFERENCES [dbo].[Organizations] ([OrganizationId]),
     [PackagePrice]   [MONEY] NULL,
     [Duration]       [INT] NULL,
     [IsArchive]      [BIT] NOT NULL DEFAULT 0,
     [IsActive]       [BIT] NOT NULL DEFAULT 1,
     CreatedBy        INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     CreatedOn        [DATETIME] NULL,
     [ModifiedBy]     INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn]     [DATETIME] NULL
  )