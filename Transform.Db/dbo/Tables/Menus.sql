﻿CREATE TABLE Menus
  (
     MenuId         INT IDENTITY(1, 1) PRIMARY KEY,
     MenuName       NVARCHAR(150) NOT NULL,
     MenuSequence   INT NOT NULL,
     RoutePath      NVARCHAR(200),
     Icon           NVARCHAR(100),
     ParentMenuId   INT FOREIGN KEY REFERENCES [dbo].[Menus] ([MenuId]) NULL,
     OrganizationId INT FOREIGN KEY REFERENCES [dbo].[Organizations] ([OrganizationId]),
     IsActive       BIT NOT NULL,
     CreatedBy      INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     CreatedOn      [DATETIME] NULL,
     [ModifiedBy]   INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn]   [DATETIME] NULL
  )