﻿CREATE TABLE [dbo].[Program]
  (
     [ProgramId]      [INT] IDENTITY(1, 1) PRIMARY KEY,
     [NAME]           [VARCHAR](500) NOT NULL,
     [OrganizationId] [INT] NOT NULL FOREIGN KEY REFERENCES [dbo].[Organizations] ([OrganizationId]),
     [IsActive]       [BIT] NOT NULL,
     [CreatedBy]      INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [CreatedOn]      [DATETIME] NULL,
     [ModifiedBy]     INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn]     [DATETIME] NULL
  ) 
