﻿CREATE TABLE UserClients
  (
     ClientId         INT PRIMARY KEY IDENTITY(1, 1),
     UserId           INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]) UNIQUE NOT NULL,
     PackageId        INT FOREIGN KEY REFERENCES [dbo].[UserPackages] ([PackageId]) NOT NULL,
     BatchId          INT FOREIGN KEY REFERENCES [dbo].[Batch] ([BatchId]) NULL,
     ProgramId        INT FOREIGN KEY REFERENCES [dbo].[Program] ([ProgramId]) NULL,
     PackagePrice     MONEY,
     Discount         MONEY,
     TotalPrice       MONEY,
     AmountPaid       MONEY,
     StartDate        DATETIME,
     EndDate          DATETIME,
     PaymentCompleted BIT,
     CreatedBy        INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     CreatedOn        DATETIME NULL,
     ModifiedBy       INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     ModifiedOn       DATETIME NULL
  )