﻿CREATE TABLE [dbo].[AdminRoles]
  (
     [RoleId]     [INT] IDENTITY(1, 1) PRIMARY KEY,
     [RoleName]   [NVARCHAR](100) NOT NULL,
     [IsActive]   [BIT] NOT NULL,
     CreatedBy    INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     CreatedOn    [DATETIME] NULL,
     [ModifiedBy] INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn] [DATETIME] NULL
  )