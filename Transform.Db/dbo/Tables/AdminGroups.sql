﻿CREATE TABLE AdminGroups
  (
     GroupId      INT IDENTITY(1, 1) PRIMARY KEY,
     GroupName    NVARCHAR(150) NOT NULL,
     IsActive     BIT NOT NULL,
     CreatedBy    INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     CreatedOn    [DATETIME] NULL,
     [ModifiedBy] INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn] [DATETIME] NULL
  )