﻿CREATE TABLE EnquiryForm
  (
     EnquiryFormId  INT IDENTITY(1, 1) PRIMARY KEY,
     FirstName      [VARCHAR](200) NOT NULL,
     LastName       [VARCHAR](200) NULL,
     Mobile         [VARCHAR](10) NOT NULL,
     Email          [VARCHAR](100),
     Gender         [CHAR](1) NULL,
     Address        [NVARCHAR](1000) NULL,
     TrailDate      DATETIME NULL,
     FollowUpDate   DATETIME NULL,
     ReferBy        INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]) NULL,
     PackageId      INT FOREIGN KEY REFERENCES [dbo].[UserPackages] ([PackageId]) NULL,
     ResponseId     INT FOREIGN KEY REFERENCES [dbo].[ClientResponse] ([ResponseId]) NOT NULL,
     OrganizationId [INT] FOREIGN KEY REFERENCES [dbo].[Organizations] ([OrganizationId]) NOT NULL,
     IsActive       [BIT] NOT NULL,
     CreatedBy      INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     CreatedOn      [DATETIME] NULL,
     [ModifiedBy]   INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn]   [DATETIME] NULL
  )