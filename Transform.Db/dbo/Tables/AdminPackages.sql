﻿CREATE TABLE [dbo].[AdminPackages]
  (
     [PackageId]      [INT] IDENTITY(1, 1) PRIMARY KEY,
     [PackageName]    [VARCHAR](1000) NOT NULL,
     [PackagePrice]   [MONEY] NOT NULL,
     [Validity]       [INT] NOT NULL,
     [Description]    [NVARCHAR](max) NULL,
     [SubBranches]    [BIT] NULL,
     [SubBranchCount] [INT] NULL,
     GroupId          INT FOREIGN KEY REFERENCES AdminGroups(GroupId) NOT NULL,
     [IsActive]       [BIT] NOT NULL,
     CreatedBy        INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     CreatedOn        [DATETIME] NULL,
     [ModifiedBy]     INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn]     [DATETIME] NULL
  )