﻿CREATE TABLE ClientResponse
  (
     ResponseId   INT IDENTITY(1, 1) PRIMARY KEY,
     ResponseName VARCHAR(100),
     IsActive     [BIT] NOT NULL
  )