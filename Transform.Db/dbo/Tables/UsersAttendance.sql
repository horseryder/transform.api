﻿CREATE TABLE UsersAttendance
  (
     AttendanceId  INT PRIMARY KEY IDENTITY(1, 1),
     UserId        INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]) NOT NULL,
     ClockDateTime DATETIME NOT NULL,
     IsClockIn     BIT NOT NULL,
  )