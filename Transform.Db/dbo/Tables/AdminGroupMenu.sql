﻿CREATE TABLE [dbo].[AdminGroupMenu]
  (
     [GroupMenuId] [INT] IDENTITY(1, 1) PRIMARY KEY,
     [GroupId]     [INT] FOREIGN KEY REFERENCES [dbo].[AdminGroups] ([GroupId]) NOT NULL,
     [MenuId]      INT FOREIGN KEY REFERENCES [dbo].[Menus] ([MenuId]) NOT NULL,
	 [IsIndeterminate] [BIT] NULL,
     [IsActive]    [BIT] NOT NULL,
     CreatedBy     INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     CreatedOn     [DATETIME] NULL,
     [ModifiedBy]  INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn]  [DATETIME] NULL
  )