﻿CREATE TABLE [dbo].[Batch]
  (
     [BatchId]        [INT] IDENTITY(1, 1) PRIMARY KEY,
     [BatchName]      [VARCHAR](100) NOT NULL,
     [StartTime]      [DATETIME] NULL,
     [EndTime]        [DATETIME] NULL,
     [OrganizationId] [INT] NOT NULL FOREIGN KEY REFERENCES [dbo].[Organizations] ([OrganizationId]),
     [IsActive]       [BIT] NOT NULL,
     [CreatedBy]      INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [CreatedOn]      [DATETIME] NULL,
     [ModifiedBy]     INT FOREIGN KEY REFERENCES [dbo].[Users] ([UserId]),
     [ModifiedOn]     [DATETIME] NULL
  )