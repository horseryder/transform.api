﻿namespace Transform.Common.Constants
{
    public static class TableConstants
    {
        public const string Organizations = "Organizations";
        public const string Users = "Users";
        public const string AdminPackages = "AdminPackages";
        public const string UserPackages = "UserPackages";
        public const string UserOrganizations = "UserOrganizations";
        public const string UserRoles = "UserRoles";
        public const string Groups = "Groups";
        public const string Menus = "Menus";
        public const string GroupMenus = "GroupMenus";
        public const string UserEmployees = "UserEmployees";
        public const string AdminGroups = "AdminGroups";
        public const string MasterGroupMenus = "MasterGroupMenus";
        public const string ClientResponse = "ClientResponse";
        public const string EnquiryForm = "EnquiryForm";
        public const string Batch = "Batch";
        public const string Program = "Program";
        public const string UserClients = "UserClients";
        public const string Equipments = "Equipments";

    }
}
