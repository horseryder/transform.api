﻿namespace Transform.Common.Constants
{
    public static class ApplicationConstants
    {

        #region DropDownConstants

        public const string DDRoles = "roles";
        public const string DDPackages = "packages";
        public const string DDUserRoles = "userRoles";
        public const string DDUserGroups = "usergroups";
        public const string DDAdminGroups = "admingroups";
        public const string SubOrganization = "subOrganizations";
        public const string ClientResponse = "clientResponse";
        public const string UserPackages = "userPackages";
        public const string AllOrganizations = "allOrganizations";
        public const string Program = "Program";
        public const string Batch = "Batch";


        #endregion

        #region FolderConstants
        public const string Users = "Users";
        public const string Logo = "Logo";

        #endregion
    }
}
