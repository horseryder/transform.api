﻿
namespace Transform.Common.Constants
{
    public enum CategoryTypes
    {
        Equipment = 1,
        Expense = 2,
        Workout = 3,
    }
}

