﻿using System;
using System.IO;

namespace Transform.Common.ImageUpload
{
    public static class ImageUpload
    {
        public static string Upload(this string baseFormat, string folder)
        {
            string base64imageString = baseFormat.Split(',')[1];
            string DirectoryPath = $@"E:\Transform\transform.api\Trasnform.Api\Images\{folder}\";
            if (!Directory.Exists(DirectoryPath))
            {
                Directory.CreateDirectory(DirectoryPath);
            }
            var FileName = string.Format(@"{0}", Guid.NewGuid());

            //Generate unique filename
            string filepath = FileName + ".jpeg";
            File.WriteAllBytes(DirectoryPath + filepath, Convert.FromBase64String(base64imageString));
            return filepath;
        }

        public static string ImageToBase64(this string imageName, string folder)
        {
            string imgPath = System.IO.Path.GetFullPath($".\\Images\\{folder}\\{imageName}");
            byte[] imageBytes = System.IO.File.ReadAllBytes(imgPath);
            string base64String = Convert.ToBase64String(imageBytes);
            return base64String;
        }
    }
}
