﻿// <copyright file="StringExtensions.cs" company="Agility E Services">
// Copyright (c) Agility E Services. All rights reserved.
// </copyright>

namespace Transform.Common.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using Newtonsoft.Json;

    public static class StringExtensions
    {
        public static string GetFirstWord(this string stringLine)
        {
            RaiseExceptionIfStringEmptyOrNull(stringLine);
            if (stringLine.Contains(" "))
            {
                stringLine = stringLine.Substring(0, stringLine.IndexOf(" "));
            }

            return stringLine;
        }

        public static string GetFirstLetter(this string stringLine)
        {
            RaiseExceptionIfStringEmptyOrNull(stringLine);
            return stringLine[0].ToString();
        }

        public static string ToDateTime(this string dateTime)
        {
            if (!string.IsNullOrEmpty(dateTime))
            {
                return DateTime.ParseExact(dateTime, "M/d/yyyy h:mm:ss tt", null).ToString("yyyy-MM-ddTHH:mm:ss.000Z");
            }
            else
            {
                return string.Empty;
            }
        }

        public static object GetValueFromDictionary(this string key, IDictionary<string, object> dictionary)
        {
            if (dictionary != null && (dictionary.ContainsKey(key) && dictionary[key] != null && dictionary[key].ToString() != string.Empty))
            {
                    return dictionary[key].ToString();
            }

            return DBNull.Value;
        }

        public static T GetJsonObject<T>(this string jsonString)
        {
            return JsonConvert.DeserializeObject<T>(jsonString);
        }

        public static bool IsEqual(this string source, string checkWith)
        {
            return string.Equals(source, checkWith, StringComparison.OrdinalIgnoreCase);
        }

        public static bool IsEqual(this IEnumerable<string> source, string checkWith)
        {
            return source.Contains(checkWith, StringComparer.OrdinalIgnoreCase);
        }

        public static void VerifyNotNull(this string targetObject, bool isNullOrWhiteSpace, bool isNullOrEmpty, string paramName = null)
        {
            if (isNullOrWhiteSpace && string.IsNullOrWhiteSpace(targetObject))
            {
                    throw new ArgumentNullException(paramName ?? nameof(targetObject), "Argument Null or White Space Exception");
            }

            if (isNullOrEmpty && string.IsNullOrEmpty(targetObject))
            {
                    throw new ArgumentNullException(paramName ?? nameof(targetObject), "Argument Null or Empty Exception");
            }

            if (!isNullOrWhiteSpace && !isNullOrEmpty && (targetObject == null))
            {
                    throw new ArgumentNullException(paramName ?? nameof(targetObject));
            }
        }

        public static T Deserialize<T>(this string targetObject)
        {
            return JsonConvert.DeserializeObject<T>(targetObject);
        }

        public static SqlDbType ParseToSqlDbType(this string str)
        {
            switch (str)
            {
                case "N":
                    return SqlDbType.Int;

                case "S":
                    return SqlDbType.NVarChar;

                case "D":
                    return SqlDbType.Date;

                case "B":
                    return SqlDbType.Bit;

                case "C":
                    return SqlDbType.Decimal;

                case "T":
                    return SqlDbType.DateTime;

                case "M":
                    return SqlDbType.Time;

                default:
                    return SqlDbType.NVarChar;
            }
        }

        public static string RemoveTrailingSlash(this string url)
        {
            if (url != null && url.EndsWith("/"))
            {
                url = url.Substring(0, url.Length - 1);
            }

            return url;
        }

        public static bool IsMissing(this string value)
        {
            return string.IsNullOrWhiteSpace(value);
        }

        public static bool IsPresent(this string value)
        {
            return !string.IsNullOrWhiteSpace(value);
        }

        public static bool ToBoolean(string value)
        {
            bool result;
            bool.TryParse(value, out result);
            return result;
        }

        public static int GetFormIdFromFormCode(this string formCode)
        {
            VerifyNotNull(formCode, true, true);

            var parts = formCode.Split('-');

            return int.Parse(parts.LastOrDefault());
        }

        private static void RaiseExceptionIfStringEmptyOrNull(string stringLine)
        {
            if (stringLine == null || stringLine.Trim() == string.Empty)
            {
                throw new ArgumentNullException(stringLine, "Input cann't be null or Empty");
            }
        }
    }
}
