﻿using System.Linq;
using System.Threading.Tasks;
using Transform.Models.Roles;

namespace Transform.Services.Services.RolesService
{
    public interface IRolesService
    {
        IQueryable<RolesModel> Get();

        RolesModel GetUsersOrganizationRole(int userId, int organizationId);

        RolesModel GetUsersPrimaryOrganizationRole(int userId);
    }
}
