﻿using System.Linq;
using Transform.Entities;
using Transform.Models.Roles;

namespace Transform.Services.Services.RolesService
{
    public class RolesService : IRolesService
    {
        private readonly TransformContext _transformContext;

        public RolesService(TransformContext transformContext)
        {
            _transformContext = transformContext;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IQueryable<RolesModel> Get()
        {
            var response = from role in _transformContext.UserRoles
                           select new RolesModel
                           {
                               RoleId = role.RoleId,
                               RoleName = role.RoleName,
                               IsActive = role.IsActive
                           };
            return response;
        }

        public RolesModel GetUsersOrganizationRole(int userId, int organizationId)
        {
            return (from userOrganization in _transformContext.UserOrganizations.Where(x => x.UserId == userId && x.OrganizationId == organizationId)
                    from role in _transformContext.UserRoles.Where(x => x.RoleId == userOrganization.RoleId).DefaultIfEmpty()
                    select new RolesModel
                    {
                        RoleId = role.RoleId,
                        RoleName = role.RoleName,
                        IsActive = role.IsActive
                    }).FirstOrDefault();
        }

        public RolesModel GetUsersPrimaryOrganizationRole(int userId)
        {
            return (from userOrganization in _transformContext.UserEmployees.Where(x => x.UserId == userId)
                    from role in _transformContext.UserRoles.Where(x => x.RoleId == userOrganization.RoleId).DefaultIfEmpty()
                    select new RolesModel
                    {
                        RoleId = role.RoleId,
                        RoleName = role.RoleName,
                        IsActive = role.IsActive
                    }).FirstOrDefault();
        }
    }
}
