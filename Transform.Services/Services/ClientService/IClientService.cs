﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transform.Models.Client;
using Transform.Models.Users;

namespace Transform.Services.Services.ClientService
{
    public interface IClientService
    {
        Task<UsersModel> SaveClientDetails(UsersModel usersModel);
        IQueryable<ClientListModel> GetUserClientList();
        Task<UserClientModel> GetUserClients(int userId);
        Task<ClientsPackageModel> SaveClientsPackage(ClientsPackageModel clientsPackageModel);
        Task SaveClientsPaymentInfo(ClientsPackageModel clientsPackageModel);
    }
}
