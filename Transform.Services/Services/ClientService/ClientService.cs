﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transform.Common.Constants;
using Transform.Common.ImageUpload;
using Transform.Entities;
using Transform.Entities.Entities;
using Transform.Models.Client;
using Transform.Models.ContextModel;
using Transform.Models.Users;
using Transform.Services.Services.UserPackagesService;
using Transform.Services.Services.UsersService;

namespace Transform.Services.Services.ClientService
{
    public class ClientService : IClientService
    {
        private readonly TransformContext _transformContext;
        private readonly DataContext _dataContext;
        private readonly IUsersService _usersService;
        private readonly IUserPackagesService _userPackagesService;


        public ClientService(TransformContext transformContext,
                    DataContext dataContext,
                    IUsersService usersService,
                    IUserPackagesService userPackagesService)
        {
            _transformContext = transformContext;
            _dataContext = dataContext;
            _usersService = usersService;
            _userPackagesService = userPackagesService;
        }

        public async Task<UsersModel> SaveClientDetails(UsersModel usersModel)
        {
            return await this._usersService.SaveUserDetails(usersModel, false);
        }

        public IQueryable<ClientListModel> GetUserClientList()
        {
            var data = from user in _transformContext.Users.Where(x => x.IsMaster == false && !x.IsEmployee && x.OrganizationId == _dataContext.organizationContext.OrganizationId)
                       select new ClientListModel
                       {
                           UserId = user.UserId,
                           FullName = (user.FirstName + " " + (user.LastName ?? string.Empty)).Trim(),
                           Mobile = user.Mobile,
                           Email = user.Email,
                           ProfilePic = user.ProfilePic,
                           Gender = user.Gender.Equals('M') ? "Male" : "Female",
                           Address = user.Address,
                           IsActive = user.IsActive
                       };
            return data;
        }

        public async Task<UserClientModel> GetUserClients(int userId)
        {
            var usersDetailsinfo = this._usersService.GetUserDetails(userId);
            if (usersDetailsinfo == null)
            {
                throw new Exception();
            }
            var clientPackagesInfo = await this.GetUserClientsPackages(userId);
            return new UserClientModel
            {
                UserDetails = usersDetailsinfo,
                ClientPackages = clientPackagesInfo
            };
        }

        public async Task<ClientsPackageModel> SaveClientsPackage(ClientsPackageModel clientsPackageModel)
        {
            var package = await this._userPackagesService.Get(clientsPackageModel.PackageId);
            var ClientsPackageEntity = new UserClients
            {
                ClientId = clientsPackageModel.ClientId,
                UserId = clientsPackageModel.UserId,
                PackageId = clientsPackageModel.PackageId,
                BatchId = clientsPackageModel.BatchId,
                ProgramId = clientsPackageModel.ProgramId,
                PackagePrice = package.PackagePrice,
                StartDate = clientsPackageModel.StartDate,
                EndDate = clientsPackageModel.EndDate.AddMonths(package.Duration),
                PaymentCompleted = false,
                CreatedBy = _dataContext.userContext.UserId,
                CreatedOn = DateTime.UtcNow
            };

            if (ClientsPackageEntity.ClientId == 0)
            {
                _transformContext.UserClients.Add(ClientsPackageEntity);
            }
            else
            {
                _transformContext.Entry(ClientsPackageEntity).State = EntityState.Modified;
            }
            await _transformContext.SaveChangesAsync();
            return await GetUserClientsPackages(clientsPackageModel.UserId);
        }

        public async Task SaveClientsPaymentInfo(ClientsPackageModel clientsPackageModel)
        {
            var client = _transformContext.UserClients.FirstOrDefault(id => id.ClientId == clientsPackageModel.ClientId);
            client.Discount = clientsPackageModel.Discount;
            decimal totalPrice = client.PackagePrice - clientsPackageModel.Discount;
            client.TotalPrice = totalPrice;
            client.AmountPaid = totalPrice;
            client.PaymentCompleted = true;
            client.ModifiedBy = _dataContext.userContext.UserId;
            client.ModifiedOn = DateTime.UtcNow;
            _transformContext.Entry(client).State = EntityState.Modified;

            var user = _transformContext.Users.FirstOrDefault(id => id.UserId == client.UserId);
            user.IsActive = true;
            user.ModifiedBy = _dataContext.userContext.UserId;
            user.ModifiedOn = DateTime.UtcNow;
            _transformContext.Entry(user).State = EntityState.Modified;
            await _transformContext.SaveChangesAsync();
        }

        private async Task<ClientsPackageModel> GetUserClientsPackages(int userId)
        {
            return await this._transformContext.UserClients.Where(x => x.UserId == userId).Select((user) => new ClientsPackageModel
            {
                ClientId = user.ClientId,
                UserId = userId,
                PackageId = user.PackageId,
                BatchId = user.BatchId,
                ProgramId = user.ProgramId,
                PackagePrice = user.PackagePrice,
                Discount = user.Discount,
                TotalPrice = user.TotalPrice,
                AmountPaid = user.AmountPaid,
                StartDate = user.StartDate,
                EndDate = user.EndDate,
                PaymentCompleted = user.PaymentCompleted
            }).FirstOrDefaultAsync();
        }
    }
}
