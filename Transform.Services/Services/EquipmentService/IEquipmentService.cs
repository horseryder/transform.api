﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transform.Models.Equipments;

namespace Transform.Services.Services.EquipmentService
{
    public interface IEquipmentService
    {
        IQueryable<EquipmentsModel> GetEquipemnts();
        Task<EquipmentsModel> GetEquipemntsDeatils(int equipmentId);
        Task Save(EquipmentsModel equipmentsModel);
    }
}
