﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transform.Entities;
using Transform.Entities.Entities;
using Transform.Models.ContextModel;
using Transform.Models.Equipments;

namespace Transform.Services.Services.EquipmentService
{
    public class EquipmentService : IEquipmentService
    {
        private readonly TransformContext _transformContext;
        private readonly DataContext _dataContext;

        public EquipmentService(TransformContext transformContext,
                    DataContext dataContext)
        {
            _transformContext = transformContext;
            _dataContext = dataContext;
        }

        public IQueryable<EquipmentsModel> GetEquipemnts()
        {
            var category = from equ in _transformContext.Equipments.Where(e => e.OrganizationId == _dataContext.organizationContext.ParentOrganizationId)
                           from cat in _transformContext.Category.Where(c => c.OrganizationId == _dataContext.organizationContext.ParentOrganizationId && c.CategoryId == equ.CategoryId).DefaultIfEmpty()
                           orderby equ.Name ascending
                           select new EquipmentsModel
                           {
                               EquipmentId = equ.EquipmentId,
                               Name = equ.Name,
                               Description = equ.Description,
                               Price = equ.Price,
                               InStock = equ.InStock,
                               CategoryId = equ.CategoryId,
                               CategoryName = cat.Name,
                               IsActive = equ.IsActive
                           };
            return category;
        }

        public async Task<EquipmentsModel> GetEquipemntsDeatils(int equipmentId)
        {
            return await (from equ in _transformContext.Equipments.Where(e => e.OrganizationId == _dataContext.organizationContext.ParentOrganizationId && e.EquipmentId == equipmentId)
                          orderby equ.Name ascending
                          select new EquipmentsModel
                          {
                              EquipmentId = equ.EquipmentId,
                              Name = equ.Name,
                              Description = equ.Description,
                              Price = equ.Price,
                              InStock = equ.InStock,
                              CategoryId = equ.CategoryId,
                              IsActive = equ.IsActive
                          }).FirstOrDefaultAsync();
        }

        public async Task Save(EquipmentsModel equipmentsModel)
        {
            var equipments = new Equipments
            {
                EquipmentId = equipmentsModel.EquipmentId,
                Name = equipmentsModel.Name,
                Description = equipmentsModel.Description,
                Price = equipmentsModel.Price,
                InStock = equipmentsModel.InStock,
                CategoryId = equipmentsModel.CategoryId,
                IsActive = equipmentsModel.IsActive,
                OrganizationId = Convert.ToInt32(_dataContext.organizationContext.ParentOrganizationId),
                CreatedBy = _dataContext.userContext.UserId,
                CreatedOn = DateTime.UtcNow
            };

            if (equipments.EquipmentId == 0)
            {
                _transformContext.Equipments.Add(equipments);
            }
            else
            {
                _transformContext.Entry(equipments).State = EntityState.Modified;
            }

            await _transformContext.SaveChangesAsync();
        }

    }
}
