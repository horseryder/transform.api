﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Transform.Entities;
using Transform.Entities.Entities;
using Transform.Models.ContextModel;
using Transform.Models.UserPackages;

namespace Transform.Services.Services.UserPackagesService
{
    public class UserPackagesService : IUserPackagesService
    {
        private readonly TransformContext _transformContext;
        private readonly DataContext _dataContext;


        public UserPackagesService(TransformContext transformContext, DataContext dataContext)
        {
            _transformContext = transformContext;
            _dataContext = dataContext;
        }

        public IQueryable<UserPackagesModel> Get()
        {
            var response = from packages in _transformContext.UserPackages
                           where packages.OrganizationId == _dataContext.organizationContext.ParentOrganizationId
                           && !packages.IsArchive
                           orderby packages.PackageName ascending
                           select new UserPackagesModel
                           {
                               PackageId = packages.PackageId,
                               PackageName = packages.PackageName,
                               PackagePrice = packages.PackagePrice,
                               Duration = packages.Duration,
                               Description = packages.Description,
                               IsActive = packages.IsActive,
                               OrganizationId = packages.OrganizationId
                           };
            return response;
        }

        public async Task<UserPackagesModel> Get(int id)
        {
            var query = from packages in _transformContext.UserPackages
                        where packages.PackageId == id
                        orderby packages.PackageName ascending
                        select new UserPackagesModel
                        {
                            PackageId = packages.PackageId,
                            PackageName = packages.PackageName,
                            PackagePrice = packages.PackagePrice,
                            Duration = packages.Duration,
                            Description = packages.Description,
                            IsActive = packages.IsActive,
                            OrganizationId = packages.OrganizationId
                        };


            return await query.FirstOrDefaultAsync();
        }

        public async Task Save(UserPackagesModel userPackagesModel)
        {
            var userPackagesEntity = new UserPackages
            {
                PackageId = userPackagesModel.PackageId,
                PackageName = userPackagesModel.PackageName,
                Description = userPackagesModel.Description,
                IsActive = userPackagesModel.IsActive,
                OrganizationId = Convert.ToInt32(_dataContext.organizationContext.ParentOrganizationId),
                PackagePrice = userPackagesModel.PackagePrice,
                Duration = userPackagesModel.Duration,
                IsArchive = false,
                CreatedBy = _dataContext.userContext.UserId,
                CreatedOn = DateTime.UtcNow
            };

            if (userPackagesEntity.PackageId != 0)
            {
                var package = await _transformContext.UserPackages.FirstOrDefaultAsync(x => x.PackageId == userPackagesEntity.PackageId);
                package.IsArchive = true;
                package.ModifiedBy = null;
                package.ModifiedOn = DateTime.UtcNow;
                userPackagesEntity.PackageId = 0;
            }

            _transformContext.UserPackages.Add(userPackagesEntity);
            await _transformContext.SaveChangesAsync();
        }

    }
}
