﻿using System.Linq;
using System.Threading.Tasks;
using Transform.Models.UserPackages;

namespace Transform.Services.Services.UserPackagesService
{
    public interface IUserPackagesService
    {
        IQueryable<UserPackagesModel> Get();

        Task<UserPackagesModel> Get(int id);

        Task Save(UserPackagesModel userPackagesModel);
    }
}
