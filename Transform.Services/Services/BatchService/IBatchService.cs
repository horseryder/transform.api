﻿using System.Linq;
using System.Threading.Tasks;
using Transform.Models.Batch;

namespace Transform.Services.Services.BatchService
{
    public interface IBatchService
    {
        IQueryable<BatchModel> Get();
        Task<BatchModel> Get(int batchId);
        Task Save(BatchModel batchModel);
    }
}
