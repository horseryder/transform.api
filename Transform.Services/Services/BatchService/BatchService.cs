﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Transform.Entities;
using Transform.Entities.Entities;
using Transform.Models.Batch;
using Transform.Models.ContextModel;

namespace Transform.Services.Services.BatchService
{
    public class BatchService : IBatchService
    {
        private readonly TransformContext _transformContext;
        private readonly DataContext _dataContext;
        public BatchService(TransformContext transformContext, DataContext dataContext)
        {
            _transformContext = transformContext;
            _dataContext = dataContext;
        }

        public IQueryable<BatchModel> Get()
        {
            var response = from batch in _transformContext.Batch
                           where batch.OrganizationId == _dataContext.organizationContext.OrganizationId
                           orderby batch.BatchName ascending
                           select new BatchModel
                           {
                               BatchId = batch.BatchId,
                               BatchName = batch.BatchName,
                               StartTime = String.Format("{0:t}", batch.StartTime),
                               EndTime = String.Format("{0:t}", batch.EndTime),
                               IsActive = batch.IsActive
                           };
            return response;
        }

        public async Task<BatchModel> Get(int batchId)
        {
            var query = from batch in _transformContext.Batch
                        where batch.BatchId == batchId
                        && batch.OrganizationId == _dataContext.organizationContext.OrganizationId
                        orderby batch.BatchName ascending
                        select new BatchModel
                        {
                            BatchId = batch.BatchId,
                            BatchName = batch.BatchName,
                            StartTime = batch.StartTime.ToString("MM/dd/yyyy HH:mm"),
                            EndTime = batch.EndTime.ToString("MM/dd/yyyy HH:mm"),
                            IsActive = batch.IsActive
                        };


            return await query.FirstOrDefaultAsync();
        }

        public async Task Save(BatchModel batchModel)
        {
            var batchEntity = new Batch
            {
                BatchId = batchModel.BatchId,
                BatchName = batchModel.BatchName,
                StartTime = Convert.ToDateTime(batchModel.StartTime),
                EndTime = Convert.ToDateTime(batchModel.EndTime),
                IsActive = batchModel.IsActive,
                OrganizationId = Convert.ToInt32(_dataContext.organizationContext.ParentOrganizationId),
                CreatedBy = _dataContext.userContext.UserId,
                CreatedOn = DateTime.UtcNow
            };

            if (batchEntity.BatchId == 0)
            {
                _transformContext.Batch.Add(batchEntity);
            }
            else
            {
                _transformContext.Entry(batchEntity).State = EntityState.Modified;
            }

            await _transformContext.SaveChangesAsync();
        }
    }
}
