﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Transform.Entities;
using Transform.Entities.Entities;
using Transform.Models.ContextModel;
using Transform.Models.UserGroups;

namespace Transform.Services.Services.GroupService
{
    public class GroupService : IGroupService
    {
        private readonly TransformContext _transformContext;
        private readonly DataContext _dataContext;

        public GroupService(TransformContext transformContext, DataContext dataContext)
        {
            _dataContext = dataContext;
            _transformContext = transformContext;
        }

        public IQueryable<GroupsModel> GetAdminGroups()
        {
            var response = from groups in _transformContext.AdminGroups
                           orderby groups.GroupName ascending
                           select new GroupsModel
                           {
                               GroupId = groups.GroupId,
                               GroupName = groups.GroupName,
                               IsActive = groups.IsActive
                           };
            return response;
        }

        public IQueryable<GroupsModel> GetUserGroups()
        {
            var response = from groups in _transformContext.UserGroups
                           where groups.OrganizationId == _dataContext.organizationContext.ParentOrganizationId
                           orderby groups.GroupName ascending
                           select new GroupsModel
                           {
                               GroupId = groups.GroupId,
                               GroupName = groups.GroupName,
                               IsActive = groups.IsActive
                           };
            return response;
        }

        public async Task SaveAdminGroups(GroupsModel groupsModel)
        {
            var groupsEntity = new AdminGroups
            {
                GroupId = groupsModel.GroupId,
                GroupName = groupsModel.GroupName,
                IsActive = groupsModel.IsActive,
                CreatedBy = _dataContext.userContext.UserId,
                CreatedOn = DateTime.UtcNow
            };

            if (groupsEntity.GroupId == 0)
            {
                _transformContext.AdminGroups.Add(groupsEntity);
            }
            else
            {
                _transformContext.Entry(groupsEntity).State = EntityState.Modified;
            }

            await _transformContext.SaveChangesAsync();
        }

        public async Task SaveUserGroups(GroupsModel groupsModel)
        {
            var groupsEntity = new UserGroups
            {
                GroupId = groupsModel.GroupId,
                GroupName = groupsModel.GroupName,
                IsActive = groupsModel.IsActive,
                OrganizationId =_dataContext.organizationContext.ParentOrganizationId,
                CreatedBy = _dataContext.userContext.UserId,
                CreatedOn = DateTime.UtcNow
            };

            if (groupsEntity.GroupId == 0)
            {
                _transformContext.UserGroups.Add(groupsEntity);
            }
            else
            {
                _transformContext.Entry(groupsEntity).State = EntityState.Modified;
            }

            await _transformContext.SaveChangesAsync();
        }



    }
}
