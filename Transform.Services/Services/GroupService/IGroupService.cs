﻿using System.Linq;
using System.Threading.Tasks;
using Transform.Models.UserGroups;

namespace Transform.Services.Services.GroupService
{
    public interface IGroupService
    {
        IQueryable<GroupsModel> GetAdminGroups();

        IQueryable<GroupsModel> GetUserGroups();

        Task SaveAdminGroups(GroupsModel groupsModel);

        Task SaveUserGroups(GroupsModel groupsModel);
    }
}
