﻿using System;
using System.Linq;
using Transform.Common.Constants;
using Transform.Entities;
using Transform.Models.ContextModel;
using Transform.Models.DropDown;

namespace Transform.Services.Services.DropDownService
{
    public class DropDownService : IDropDownService
    {
        private readonly TransformContext _transformContext;
        private readonly DataContext _dataContext;

        public DropDownService(TransformContext transformContext, DataContext dataContext)
        {
            _transformContext = transformContext;
            _dataContext = dataContext;
        }

        public DropDownList GetMasteData(string[] dropDowns)
        {
            var dropDownList = new DropDownList();
            int parentOrganizationId = Convert.ToInt32(this._dataContext.organizationContext != null ? this._dataContext.organizationContext.ParentOrganizationId : 0);
            int organizationId = Convert.ToInt32(this._dataContext.organizationContext != null ? this._dataContext.organizationContext.OrganizationId : 0);
            foreach (var item in dropDowns)
            {
                switch (item)
                {
                    case ApplicationConstants.DDRoles:
                        dropDownList.Roles = this._transformContext.UserRoles.Where(act => act.IsActive).Select(role => new DropDownModel
                        {
                            Id = role.RoleId,
                            Value = role.RoleName
                        });
                        break;

                    case ApplicationConstants.DDPackages:
                        dropDownList.Packages = this._transformContext.AdminPackages.Where(act => act.IsActive).Select(package => new DropDownModel
                        {
                            Id = package.PackageId,
                            Value = package.PackageName
                        });
                        break;
                    case ApplicationConstants.DDUserRoles:
                        dropDownList.UserRoles = this._transformContext.UserRoles.Where(act => act.IsActive && act.OrganizationId == parentOrganizationId).Select(role => new DropDownModel
                        {
                            Id = role.RoleId,
                            Value = role.RoleName
                        });
                        break;
                    case ApplicationConstants.DDUserGroups:
                        dropDownList.UserGroups = this._transformContext.UserGroups.Where(act => act.IsActive && act.OrganizationId == parentOrganizationId).Select(group => new DropDownModel
                        {
                            Id = group.GroupId,
                            Value = group.GroupName
                        });
                        break;
                    case ApplicationConstants.DDAdminGroups:
                        dropDownList.AdminGroups = this._transformContext.AdminGroups.Where(act => act.IsActive).Select(group => new DropDownModel
                        {
                            Id = group.GroupId,
                            Value = group.GroupName
                        });
                        break;
                    case ApplicationConstants.SubOrganization:
                        dropDownList.SubOrganization = this._transformContext.Organizations.Where(act => act.IsActive && !act.IsMaster && act.ParentOrganizationId == parentOrganizationId).Select(org => new DropDownModel
                        {
                            Id = org.OrganizationId,
                            Value = org.Name
                        });
                        break;
                    case ApplicationConstants.ClientResponse:
                        dropDownList.ClientResponse = this._transformContext.ClientResponse.Where(x => x.IsActive).Select(response => new DropDownModel
                        {
                            Id = response.ResponseId,
                            Value = response.ResponseName
                        });
                        break;
                    case ApplicationConstants.UserPackages:
                        dropDownList.UserPackages = this._transformContext.UserPackages.Where(x => x.IsActive && !x.IsArchive && x.OrganizationId == _dataContext.organizationContext.OrganizationId).Select(pck => new DropDownModel
                        {
                            Id = pck.PackageId,
                            Value = pck.PackageName
                        });
                        break;
                    case ApplicationConstants.AllOrganizations:
                        dropDownList.SubOrganization = this._transformContext.Organizations.Where(act => act.IsActive && (act.ParentOrganizationId == parentOrganizationId || act.OrganizationId == parentOrganizationId)).Select(org => new DropDownModel
                        {
                            Id = org.OrganizationId,
                            Value = org.Name
                        });
                        break;
                    case ApplicationConstants.Program:
                        dropDownList.Program = this._transformContext.Programs.Where(act => act.IsActive && act.OrganizationId == organizationId).Select(program => new DropDownModel
                        {
                            Id = program.ProgramId,
                            Value = program.Name
                        });
                        break;
                    case ApplicationConstants.Batch:
                        dropDownList.Batch = this._transformContext.Batch.Where(act => act.IsActive && act.OrganizationId == organizationId).Select(program => new DropDownModel
                        {
                            Id = program.BatchId,
                            Value = program.BatchName
                        });
                        break;
                }
            }

            return dropDownList;
        }
    }
}
