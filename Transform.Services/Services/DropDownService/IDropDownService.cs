﻿using System.Collections.Generic;
using Transform.Models.DropDown;

namespace Transform.Services.Services.DropDownService
{
    public interface IDropDownService
    {
        DropDownList GetMasteData(string[] dropDowns);
    }
}
