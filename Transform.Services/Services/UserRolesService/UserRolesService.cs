﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Transform.Entities;
using Transform.Entities.Entities;
using Transform.Models.ContextModel;
using Transform.Models.UserRoles;

namespace Transform.Services.Services.UserRolesService
{
    public class UserRolesService : IUserRolesService
    {
        private readonly TransformContext _transformContext;
        private readonly DataContext _dataContext;

        public UserRolesService(TransformContext transformContext, DataContext dataContext)
        {
            _transformContext = transformContext;
            _dataContext = dataContext;
        }

        public IQueryable<UserRolesModel> Get()
        {
            var response = from role in _transformContext.UserRoles
                           where role.OrganizationId == _dataContext.organizationContext.ParentOrganizationId
                           orderby role.RoleName ascending
                           select new UserRolesModel
                           {
                               RoleId = role.RoleId,
                               RoleName = role.RoleName,
                               IsActive = role.IsActive
                           };
            return response;
        }

        public async Task<UserRolesModel> Get(int roleId)
        {
            var query = from role in _transformContext.UserRoles
                        where role.RoleId == roleId 
                        && role.OrganizationId == _dataContext.organizationContext.ParentOrganizationId
                        orderby role.RoleName ascending
                        select new UserRolesModel
                        {
                            RoleId = role.RoleId,
                            RoleName = role.RoleName,
                            IsActive = role.IsActive
                        };


            return await query.FirstOrDefaultAsync();
        }

        public async Task Save(UserRolesModel userRolesModel)
        {
            var userRolesEntity = new UserRoles
            {
                RoleId = userRolesModel.RoleId,
                RoleName = userRolesModel.RoleName,
                IsActive = userRolesModel.IsActive,
                OrganizationId = Convert.ToInt32(_dataContext.organizationContext.ParentOrganizationId),
                CreatedBy = _dataContext.userContext.UserId,
                CreatedOn = DateTime.UtcNow
            };

            if (userRolesEntity.RoleId == 0)
            {
                _transformContext.UserRoles.Add(userRolesEntity);
            }
            else
            {
                _transformContext.Entry(userRolesEntity).State = EntityState.Modified;
            }

            await _transformContext.SaveChangesAsync();
        }
    }
}
