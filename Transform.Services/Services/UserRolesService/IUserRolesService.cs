﻿using System.Linq;
using System.Threading.Tasks;
using Transform.Models.UserRoles;

namespace Transform.Services.Services.UserRolesService
{
    public interface IUserRolesService
    {
        IQueryable<UserRolesModel> Get();
        Task<UserRolesModel> Get(int roleId);
        Task Save(UserRolesModel userRolesModel);
    }
}
