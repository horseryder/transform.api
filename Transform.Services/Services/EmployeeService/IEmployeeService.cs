﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transform.Models.DropDown;
using Transform.Models.EmployeesPermission;
using Transform.Models.UserEmployees;
using Transform.Models.Users;

namespace Transform.Services.Services.EmployeeService
{
    public interface IEmployeeService
    {
        Task<UsersModel> SaveEmployeeDetails(UsersModel usersModel);
        Task<EmployeesPermissionModel> SaveEmployeePermissions(EmployeesPermissionModel userEmployeesModel);
        Task<UserEmployeeModel> GetUserEmployees(int userId);
        IQueryable<EmployeeListModel> GetUserEmployeesList();
        IQueryable<DropDownModel> GetClientsbyName(string searchText);
    }
}
