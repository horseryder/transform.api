﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Transform.Common.Constants;
using Transform.Common.ImageUpload;
using Transform.Entities;
using Transform.Entities.Entities;
using Transform.Models.ContextModel;
using Transform.Models.DropDown;
using Transform.Models.EmployeesPermission;
using Transform.Models.UserEmployees;
using Transform.Models.Users;
using Transform.Services.Services.UsersService;

namespace Transform.Services.Services.EmployeeService
{
    public class EmployeeService : IEmployeeService
    {
        private readonly TransformContext _transformContext;
        private readonly DataContext _dataContext;
        private readonly IUsersService _usersService;


        public EmployeeService(TransformContext transformContext,
                            DataContext dataContext,
                            IUsersService usersService)
        {
            _transformContext = transformContext;
            _dataContext = dataContext;
            _usersService = usersService;
        }

        public async Task<UsersModel> SaveEmployeeDetails(UsersModel usersModel)
        {
            return await this._usersService.SaveUserDetails(usersModel, true);

        }

        public async Task<EmployeesPermissionModel> SaveEmployeePermissions(EmployeesPermissionModel userEmployeesModel)
        {
            var userEmployeeEntity = new UserEmployees
            {
                EmployeeId = userEmployeesModel.EmployeeId,
                UserId = userEmployeesModel.UserId,
                GroupId = userEmployeesModel.GroupId,
                RoleId = userEmployeesModel.RoleId,
                IsActive = userEmployeesModel.IsActive,
                CreatedBy = _dataContext.userContext.UserId,
                CreatedOn = DateTime.UtcNow
            };

            if (userEmployeeEntity.EmployeeId == 0)
            {
                _transformContext.UserEmployees.Add(userEmployeeEntity);
            }
            else
            {
                _transformContext.Entry(userEmployeeEntity).State = EntityState.Modified;
            }

            var existingOrganization = _transformContext.UserOrganizations.Where(x => x.UserId == userEmployeesModel.UserId).ToList();
            var deletedOrganization = existingOrganization.Where(org => !userEmployeesModel.OrganizationList
                                   .Select(data => data.OrganizationId).Contains(org.OrganizationId));
            _transformContext.UserOrganizations.RemoveRange(deletedOrganization);

            var updatedOrganizationList = userEmployeesModel.OrganizationList.Where(org => !existingOrganization.Select(x => x.OrganizationId).Contains(org.OrganizationId));

            foreach (var org in updatedOrganizationList)
            {
                var userOrganizationsEntity = new UserOrganizations
                {
                    UserId = userEmployeesModel.UserId,
                    OrganizationId = org.OrganizationId,
                    GroupId = org.GroupId,
                    RoleId = org.RoleId,
                    IsActive = true,
                    CreatedBy = _dataContext.userContext.UserId,
                    CreatedOn = DateTime.UtcNow
                };
                _transformContext.UserOrganizations.Add(userOrganizationsEntity);
            }
            var user = _transformContext.Users.First(a => a.UserId == userEmployeesModel.UserId);
            user.OrganizationId = userEmployeesModel.PrimaryOrganizationId;
            await _transformContext.SaveChangesAsync();
            return await GetUserEmployeesPermission(userEmployeesModel.UserId);
        }

        public async Task<UserEmployeeModel> GetUserEmployees(int userId)
        {
            var usersDetailsinfo = this._usersService.GetUserDetails(userId);
            if (usersDetailsinfo == null)
            {
                throw new Exception();
            }
            var employeesPermissioninfo = await this.GetUserEmployeesPermission(userId);
            return new UserEmployeeModel
            {
                UserDetails = usersDetailsinfo,
                EmployeesPermission = employeesPermissioninfo
            };
        }

        public IQueryable<EmployeeListModel> GetUserEmployeesList()
        {
            var data = from user in _transformContext.Users.Where(x => x.IsMaster == false && x.IsEmployee && x.OrganizationId == _dataContext.organizationContext.OrganizationId)
                       from employee in _transformContext.UserEmployees.Where(x => x.IsActive && x.UserId == user.UserId).DefaultIfEmpty()
                       from groups in _transformContext.UserGroups.Where(x => x.IsActive && x.GroupId == employee.GroupId).DefaultIfEmpty()
                       from role in _transformContext.UserRoles.Where(x => x.IsActive && x.RoleId == employee.RoleId).DefaultIfEmpty()
                       select new EmployeeListModel
                       {
                           UserId = user.UserId,
                           FullName = (user.FirstName + " " + (user.LastName ?? string.Empty)).Trim(),
                           Mobile = user.Mobile,
                           Email = user.Email,
                           ProfilePic = user.ProfilePic,
                           Gender = user.Gender.Equals('M') ? "Male" : "Female",
                           Address = user.Address,
                           IsActive = user.IsActive,
                           RoleName = role.RoleName,
                           GroupName = groups.GroupName
                       };
            return data;
        }

        //add Clients conditions
        public IQueryable<DropDownModel> GetClientsbyName(string searchText)
        {
            var data = from user in _transformContext.Users
                       where user.IsMaster == false && !user.IsEmployee && user.OrganizationId == _dataContext.organizationContext.OrganizationId
                       && (user.FirstName + " " + (user.LastName ?? string.Empty)).Trim().ToLower().Contains(searchText.ToLower())
                       && user.IsActive
                       select new DropDownModel
                       {
                           Id = user.UserId,
                           Value = (user.FirstName + " " + (user.LastName ?? string.Empty)).Trim(),
                       };
            return data;
        }

        private async Task<EmployeesPermissionModel> GetUserEmployeesPermission(int userId)
        {
            return await this._transformContext.UserEmployees.Where(x => x.UserId == userId).Select((user) => new EmployeesPermissionModel
            {
                EmployeeId = user.EmployeeId,
                UserId = userId,
                GroupId = user.GroupId,
                RoleId = user.RoleId,
                IsActive = user.IsActive,
                PrimaryOrganizationId = _usersService.GetUserDetails(userId).OrganizationId ?? 0,
                OrganizationList = _transformContext.UserOrganizations.Where(x => x.UserId == userId).Select((org) => new EmployeeOrganizationModel() { OrganizationId = org.OrganizationId, RoleId = org.RoleId, GroupId = org.GroupId }).ToList()
            }).FirstOrDefaultAsync();
        }

    }
}
