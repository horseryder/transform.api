﻿using System.Linq;
using Transform.Entities;
using System.Collections.Generic;
using Transform.Models.ContextModel;
using Transform.Entities.Entities;
using System;

namespace Transform.Services.Services.ContextService
{
    public class ContextService : IContextService
    {
        private readonly TransformContext _transformContext;

        public ContextService(TransformContext transformContext)
        {
            _transformContext = transformContext;
        }

        public DataContext GetUserOrg(int userId)
        {
            var userDetail = _transformContext.Users.SingleOrDefault(x => x.UserId == userId);

            if (userDetail.IsSysAdmin == true)
            {
                return new DataContext
                {
                    userContext = new UserContext
                    {
                        UserId = userDetail.UserId,
                        IsSysAdmin = true
                    }
                };
            }
            else
            {
                var orgs = _transformContext.Organizations;

                var query = from user in _transformContext.Users
                            join org in _transformContext.Organizations on user.OrganizationId equals org.OrganizationId
                            join employees in _transformContext.UserEmployees on user.UserId equals employees.UserId
                            into userDetails
                            from UserEmployees in userDetails.DefaultIfEmpty()
                            where user.UserId == userId
                            select new DataContext
                            {
                                organizationContext = new OrganizationContext
                                {
                                    OrganizationId = org.OrganizationId,
                                    ParentOrganizationId = org.IsMaster ? org.OrganizationId : org.ParentOrganizationId,
                                    PackageId = org.IsMaster ? Convert.ToInt32(org.PackageId) : Convert.ToInt32(orgs.FirstOrDefault(x => x.OrganizationId == org.ParentOrganizationId).PackageId),
                                    IsMaster = org.IsMaster
                                },
                                userContext = new UserContext
                                {
                                    UserId = user.UserId,
                                    GroupId = UserEmployees.GroupId,
                                    IsSysAdmin = false,
                                    IsMaster = user.IsMaster ?? false
                                }
                            };
                return query.FirstOrDefault();
            }

        }

        private int GetPackageId(Organizations org)
        {
            int? PackageId = org.IsMaster ? org.PackageId : (_transformContext.Organizations.FirstOrDefault(x => x.OrganizationId == org.ParentOrganizationId).PackageId);
            return Convert.ToInt32(PackageId);
        }
    }
}
