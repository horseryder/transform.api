﻿using Transform.Models.ContextModel;

namespace Transform.Services.Services.ContextService
{
    public interface IContextService
    {
        DataContext GetUserOrg(int userId);
    }
}
