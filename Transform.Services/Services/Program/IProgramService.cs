﻿using System.Linq;
using System.Threading.Tasks;
using Transform.Models.Program;

namespace Transform.Services.Services.Program
{
    public interface IProgramService
    {
        IQueryable<ProgramModel> Get();
        Task<ProgramModel> Get(int programId);
        Task Save(ProgramModel programModel);
    }
}
