﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Transform.Entities;
using Transform.Entities.Entities;
using Transform.Models.ContextModel;
using Transform.Models.Program;

namespace Transform.Services.Services.Program
{
    public class ProgramService : IProgramService
    {
        private readonly TransformContext _transformContext;
        private readonly DataContext _dataContext;
        public ProgramService(TransformContext transformContext, DataContext dataContext)
        {
            _transformContext = transformContext;
            _dataContext = dataContext;
        }

        public IQueryable<ProgramModel> Get()
        {
            var response = from program in _transformContext.Programs
                           where program.OrganizationId == _dataContext.organizationContext.OrganizationId
                           orderby program.Name ascending
                           select new ProgramModel
                           {
                               ProgramId = program.ProgramId,
                               Name = program.Name,
                               IsActive = program.IsActive
                           };
            return response;
        }

        public async Task<ProgramModel> Get(int programId)
        {
            var query = from program in _transformContext.Programs
                        where program.ProgramId == programId
                        && program.OrganizationId == _dataContext.organizationContext.OrganizationId
                        orderby program.Name ascending
                        select new ProgramModel
                        {
                            ProgramId = program.ProgramId,
                            Name = program.Name,
                            IsActive = program.IsActive
                        };

            return await query.FirstOrDefaultAsync();
        }

        public async Task Save(ProgramModel programModel)
        {
            var programEntity = new Programs
            {
                ProgramId = programModel.ProgramId,
                Name = programModel.Name,
                IsActive = programModel.IsActive,
                OrganizationId = Convert.ToInt32(_dataContext.organizationContext.OrganizationId),
                CreatedBy = _dataContext.userContext.UserId,
                CreatedOn = DateTime.UtcNow
            };

            if (programEntity.ProgramId == 0)
            {
                _transformContext.Programs.Add(programEntity);
            }
            else
            {
                _transformContext.Entry(programEntity).State = EntityState.Modified;
            }

            await _transformContext.SaveChangesAsync();
        }
    }
}
