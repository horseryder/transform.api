﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Transform.Common.Constants;
using Transform.Common.ImageUpload;
using Transform.Entities;
using Transform.Entities.Entities;
using Transform.Models.ContextModel;
using Transform.Models.Organizations;
using Transform.Models.Users;

namespace Transform.Services.Services.OrganizationsService
{
    public class OrganizationService : IOrganizationService
    {
        private readonly TransformContext _transformContext;
        private readonly DataContext _dataContext;

        public OrganizationService(TransformContext transformContext,
             DataContext dataContext)
        {
            _transformContext = transformContext;
            _dataContext = dataContext;
        }

        #region Admin
        public IQueryable<OrganizationsModel> GetOrganizationsForAdmin()
        {
            var response = from organization in _transformContext.Organizations
                           join packages in _transformContext.AdminPackages on organization.PackageId equals packages.PackageId
                           where organization.ParentOrganizationId == null
                           orderby organization.Name
                           select new OrganizationsModel
                           {
                               OrganizationId = organization.OrganizationId,
                               Name = organization.Name,
                               Address = organization.Address,
                               Logo = organization.Logo,
                               PackageName = packages.PackageName,
                               IsActive = organization.IsActive
                           };
            return response;
        }

        public UserSetupModel Get(int organizationId)
        {
            var response = (from organization in _transformContext.Organizations
                            join packages in _transformContext.AdminPackages on
                            organization.PackageId equals packages.PackageId
                            join user in _transformContext.Users on
                            organization.OrganizationId equals user.OrganizationId
                            where organization.OrganizationId == organizationId
                            select new UserSetupModel
                            {
                                OrganizationModel = new OrganizationsModel
                                {
                                    OrganizationId = organization.OrganizationId,
                                    Name = organization.Name,
                                    Address = organization.Address,
                                    Logo = organization.Logo,
                                    PackageId = organization.PackageId
                                },
                                UsersModel = new UsersModel
                                {
                                    UserId = user.UserId,
                                    FirstName = user.FirstName,
                                    LastName = user.LastName,
                                    Mobile = user.Mobile,
                                    Email = user.Email,
                                    Password = user.Password,
                                    IsMaster = user.IsMaster,
                                    ProfilePic = user.ProfilePic,
                                    Gender = user.Gender,
                                    Address = user.Address,
                                    IsActive = user.IsActive,
                                }
                            }).FirstOrDefault();
            return response;
        }
        #endregion

        #region Portal
        #endregion

        public IQueryable<OrganizationsModel> GetSubOrganizations()
        {
            var response = from organization in _transformContext.Organizations
                           where !organization.IsMaster && organization.ParentOrganizationId == this._dataContext.organizationContext.ParentOrganizationId
                           orderby organization.Name
                           select new OrganizationsModel
                           {
                               OrganizationId = organization.OrganizationId,
                               Name = organization.Name,
                               Address = organization.Address,
                               Logo = organization.Logo,
                               IsActive = organization.IsActive
                           };
            return response;
        }

        public async Task<OrganizationsModel> GetOrganizationDetails(int organizationId)
        {
            var response = from organization in _transformContext.Organizations
                           where !organization.IsMaster && organization.ParentOrganizationId == this._dataContext.organizationContext.ParentOrganizationId
                           && organization.OrganizationId == organizationId
                           orderby organization.Name ascending
                           select new OrganizationsModel
                           {
                               OrganizationId = organization.OrganizationId,
                               Name = organization.Name,
                               PackageId = organization.PackageId,
                               Address = organization.Address,
                               Logo = organization.Logo,
                               IsActive = organization.IsActive
                           };
            return await response.FirstOrDefaultAsync();
        }

        public async Task<int> Save(OrganizationsModel organizationModel)
        {
            var organizationEntity = OrganizationDTO(organizationModel);

            if (organizationEntity.OrganizationId == 0)
            {
                _transformContext.Organizations.Add(organizationEntity);
            }
            else
            {
                _transformContext.Entry(organizationEntity).State = EntityState.Modified;
                if (String.IsNullOrEmpty(organizationModel.Logo))
                {
                    _transformContext.Entry(organizationEntity).Property(x => x.Logo).IsModified = false;
                }
            }

            if (!String.IsNullOrEmpty(organizationModel.Logo))
            {
                organizationEntity.Logo = organizationModel.Logo.Upload(ApplicationConstants.Logo);
            }

            await _transformContext.SaveChangesAsync();
            return organizationEntity.OrganizationId;
        }

        private Organizations OrganizationDTO(OrganizationsModel organizationModel)
        {
            return new Organizations
            {
                OrganizationId = organizationModel.OrganizationId,
                Name = organizationModel.Name,
                Address = organizationModel.Address,
                IsActive = organizationModel.IsActive,
                PackageId = organizationModel.IsMaster ?? false ? organizationModel.PackageId : null,
                CreatedBy = _dataContext.userContext.UserId,
                IsMaster = organizationModel.IsMaster ?? false,
                ParentOrganizationId = organizationModel.IsMaster ?? false ? null : this._dataContext.organizationContext.ParentOrganizationId,
                CreatedOn = DateTime.UtcNow
            };
        }
    }
}

