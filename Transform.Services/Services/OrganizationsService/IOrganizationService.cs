﻿using System.Linq;
using System.Threading.Tasks;
using Transform.Models.Organizations;
using Transform.Models.Users;

namespace Transform.Services.Services.OrganizationsService
{
    public interface IOrganizationService
    {
        IQueryable<OrganizationsModel> GetSubOrganizations();
        Task<OrganizationsModel> GetOrganizationDetails(int organizationId);
        UserSetupModel Get(int organizationId);
        Task<int> Save(OrganizationsModel organizationModel);

        IQueryable<OrganizationsModel> GetOrganizationsForAdmin();
    }
}
