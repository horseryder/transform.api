﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transform.Entities;
using Transform.Entities.Entities;
using Transform.Models.ContextModel;
using Transform.Models.GroupMenu;
using Transform.Services.Services.MenuService;

namespace Transform.Services.Services.GroupMenuService
{
    public class GroupMenuService : IGroupMenuService
    {
        private readonly TransformContext _transformContext;
        private readonly IMenuService _menuService;
        private readonly DataContext _dataContext;


        public GroupMenuService(TransformContext transformContext, IMenuService menuService, DataContext dataContext)
        {
            _transformContext = transformContext;
            _menuService = menuService;
            _dataContext = dataContext;
        }

        public async Task<IQueryable<GroupLevelMenus>> GetUserGroupMenu(int groupId)
        {
            IQueryable<Menus> menus = _menuService.GetOrganizationMenu();
            List<UserGroupMenu> groupMenus = await GetUserGroupMenus(groupId, _dataContext.organizationContext.ParentOrganizationId);

            return GetUserSubMenus(menus, groupMenus, null);
        }

        public async Task<IQueryable<GroupLevelMenus>> GetAdminGroupMenu(int groupId)
        {
            IQueryable<Menus> menus = _transformContext.Menus.Select(data => data);
            List<AdminGroupMenu> groupMenus = await GetAdminGroupMenus(groupId);

            return GetAdminSubMenus(menus, groupMenus, null);
        }

        public async Task SaveAdminGroupMenus(List<GroupMenuEntityModel> groupMenuEntityModel, int groupId)
        {

            var existingGroupMenus = _transformContext.AdminGroupMenu.Where(x => x.GroupId == groupId).ToList();

            // Deleted unchecked menus
            var deletedGroupMenus = existingGroupMenus.Where(gp => !groupMenuEntityModel
                                    .Select(data => data.GroupMenuId).Contains(gp.GroupMenuId));
            _transformContext.AdminGroupMenu.RemoveRange(deletedGroupMenus);

            var updatedGroupMenus = existingGroupMenus.Where(gpMenu => !deletedGroupMenus.Select(x => x.GroupMenuId).Contains(gpMenu.GroupMenuId));

            //Update existing
            foreach (var groupMenu in groupMenuEntityModel.Where(id => id.GroupMenuId > 0))
            {
                var gpMenu = updatedGroupMenus.FirstOrDefault(id => id.GroupMenuId == groupMenu.GroupMenuId);
                gpMenu.IsIndeterminate = groupMenu.IsIndeterminate;
                _transformContext.Entry(gpMenu).State = EntityState.Modified;
            }


            // Insert newly checked menus
            var newGroupMenus = groupMenuEntityModel.Where(gp => gp.GroupMenuId == 0);
            foreach (var menuEntityModel in newGroupMenus)
            {
                var groupMenuEntity = new AdminGroupMenu
                {
                    GroupMenuId = 0,
                    GroupId = groupId,
                    IsActive = true,
                    MenuId = menuEntityModel.MenuId,
                    IsIndeterminate = menuEntityModel.IsIndeterminate,
                    CreatedBy = _dataContext.userContext.UserId,
                    CreatedOn = DateTime.UtcNow
                };
                _transformContext.AdminGroupMenu.Add(groupMenuEntity);
            }

            await _transformContext.SaveChangesAsync();
        }

        public async Task SaveUserGroupMenus(List<GroupMenuEntityModel> groupMenuEntityModel, int groupId)
        {
            int? parentOrganizationId = _dataContext.userContext.IsSysAdmin ? null : _dataContext.organizationContext.ParentOrganizationId;

            var existingGroupMenus = _transformContext.UserGroupMenu.Where(x => x.OrganizationId == parentOrganizationId
                                       && x.GroupId == groupId).ToList();

            // Deleted unchecked menus
            var deletedGroupMenus = existingGroupMenus.Where(gp => !groupMenuEntityModel
                                    .Select(data => data.GroupMenuId).Contains(gp.GroupMenuId));
            _transformContext.UserGroupMenu.RemoveRange(deletedGroupMenus);

            var updatedGroupMenus = existingGroupMenus.Where(gpMenu => !deletedGroupMenus.Select(x => x.GroupMenuId).Contains(gpMenu.GroupMenuId));

            //Update existing
            foreach (var groupMenu in groupMenuEntityModel.Where(id => id.GroupMenuId > 0))
            {
                var gpMenu = updatedGroupMenus.FirstOrDefault(id => id.GroupMenuId == groupMenu.GroupMenuId);
                gpMenu.IsIndeterminate = groupMenu.IsIndeterminate;
                _transformContext.Entry(gpMenu).State = EntityState.Modified;
            }


            // Insert newly checked menus
            var newGroupMenus = groupMenuEntityModel.Where(gp => gp.GroupMenuId == 0);
            foreach (var menuEntityModel in newGroupMenus)
            {
                var groupMenuEntity = new UserGroupMenu
                {
                    GroupMenuId = 0,
                    GroupId = groupId,
                    OrganizationId = parentOrganizationId,
                    IsActive = true,
                    MenuId = menuEntityModel.MenuId,
                    IsIndeterminate = menuEntityModel.IsIndeterminate,
                    CreatedBy = _dataContext.userContext.UserId,
                    CreatedOn = DateTime.UtcNow
                };
                _transformContext.UserGroupMenu.Add(groupMenuEntity);
            }

            await _transformContext.SaveChangesAsync();
        }

        private async Task<List<AdminGroupMenu>> GetAdminGroupMenus(int groupId)
        {
            return await _transformContext.AdminGroupMenu.Where(group => group.GroupId == groupId)
                            .Select(data => data).ToListAsync();
        }

        private async Task<List<UserGroupMenu>> GetUserGroupMenus(int groupId, int? parentOrganizationId)
        {
            return await _transformContext.UserGroupMenu.Where(group => group.GroupId == groupId
                            && group.OrganizationId == parentOrganizationId)
                            .Select(data => data).ToListAsync();
        }

        private IQueryable<GroupLevelMenus> GetAdminSubMenus(IQueryable<Menus> menus, IEnumerable<AdminGroupMenu> groupMenus, int? menuId)
        {
            var response = menus.Where(id => id.ParentMenuId == menuId).Select(data => new GroupLevelMenus
            {
                MenuId = data.MenuId,
                MenuName = data.MenuName,
                RoutePath = data.RoutePath,
                MenuSequence = data.MenuSequence,
                ParentMenuId = data.ParentMenuId,
                IsActive = data.IsActive,
                SubMenus = GetAdminSubMenus(menus, groupMenus, data.MenuId),
                IsChecked = groupMenus.Any(gp => gp.MenuId == data.MenuId),
                GroupMenuId = groupMenus.FirstOrDefault(gp => gp.MenuId == data.MenuId).GroupMenuId,
                IsIndeterminate = groupMenus.FirstOrDefault(gp => gp.MenuId == data.MenuId).IsIndeterminate,
            });

            return response;
        }

        private IQueryable<GroupLevelMenus> GetUserSubMenus(IQueryable<Menus> menus, IEnumerable<UserGroupMenu> groupMenus, int? menuId)
        {
            var response = menus.Where(id => id.ParentMenuId == menuId).Select(data => new GroupLevelMenus
            {
                MenuId = data.MenuId,
                MenuName = data.MenuName,
                RoutePath = data.RoutePath,
                MenuSequence = data.MenuSequence,
                ParentMenuId = data.ParentMenuId,
                IsActive = data.IsActive,
                SubMenus = GetUserSubMenus(menus, groupMenus, data.MenuId),
                IsChecked = groupMenus.Any(gp => gp.MenuId == data.MenuId),
                GroupMenuId = groupMenus.FirstOrDefault(gp => gp.MenuId == data.MenuId).GroupMenuId,
                IsIndeterminate = groupMenus.FirstOrDefault(gp => gp.MenuId == data.MenuId).IsIndeterminate,
            });

            return response;
        }

    }

}
