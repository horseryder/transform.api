﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transform.Models.GroupMenu;

namespace Transform.Services.Services.GroupMenuService
{
    public interface IGroupMenuService
    {
        Task SaveAdminGroupMenus(List<GroupMenuEntityModel> groupMenuEntityModel, int groupId);

        Task SaveUserGroupMenus(List<GroupMenuEntityModel> groupMenuEntityModel, int groupId);

        Task<IQueryable<GroupLevelMenus>> GetAdminGroupMenu(int groupId);

        Task<IQueryable<GroupLevelMenus>> GetUserGroupMenu(int groupId);
    }
}
