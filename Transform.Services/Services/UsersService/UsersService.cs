﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Transform.Entities;
using Transform.Entities.Entities;
using System.ComponentModel.DataAnnotations;
using Transform.Models.Users;
using Transform.Services.Services.OrganizationsService;
using Transform.Common.ImageUpload;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Collections.Generic;
using Transform.Common.Constants;
using Transform.Models.Authentication;
using Microsoft.Extensions.Options;
using Transform.Models.ContextModel;
using Transform.Models.Menu;
using Transform.Services.Services.MenuService;
using Transform.Services.Services.RolesService;
using Transform.Notifications.Services;
using Transform.Notifications.Models;

namespace Transform.Services.Services.UsersService
{
    public class UsersService : IUsersService
    {
        private readonly TransformContext _transformContext;
        private readonly IOrganizationService _organizationService;
        private readonly IRolesService _rolesService;
        private readonly TokenManagement _tokenManagement;
        private readonly DataContext _dataContext;
        private readonly IMenuService _menuService;
        private readonly IRazorTemplateService _razorTemplateService;
        private readonly INotificationService _notificationService;

        public UsersService(TransformContext transformContext,
                            IOrganizationService organizationService,
                            IOptions<TokenManagement> tokenManagement,
                            DataContext dataContext,
                            IMenuService menuService,
                            IRolesService rolesService,
                            IRazorTemplateService razorTemplateService,
                            INotificationService notificationService)
        {
            _transformContext = transformContext;
            _organizationService = organizationService;
            _tokenManagement = tokenManagement.Value;
            _dataContext = dataContext;
            _menuService = menuService;
            _rolesService = rolesService;
            _razorTemplateService = razorTemplateService;
            _notificationService = notificationService;
        }

        public UserResponseModel Login(AuthenticateModel authenticateModel)
        {
            var user = this._transformContext.Users.SingleOrDefault(x => (x.Email == authenticateModel.UserName || x.Mobile == authenticateModel.UserName) && x.Password == authenticateModel.Password);

            if (user == null)
                return null;

            var JWToken = new JwtSecurityToken(
            issuer: _tokenManagement.Issuer,
            audience: _tokenManagement.Audience,
            claims: new List<Claim> { new Claim(ClaimTypes.Name, user.UserId.ToString()) },
            notBefore: new DateTimeOffset(DateTime.UtcNow).DateTime,
            expires: new DateTimeOffset(DateTime.UtcNow.AddDays(_tokenManagement.AccessExpiration)).DateTime,
            signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_tokenManagement.Key)),
                                SecurityAlgorithms.HmacSha256Signature)
        );

            return new UserResponseModel
            {
                UserId = user.UserId,
                UserName = $"{user.FirstName} { user.LastName}",
                AccessToken = new JwtSecurityTokenHandler().WriteToken(JWToken),
                RefreshToken = ""
            };
        }

        public async Task Save(UserSetupModel userSetupModel)
        {
            var usersModel = userSetupModel.UsersModel;
            if (!_transformContext.Users.Any(x => (x.Mobile == usersModel.Mobile || x.Email == usersModel.Email) && x.UserId != usersModel.UserId))
            {
                userSetupModel.OrganizationModel.IsMaster = true;
                int organizationId = await _organizationService.Save(userSetupModel.OrganizationModel);
                var userEntity = new Users
                {
                    UserId = usersModel.UserId,
                    FirstName = usersModel.FirstName,
                    LastName = usersModel.LastName,
                    Mobile = usersModel.Mobile,
                    Email = usersModel.Email,
                    Password = usersModel.Password,
                    IsMaster = true,
                    OrganizationId = organizationId,
                    Gender = usersModel.Gender,
                    Address = usersModel.Address,
                    IsActive = usersModel.IsActive,
                    CreatedBy = _dataContext.userContext.UserId,
                    CreatedOn = DateTime.UtcNow
                };

                if (userEntity.UserId == 0)
                {
                    _transformContext.Users.Add(userEntity);
                }
                else
                {
                    _transformContext.Entry(userEntity).State = EntityState.Modified;
                    if (String.IsNullOrEmpty(usersModel.ProfilePic))
                    {
                        _transformContext.Entry(userEntity).Property(x => x.ProfilePic).IsModified = false;
                    }
                }

                if (!String.IsNullOrEmpty(usersModel.ProfilePic))
                {
                    userEntity.ProfilePic = usersModel.ProfilePic.Upload(ApplicationConstants.Users);
                }

                await _transformContext.SaveChangesAsync();
            }
            else
            {
                throw new ValidationException("Email or Mobile already Exists");
            }
        }

        public IQueryable<MenuLevel> GetUserMenu()
        {
            if (_dataContext.userContext.IsMaster)
            {
                return _menuService.GetMenu();
            }
            IQueryable<Menus> menus = GetUserMenuList();

            return _menuService.GetSubMenus(menus, null);
        }

        private IQueryable<Menus> GetUserMenuList()
        {
            List<UserGroupMenu> groupMenus = _transformContext.UserGroupMenu.Where(group => group.GroupId == _dataContext.userContext.GroupId
                                    && group.IsActive).Select(data => data).ToList();

            return _transformContext.Menus.Where(x => groupMenus.Select(data => data.MenuId).Contains(x.MenuId)).Select(data => data);
        }

        public UsersModel GetUserDetails(int userId)
        {
            return _transformContext.Users.Where(x => x.IsMaster == false && x.UserId == userId && x.OrganizationId == _dataContext.organizationContext.OrganizationId).Select((user) => new UsersModel
            {
                UserId = user.UserId,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Mobile = user.Mobile,
                Email = user.Email,
                Password = user.Password,
                ProfilePic = user.ProfilePic,
                IsMaster = false,
                OrganizationId = user.OrganizationId,
                Gender = user.Gender,
                Address = user.Address,
                IsActive = user.IsActive
            }).FirstOrDefault();
        }

        public UserResponseModel AdminLogin(AuthenticateModel authenticateModel)
        {
            var user = this._transformContext.Users.SingleOrDefault(x => (x.Email == authenticateModel.UserName ||
                              x.Mobile == authenticateModel.UserName) && x.Password == authenticateModel.Password &&
                              x.IsSysAdmin == true);

            if (user == null)
                return null;

            var JWToken = new JwtSecurityToken(
            issuer: _tokenManagement.Issuer,
            audience: _tokenManagement.Audience,
            claims: new List<Claim> { new Claim(ClaimTypes.Name, user.UserId.ToString()) },
            notBefore: new DateTimeOffset(DateTime.UtcNow).DateTime,
            expires: new DateTimeOffset(DateTime.UtcNow.AddDays(_tokenManagement.AccessExpiration)).DateTime,
            signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_tokenManagement.Key)),
                                SecurityAlgorithms.HmacSha256Signature)
        );

            return new UserResponseModel
            {
                UserId = user.UserId,
                UserName = $"{user.FirstName} { user.LastName}",
                AccessToken = new JwtSecurityTokenHandler().WriteToken(JWToken),
                RefreshToken = ""
            };
        }

        public async Task<UsersModel> SaveUserDetails(UsersModel usersModel, bool isEmployee)
        {
            if (!_transformContext.Users.Any(x => (x.Mobile == usersModel.Mobile || !string.IsNullOrEmpty(usersModel.Email) ? x.Email == usersModel.Email : false) && x.UserId != usersModel.UserId))
            {
                var userEntity = new Users
                {
                    UserId = usersModel.UserId,
                    FirstName = usersModel.FirstName,
                    LastName = usersModel.LastName,
                    Mobile = usersModel.Mobile,
                    Email = usersModel.Email,
                    Password = usersModel.Password,
                    IsMaster = false,
                    IsEmployee = isEmployee,
                    Gender = usersModel.Gender,
                    Address = usersModel.Address,
                    OrganizationId = _dataContext.organizationContext.OrganizationId,
                    IsActive = isEmployee ? usersModel.IsActive : false,
                    CreatedBy = _dataContext.userContext.UserId,
                    CreatedOn = DateTime.UtcNow
                };

                if (userEntity.UserId == 0)
                {
                    _transformContext.Users.Add(userEntity);
                }
                else
                {
                    _transformContext.Entry(userEntity).State = EntityState.Modified;
                    if (String.IsNullOrEmpty(usersModel.ProfilePic))
                    {
                        _transformContext.Entry(userEntity).Property(x => x.ProfilePic).IsModified = false;
                    }
                }

                if (!String.IsNullOrEmpty(usersModel.ProfilePic))
                {
                    userEntity.ProfilePic = usersModel.ProfilePic.Upload(ApplicationConstants.Users);
                }

                await _transformContext.SaveChangesAsync();
                return this.GetUserDetails(userEntity.UserId);

            }
            else
            {
                throw new ValidationException("Email or Mobile already Exists");
            }

        }

        public async Task<UserInfoModel> UpdateProfileInfo(UsersModel usersModel)
        {
            if (!_transformContext.Users.Any(x => x.UserId != this._dataContext.userContext.UserId && (x.Mobile == usersModel.Mobile || (usersModel.Email != null && x.Email == usersModel.Email))))
            {
                var user = await this._transformContext.Users.Where(x => x.UserId == this._dataContext.userContext.UserId).FirstOrDefaultAsync();
                user.FirstName = usersModel.FirstName;
                user.LastName = usersModel.LastName;
                user.Mobile = usersModel.Mobile;
                user.Email = usersModel.Email;
                user.Gender = usersModel.Gender;
                user.Address = usersModel.Address;
                user.ModifiedBy = _dataContext.userContext.UserId;
                user.ModifiedOn = DateTime.UtcNow;

                if (!String.IsNullOrEmpty(usersModel.ProfilePic))
                {
                    user.ProfilePic = usersModel.ProfilePic.Upload(ApplicationConstants.Users);
                }
                //var template = await _razorTemplateService.GetHtmlAsync("EmailTemplates/ChangePassword", "Test");
                //await this._notificationService.SendEmailAsync(new EmailModel()
                //{
                //    ToMailIds = new string[] { "harishkdm777@gmail.com" },
                //    EmailTemplate = template,
                //    Subject = "Registration"
                //});
                await _transformContext.SaveChangesAsync();
                return await this.GetLoggedUserInfo();

            }
            else
            {
                throw new ValidationException("Email or Mobile already Exists");
            }

        }

        public async Task<UserInfoModel> GetLoggedUserInfo()
        {
            var userInfo = await _transformContext.Users.Where(x => x.UserId == this._dataContext.userContext.UserId).Select((user) => new UserInfoModel
            {
                UserId = user.UserId,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Mobile = user.Mobile,
                Email = user.Email,
                ProfilePic = user.ProfilePic != null ? user.ProfilePic.ImageToBase64("Users") : null,
                Gender = user.Gender,
                OrganizationId = user.OrganizationId,
                Address = user.Address
            }).FirstOrDefaultAsync();

            if (this._dataContext.userContext.IsMaster || this._dataContext.userContext.IsSysAdmin)
            {
                userInfo.RoleName = "System Admin";
            }
            else if (userInfo.OrganizationId != null && userInfo?.OrganizationId == this._dataContext.organizationContext.OrganizationId)
            {
                var role = _rolesService.GetUsersPrimaryOrganizationRole(this._dataContext.userContext.UserId);
                userInfo.RoleName = role?.RoleName;
            }
            else
            {
                var role = _rolesService.GetUsersOrganizationRole(this._dataContext.userContext.UserId, this._dataContext.organizationContext.OrganizationId);
                userInfo.RoleName = role?.RoleName;
            }
            return userInfo;
        }

        public async Task ChangePassword(UserPasswordModel userPasswordModel)
        {
            var user = await this._transformContext.Users.Where(x => x.UserId == this._dataContext.userContext.UserId && x.Password == userPasswordModel.CurrentPassword).FirstOrDefaultAsync();

            if (user != null)
            {
                user.Password = userPasswordModel.NewPassword;
                user.ModifiedBy = _dataContext.userContext.UserId;
                user.ModifiedOn = DateTime.UtcNow;

                await _transformContext.SaveChangesAsync();
                var template = await _razorTemplateService.GetHtmlAsync("EmailTemplates/ChangePassword", "Test");
                await _notificationService.SendEmailAsync(
                    new EmailModel
                    {
                        EmailTemplate = template,
                        Subject = "PasswordChanged",
                        ToMailIds = new string[] { "harishkdm2819@gmail.com" },
                        ImageAttachment = new string[] { user.ProfilePic?.ImageToBase64("Users") }
                    });
            }
            else
            {
                throw new ValidationException("Current Password is Invalid");
            }
        }
    }
}
