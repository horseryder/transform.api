﻿using System.Linq;
using System.Threading.Tasks;
using Transform.Models.Menu;
using Transform.Models.Users;

namespace Transform.Services.Services.UsersService
{
    public interface IUsersService
    {

        UserResponseModel Login(AuthenticateModel authenticateModel);

        UserResponseModel AdminLogin(AuthenticateModel authenticateModel);

        Task Save(UserSetupModel userSetupModel);

        IQueryable<MenuLevel> GetUserMenu();

        UsersModel GetUserDetails(int userId);

        Task<UsersModel> SaveUserDetails(UsersModel usersModel, bool isEmployee);

        Task<UserInfoModel> UpdateProfileInfo(UsersModel usersModel);

        Task<UserInfoModel> GetLoggedUserInfo();

        Task ChangePassword(UserPasswordModel userPasswordModel);
    }
}
