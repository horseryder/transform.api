﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Transform.Entities;
using Transform.Entities.Entities;
using Transform.Models.ContextModel;
using Transform.Models.EnquiryForm;

namespace Transform.Services.Services.EnquiryFormService
{
    public class EnquiryFormService : IEnquiryFormService
    {
        private readonly TransformContext _transformContext;
        private readonly DataContext _dataContext;

        public EnquiryFormService(TransformContext transformContext, DataContext dataContext)
        {
            _transformContext = transformContext;
            _dataContext = dataContext;
        }

        public IQueryable<EnquiryFormListModel> Get()
        {
            return from form in _transformContext.EnquiryForm
                   join response in _transformContext.ClientResponse
                        on form.ResponseId equals response.ResponseId into responses
                   from clientResponse in responses.DefaultIfEmpty()
                   where form.OrganizationId == _dataContext.organizationContext.OrganizationId
                   select new EnquiryFormListModel
                   {
                       EnquiryFormId = form.EnquiryFormId,
                       FirstName = form.FirstName,
                       LastName = form.LastName,
                       FullName = $"{form.FirstName} {form.LastName}",
                       Mobile = form.Mobile,
                       Email = form.Email,
                       TrailDate = form.TrailDate.HasValue ? form.TrailDate.Value.ToString("MM/dd/yyyy") : null,
                       FollowUpDate = form.FollowUpDate.HasValue ? form.FollowUpDate.Value.ToString("MM/dd/yyyy") : null,
                       ReferBy = form.ReferBy,
                       PackageId = form.PackageId,
                       ResponseId = form.ResponseId,
                       ResponseName = clientResponse.ResponseName,
                       OrganizationId = form.OrganizationId,
                       Gender = form.Gender,
                       Address = form.Address,
                       IsActive = form.IsActive
                   };
        }
        public async Task<EnquiryFormModel> Get(int enquiryFormId)
        {
            var query = from form in _transformContext.EnquiryForm
                        where form.OrganizationId == _dataContext.organizationContext.OrganizationId
                        && form.EnquiryFormId == enquiryFormId
                        select new EnquiryFormModel
                        {
                            EnquiryFormId = form.EnquiryFormId,
                            FirstName = form.FirstName,
                            LastName = form.LastName,
                            Mobile = form.Mobile,
                            Email = form.Email,
                            TrailDate = form.TrailDate.HasValue ? form.TrailDate.Value.ToString("MM/dd/yyyy") : null,
                            FollowUpDate = form.FollowUpDate.HasValue ? form.FollowUpDate.Value.ToString("MM/dd/yyyy") : null,
                            ReferBy = form.ReferBy,
                            PackageId = form.PackageId,
                            ResponseId = form.ResponseId,
                            OrganizationId = form.OrganizationId,
                            Gender = form.Gender,
                            Address = form.Address,
                            IsActive = form.IsActive
                        };
            return await query.FirstOrDefaultAsync();
        }
        public async Task Save(EnquiryFormModel enquiryFormModel)
        {
            var enquiryFormEntity = new EnquiryForm
            {
                EnquiryFormId = enquiryFormModel.EnquiryFormId,
                FirstName = enquiryFormModel.FirstName,
                LastName = enquiryFormModel.LastName,
                Mobile = enquiryFormModel.Mobile,
                OrganizationId = Convert.ToInt32(_dataContext.organizationContext.OrganizationId),
                Email = enquiryFormModel.Email,
                Gender = enquiryFormModel.Gender,
                Address = enquiryFormModel.Address,
                TrailDate = string.IsNullOrEmpty(enquiryFormModel.TrailDate) ? (DateTime?)null : DateTime.Parse(enquiryFormModel.TrailDate),
                FollowUpDate = string.IsNullOrEmpty(enquiryFormModel.FollowUpDate) ? (DateTime?)null : DateTime.Parse(enquiryFormModel.FollowUpDate),
                ReferBy = enquiryFormModel.ReferBy,
                PackageId = enquiryFormModel.PackageId,
                ResponseId = enquiryFormModel.ResponseId,
                IsActive = enquiryFormModel.IsActive,
                CreatedBy = _dataContext.userContext.UserId,
                CreatedOn = DateTime.UtcNow
            };

            if (enquiryFormEntity.EnquiryFormId == 0)
            {
                _transformContext.EnquiryForm.Add(enquiryFormEntity);
            }
            else
            {
                _transformContext.Entry(enquiryFormEntity).State = EntityState.Modified;
            }
            await _transformContext.SaveChangesAsync();
        }
    }
}
