﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transform.Models.EnquiryForm;

namespace Transform.Services.Services.EnquiryFormService
{
    public interface IEnquiryFormService
    {
        IQueryable<EnquiryFormListModel> Get();
        Task<EnquiryFormModel> Get(int enquiryFormId);
        Task Save(EnquiryFormModel enquiryFormModel);
    }
}
