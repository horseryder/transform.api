﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transform.Entities;
using Transform.Entities.Entities;
using Transform.Models.ContextModel;

namespace Transform.Services.Services.UsersAttendanceService
{
    public class UsersAttendanceService : IUsersAttendanceService
    {
        private readonly TransformContext transformContext;
        private readonly DataContext dataContext;


        public UsersAttendanceService(TransformContext transformContext,
            DataContext dataContext)
        {
            this.transformContext = transformContext;
            this.dataContext = dataContext;
        }


        public async Task UserAttendance(int userId)
        {
            var latestClockTime = await this.transformContext.UsersAttendance
                .Where(x => x.ClockDateTime.ToString("MM/dd/yyyy") == DateTime.UtcNow.ToString("MM/dd/yyyy") && x.UserId == userId && x.OrganizationId == this.dataContext.organizationContext.OrganizationId)
                .OrderByDescending(x => x.ClockDateTime)
                .FirstOrDefaultAsync();

            var userAttendanceEntity = new UsersAttendance
            {
                UserId = userId,
                OrganizationId = this.dataContext.organizationContext.OrganizationId,
                ClockDateTime = DateTime.UtcNow,
                IsClockIn = !latestClockTime?.IsClockIn ?? true
            };
            this.transformContext.UsersAttendance.Add(userAttendanceEntity);
            await this.transformContext.SaveChangesAsync();
        }
    }
}
