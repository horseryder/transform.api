﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Transform.Services.Services.UsersAttendanceService
{
    public interface IUsersAttendanceService
    {
        Task UserAttendance(int userId);
    }
}
