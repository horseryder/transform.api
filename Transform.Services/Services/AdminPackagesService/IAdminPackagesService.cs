﻿using System.Linq;
using System.Threading.Tasks;
using Transform.Models.AdminPackages;

namespace Transform.Services.Services.AdminPackagesService
{
    public interface IAdminPackagesService
    {
        IQueryable<AdminPackagesModel> Get();

        Task Save(AdminPackagesModel adminPackagesModel);
    }
}
