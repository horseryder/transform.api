﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Transform.Entities;
using Transform.Entities.Entities;
using Transform.Models.AdminPackages;
using Transform.Models.ContextModel;

namespace Transform.Services.Services.AdminPackagesService
{
    public class AdminPackagesService : IAdminPackagesService
    {
        private readonly TransformContext _transformContext;
        private readonly DataContext _dataContext;

        /// <summary>
        /// Intiliaze
        /// </summary>
        /// <param name="transformContext"></param>
        public AdminPackagesService(TransformContext transformContext, DataContext dataContext)
        {
            _transformContext = transformContext;
            _dataContext = dataContext;
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <returns></returns>
        public IQueryable<AdminPackagesModel> Get()
        {
            return _transformContext.AdminPackages.Select(packages => new AdminPackagesModel
            {
                PackageId = packages.PackageId,
                PackageName = packages.PackageName,
                GroupId = packages.GroupId,
                PackagePrice = packages.PackagePrice,
                Validity = packages.Validity,
                Description = packages.Description,
                SubBranches = packages.SubBranches,
                SubBranchCount = packages.SubBranchCount,
                IsActive = packages.IsActive
            });
        }

        public async Task Save(AdminPackagesModel adminPackagesModel)
        {
            var packageEntity = new AdminPackages
            {
                PackageId = adminPackagesModel.PackageId,
                PackageName = adminPackagesModel.PackageName,
                GroupId = adminPackagesModel.GroupId,
                PackagePrice = adminPackagesModel.PackagePrice,
                Validity = adminPackagesModel.Validity,
                Description = adminPackagesModel.Description,
                SubBranches = adminPackagesModel.SubBranches,
                SubBranchCount = adminPackagesModel.SubBranchCount,
                IsActive = adminPackagesModel.IsActive,
                CreatedBy = _dataContext.userContext.UserId,
                CreatedOn = DateTime.UtcNow
            };

            if (packageEntity.PackageId == 0)
            {
                _transformContext.AdminPackages.Add(packageEntity);
            }
            else
            {
                _transformContext.Entry(packageEntity).State = EntityState.Modified;
            }
            await _transformContext.SaveChangesAsync();
        }
    }
}
