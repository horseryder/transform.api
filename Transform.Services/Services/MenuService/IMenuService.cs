﻿using System.Linq;
using System.Threading.Tasks;
using Transform.Entities.Entities;
using Transform.Models.Menu;

namespace Transform.Services.Services.MenuService
{
    public interface IMenuService
    {
        Task<MenusModel> SaveMenu(MenusModel menusModel);
        IQueryable<MenuLevel> GetMenu();
        IQueryable<Menus> GetOrganizationMenu();
        IQueryable<MenuLevel> GetSubMenus(IQueryable<Menus> menus, int? menuId);
     }
}
