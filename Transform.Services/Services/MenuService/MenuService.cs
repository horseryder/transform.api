﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transform.Entities;
using Transform.Entities.Entities;
using Transform.Models.ContextModel;
using Transform.Models.Menu;

namespace Transform.Services.Services.MenuService
{
    public class MenuService : IMenuService
    {
        private readonly TransformContext _transformContext;
        private readonly DataContext _dataContext;

        public MenuService(TransformContext transformContext, DataContext dataContext)
        {
            _dataContext = dataContext;
            _transformContext = transformContext;
        }

        public async Task<MenusModel> SaveMenu(MenusModel menusModel)
        {
            var MenuEntity = new Menus
            {
                MenuId = menusModel.MenuId,
                MenuName = menusModel.MenuName,
                MenuSequence = menusModel.MenuSequence,
                RoutePath = menusModel.RoutePath,
                Icon = menusModel.Icon,
                ParentMenuId = menusModel.ParentMenuId,
                IsActive = menusModel.IsActive,
                CreatedBy = _dataContext.userContext.UserId,
                CreatedOn = DateTime.UtcNow
            };

            if (MenuEntity.MenuId == 0)
            {
                _transformContext.Menus.Add(MenuEntity);
            }
            else
            {
                _transformContext.Entry(MenuEntity).State = EntityState.Modified;
            }

            await _transformContext.SaveChangesAsync();
            return await this.GetMenubyId(MenuEntity.MenuId);
        }

        private async Task<MenusModel> GetMenubyId(int menuId)
        {
            var response = from menu in _transformContext.Menus
                           where menu.MenuId == menuId
                           select new MenusModel
                           {
                               MenuId = menu.MenuId,
                               MenuName = menu.MenuName,
                               MenuSequence = menu.MenuSequence,
                               Icon = menu.Icon,
                               IsActive = menu.IsActive,
                               ParentMenuId = menu.ParentMenuId,
                               RoutePath = menu.RoutePath
                           };
            return await response.FirstOrDefaultAsync();
        }

        public IQueryable<MenuLevel> GetMenu()
        {
            IQueryable<Menus> menus = _transformContext.Menus.Select(data => data);

            return GetSubMenus(menus, null);
        }

        public IQueryable<MenuLevel> GetSubMenus(IQueryable<Menus> menus, int? menuId)
        {
            var response = menus.Where(id => id.ParentMenuId == menuId).Select(data => new MenuLevel
            {
                MenuId = data.MenuId,
                MenuName = data.MenuName,
                RoutePath = data.RoutePath,
                Icon = data.Icon,
                MenuSequence = data.MenuSequence,
                ParentMenuId = data.ParentMenuId,
                IsActive = data.IsActive,
                SubMenus = GetSubMenus(menus, data.MenuId)
            });
            return response;
        }

        public IQueryable<Menus> GetOrganizationMenu()
        {
            if (_dataContext.organizationContext.IsMaster)
            {
                int groupId = _transformContext.AdminPackages.FirstOrDefault(x => x.PackageId == _dataContext.organizationContext.PackageId).GroupId;
                var groupMenus = _transformContext.AdminGroupMenu.Where(group => group.GroupId == groupId
                                    && group.IsActive).Select(data => data).ToList();

                return _transformContext.Menus.Where(x => groupMenus.Select(data => data.MenuId).Contains(x.MenuId)).Select(data => data);
            }
            else
            {
                var groupMenus = _transformContext.UserGroupMenu.Where(group => group.GroupId == _dataContext.userContext.GroupId
                                    && group.IsActive).Select(data => data).ToList();

                return _transformContext.Menus.Where(x => groupMenus.Select(data => data.MenuId).Contains(x.MenuId)).Select(data => data);

            }
        }

    }
}
