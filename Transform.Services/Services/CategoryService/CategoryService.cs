﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transform.Common.Constants;
using Transform.Entities;
using Transform.Entities.Entities;
using Transform.Models.Category;
using Transform.Models.ContextModel;
using Transform.Models.DropDown;

namespace Transform.Services.Services.CategoryService
{
    public class CategoryService : ICategoryService
    {
        private readonly TransformContext _transformContext;
        private readonly DataContext _dataContext;

        public CategoryService(TransformContext transformContext,
                            DataContext dataContext)
        {
            _transformContext = transformContext;
            _dataContext = dataContext;
        }

        public IQueryable<CategoryModel> GetCategory()
        {
            var category = from cat in _transformContext.Category
                           where cat.OrganizationId == _dataContext.organizationContext.ParentOrganizationId
                           orderby cat.OrderNumber ascending
                           select new CategoryModel
                           {
                               CategoryId = cat.CategoryId,
                               Name = cat.Name,
                               CategoryType = cat.CategoryType,
                               CategoryTypeName = cat.CategoryType != null ? Enum.GetName(typeof(CategoryTypes), cat.CategoryType) : null,
                               OrderNumber = cat.OrderNumber,
                               Description = cat.Description,
                               IsActive = cat.IsActive
                           };
            return category;
        }

        public async Task<CategoryModel> GetCategoryDeatils(int CategoryId)
        {
            return await (from cat in this.GetCategory()
                          where cat.CategoryId == CategoryId
                          select cat).FirstOrDefaultAsync();
        }

        public IQueryable<DropDownModel> GetCategoryList(int categoryType)
        {
            return _transformContext.Category.Where(x => x.CategoryType == categoryType && x.OrganizationId == _dataContext.organizationContext.ParentOrganizationId && x.IsActive)
                .OrderBy(x => x.OrderNumber).Select((cat) => new DropDownModel
                {
                    Id = cat.CategoryId,
                    Value = cat.Name
                });
        }

        public List<DropDownModel> GetCategoryTypes()
        {
            List<DropDownModel> categoryTypes = new List<DropDownModel>();
            foreach (var types in Enum.GetValues(typeof(CategoryTypes)))
            {
                DropDownModel val = new DropDownModel
                {
                    Id = (int)types,
                    Value = Enum.GetName(typeof(CategoryTypes), types)
                };

                categoryTypes.Add(val);
            }
            return categoryTypes;
        }

        public async Task Save(CategoryModel categoryModel)
        {
            var category = new Category
            {
                CategoryId = categoryModel.CategoryId,
                Name = categoryModel.Name,
                Description = categoryModel.Description,
                CategoryType = categoryModel.CategoryType,
                OrderNumber = categoryModel.OrderNumber,
                IsActive = categoryModel.IsActive,
                OrganizationId = _dataContext.organizationContext.ParentOrganizationId,
                CreatedBy = _dataContext.userContext.UserId,
                CreatedOn = DateTime.UtcNow
            };

            if (category.CategoryId == 0)
            {
                _transformContext.Category.Add(category);
            }
            else
            {
                _transformContext.Entry(category).State = EntityState.Modified;
            }

            await _transformContext.SaveChangesAsync();
        }

    }
}
