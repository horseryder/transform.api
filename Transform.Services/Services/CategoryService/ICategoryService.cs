﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transform.Models.Category;
using Transform.Models.DropDown;

namespace Transform.Services.Services.CategoryService
{
    public interface ICategoryService
    {
        IQueryable<CategoryModel> GetCategory();
        Task<CategoryModel> GetCategoryDeatils(int CategoryId);
        IQueryable<DropDownModel> GetCategoryList(int categoryType);
        Task Save(CategoryModel categoryModel);
        List<DropDownModel> GetCategoryTypes();
    }
}
