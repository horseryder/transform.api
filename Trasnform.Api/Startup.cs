﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Transform.Entities;
using Transform.Models.Authentication;
using Transform.Models.ContextModel;
using Transform.Notifications.Configurations;
using Transform.Notifications.Services;
using Transform.Services.Services.AdminPackagesService;
using Transform.Services.Services.BatchService;
using Transform.Services.Services.CategoryService;
using Transform.Services.Services.ClientService;
using Transform.Services.Services.ContextService;
using Transform.Services.Services.DropDownService;
using Transform.Services.Services.EmployeeService;
using Transform.Services.Services.EnquiryFormService;
using Transform.Services.Services.EquipmentService;
using Transform.Services.Services.GroupMenuService;
using Transform.Services.Services.GroupService;
using Transform.Services.Services.MenuService;
using Transform.Services.Services.OrganizationsService;
using Transform.Services.Services.Program;
using Transform.Services.Services.RolesService;
using Transform.Services.Services.UserPackagesService;
using Transform.Services.Services.UserRolesService;
using Transform.Services.Services.UsersAttendanceService;
using Transform.Services.Services.UsersService;
using Trasnform.Api.Infrastructure;

namespace Trasnform.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.Configure<TokenManagement>(Configuration.GetSection("TokenManagement"));
            var token = Configuration.GetSection("TokenManagement").Get<TokenManagement>();

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    ValidateLifetime = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(token.Key)),
                    ValidIssuer = token.Issuer,
                    ValidAudience = token.Audience,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ClockSkew = TimeSpan.Zero
                };
            });

            services.AddScoped((provider) =>
            {
                var context = provider.GetRequiredService<IHttpContextAccessor>().HttpContext;

                if (context == null || !context.User.Claims.Any())
                {
                    return new DataContext();
                }

                int userId = Convert.ToInt32(context.User.FindFirst(ClaimTypes.Name).Value);
                var userOrg = provider.GetRequiredService<IContextService>();
                return userOrg.GetUserOrg(userId);
            });

            services.AddMvc(config =>
            {
                var policy = new AuthorizationPolicyBuilder()
                         .RequireAuthenticatedUser()
                         .Build();
                config.Filters.Add(new AuthorizeFilter(policy));

                config.Filters.Add(typeof(ModelStateValidation));
                config.Filters.Add(typeof(ExceptionHandler));
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
            .AddJsonOptions(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            });

            services.AddDbContext<TransformContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("MyConnection"));
            }, ServiceLifetime.Transient
            );

            services.Configure<EmailConfiguration>(Configuration.GetSection(nameof(EmailConfiguration)));
            services.AddSingleton(typeof(EmailConfiguration), provider => provider.GetRequiredService<IOptions<EmailConfiguration>>().Value);
            services.AddScoped<EmailService>();

            services.AddTransient<IOrganizationService, OrganizationService>()
                    .AddTransient<IUsersService, UsersService>()
                    .AddTransient<IRolesService, RolesService>()
                    .AddTransient<IAdminPackagesService, AdminPackagesService>()
                    .AddTransient<IUserPackagesService, UserPackagesService>()
                    .AddTransient<IOrganizationService, OrganizationService>()
                    .AddTransient<IGroupService, GroupService>()
                    .AddTransient<IUserRolesService, UserRolesService>()
                    .AddTransient<IMenuService, MenuService>()
                    .AddTransient<IGroupMenuService, GroupMenuService>()
                    .AddTransient<IDropDownService, DropDownService>()
                    .AddTransient<IEmployeeService, EmployeeService>()
                    .AddTransient<IEnquiryFormService, EnquiryFormService>()
                    .AddTransient<IBatchService, BatchService>()
                    .AddTransient<IProgramService, ProgramService>()
                    .AddTransient<IContextService, ContextService>()
                    .AddTransient<IUsersAttendanceService, UsersAttendanceService>()
                    .AddTransient<IClientService, ClientService>()
                    .AddTransient<INotificationService, NotificationService>()
                    .AddTransient<ICategoryService, CategoryService>()
                    .AddTransient<IEquipmentService, EquipmentService>()
                    .AddTransient<IRazorTemplateService, RazorTemplateService>();


            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Transform API", Version = "v1" });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "Please enter into field the word 'Bearer' following by space and JWT",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                  {
                     {
                        new OpenApiSecurityScheme
                          {
                       Reference = new OpenApiReference
                        {
                             Type = ReferenceType.SecurityScheme,
                              Id = "Bearer"
                               },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header,

                          },
                              new List<string>()
                            }
                        });
            });


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseCors(options => options.AllowAnyOrigin()
                                           .AllowAnyHeader()
                                           .AllowAnyMethod()
                                           .AllowCredentials());

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Transform API V1");
                c.RoutePrefix = string.Empty;
            });

            app.UseAuthentication();

            app.UseMvc();
        }
    }
}
