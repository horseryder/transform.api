﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Data.SqlClient;

namespace Trasnform.Api.Infrastructure
{
    public class ExceptionHandler : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {

            HttpResponse response = context.HttpContext.Response;

            var exception = context.Exception;
            var errorMessage = "";
            if (exception is DbUpdateException updateEx)
            {
                var sqlEx = updateEx.InnerException as SqlException;
                if (sqlEx.Number == 2601 ||  sqlEx.Number == 2627)
                {
                    response.StatusCode = 500;
                    errorMessage = GetErrorMessage(exception.Message, 500);
                }
            }
            else if (exception is ArgumentNullException)
            {

            }
            else
            {
                response.StatusCode = 500;
                errorMessage = GetErrorMessage(exception.Message, 500);
            }

            response.ContentType = "application/json";
            response.ContentLength = errorMessage.Length;
            response.WriteAsync(errorMessage);
        }

        private string GetErrorMessage(string message, int code)
        {
            return JsonConvert.SerializeObject(new
            {
                errorMessage = message,
                errorCode = code
            });
        }
    }
}
