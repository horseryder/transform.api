﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transform.Models.Equipments;
using Transform.Services.Services.EquipmentService;

namespace Trasnform.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EquipmentController : BaseController
    {
        private readonly IEquipmentService _equipmentService;

        public EquipmentController(IEquipmentService equipmentService)
        {
            _equipmentService = equipmentService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_equipmentService.GetEquipemnts());
        }
        [HttpGet("{equipemntId}")]
        public async Task<IActionResult> GetCategoryDetail(int equipemntId)
        {
            return Ok(await _equipmentService.GetEquipemntsDeatils(equipemntId));
        }

        [HttpPost]
        public async Task<IActionResult> Save([FromBody] EquipmentsModel equipmentsModel)
        {
            await _equipmentService.Save(equipmentsModel);
            return Ok();
        }
    }
}
