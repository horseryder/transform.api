﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Transform.Models.Batch;
using Transform.Services.Services.BatchService;

namespace Trasnform.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BatchController : BaseController
    {
        private readonly IBatchService _batchService;

        public BatchController(IBatchService batchService)
        {
            _batchService = batchService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_batchService.Get());
        }

        [HttpGet("{batchId}")]
        public async Task<IActionResult> GetOrganizationDetail(int batchId)
        {
            return Ok(await _batchService.Get(batchId));
        }

        [HttpPost]
        public async Task<IActionResult> Save([FromBody] BatchModel batchModel)
        {
            await _batchService.Save(batchModel);
            return Ok();
        }
    }
}
