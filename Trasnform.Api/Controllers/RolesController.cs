﻿using Microsoft.AspNetCore.Mvc;
using Transform.Services.Services.RolesService;

namespace Trasnform.Api.Controllers
{
    [Route("api/[controller]")]
    public class RolesController : BaseController
    {
        private readonly IRolesService _rolesService;

        public RolesController(IRolesService rolesService)
        {
            _rolesService = rolesService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_rolesService.Get());         
        }
    }
}