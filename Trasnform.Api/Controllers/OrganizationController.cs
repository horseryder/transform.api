﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Transform.Models.Organizations;
using Transform.Services.Services.OrganizationsService;

namespace Trasnform.Api.Controllers
{
    [Route("api/[controller]")]
    public class OrganizationController : BaseController
    {
        private readonly IOrganizationService _organizationService;

        public OrganizationController(IOrganizationService organizationService)
        {
            _organizationService = organizationService;
        }

        [HttpGet("admin")]
        public IActionResult GetOrganizationsForAdmin()
        {
            return Ok(_organizationService.GetOrganizationsForAdmin());
        }

        [HttpGet("SubOrganization")]
        public IActionResult Get()
        {
            return Ok(_organizationService.GetSubOrganizations());
        }

        [HttpGet("details/{organizationId}")]
        public IActionResult Get(int organizationId)
        {
            return Ok(_organizationService.Get(organizationId));
        }

        [HttpGet("SubOrganization/{organizationId}")]
        public async Task<IActionResult> GetOrganizationDetail(int organizationId)
        {
            return Ok(await _organizationService.GetOrganizationDetails(organizationId));
        }

        [HttpPost("user")]
        public async Task<IActionResult> SaveUserOrganization([FromBody] OrganizationsModel organizationModel)
        {
            organizationModel.IsMaster = false;
            await _organizationService.Save(organizationModel);
            return Ok();
        }
    }
}