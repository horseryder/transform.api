﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Transform.Models.Category;
using Transform.Services.Services.CategoryService;

namespace Trasnform.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : BaseController
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_categoryService.GetCategory());
        }

        [HttpGet("categoryType")]
        public IActionResult GetCategoryTypes()
        {
            return Ok(_categoryService.GetCategoryTypes());
        }

        [HttpGet("{categoryId}")]
        public async Task<IActionResult> GetCategoryDetail(int categoryId)
        {
            return Ok(await _categoryService.GetCategoryDeatils(categoryId));
        }

        [HttpGet("CategoryByType/{categoryType}")]
        public IActionResult GetCategorybyType(int categoryType)
        {
            return Ok(_categoryService.GetCategoryList(categoryType));
        }

        [HttpPost]
        public async Task<IActionResult> Save([FromBody] CategoryModel categoryModel)
        {
            await _categoryService.Save(categoryModel);
            return Ok();
        }
    }
}
