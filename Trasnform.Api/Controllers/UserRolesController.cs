﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Transform.Models.UserRoles;
using Transform.Services.Services.UserRolesService;

namespace Trasnform.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserRolesController : BaseController
    {
        private readonly IUserRolesService _userRolesService;

        public UserRolesController(IUserRolesService userRolesService)
        {
            _userRolesService = userRolesService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_userRolesService.Get());
        }

        [HttpGet("{roleId}")]
        public async Task<IActionResult> GetOrganizationDetail(int roleId)
        {
            return Ok(await _userRolesService.Get(roleId));
        }

        [HttpPost]
        public async Task<IActionResult> Save([FromBody] UserRolesModel userRolesModel)
        {
            await _userRolesService.Save(userRolesModel);
            return Ok();
        }
    }
}