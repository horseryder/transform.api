﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Transform.Services.Services.DropDownService;

namespace Trasnform.Api.Controllers
{
    [Route("api/[controller]")]
    public class DropDownServiceController : BaseController
    {
        private readonly IDropDownService _dropDownService;

        public DropDownServiceController(IDropDownService dropDownService)
        {
            _dropDownService = dropDownService;
        }

        [HttpPost]
        public IActionResult GetMasterData([FromBody] string[] dropDowns)
        {
            var data = _dropDownService.GetMasteData(dropDowns);
            return this.Ok(data);
        }

    }
}