﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Transform.Models.UserPackages;
using Transform.Services.Services.UserPackagesService;

namespace Trasnform.Api.Controllers
{
    [Route("api/[controller]")]
    public class UserPackagesController : BaseController
    {
        private readonly IUserPackagesService _userPackagesService;
        public UserPackagesController(IUserPackagesService userPackagesService)
        {
            _userPackagesService = userPackagesService;
        }

        // GET: api/UserPackages
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_userPackagesService.Get());
        }

        // GET: api/UserPackages/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await _userPackagesService.Get(id));
        }

        // POST: api/UserPackages
        [HttpPost]
        public async Task<IActionResult> Save([FromBody] UserPackagesModel userPackagesModel)
        {
            await _userPackagesService.Save(userPackagesModel);
            return Ok();
        }
    }
}
