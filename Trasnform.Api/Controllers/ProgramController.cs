﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Transform.Models.Program;
using Transform.Services.Services.Program;

namespace Trasnform.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProgramController : ControllerBase
    {
        private readonly IProgramService _programService;

        public ProgramController(IProgramService programService)
        {
            _programService = programService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_programService.Get());
        }

        [HttpGet("{programId}")]
        public async Task<IActionResult> GetOrganizationDetail(int programId)
        {
            return Ok(await _programService.Get(programId));
        }

        [HttpPost]
        public async Task<IActionResult> Save([FromBody] ProgramModel batchModel)
        {
            await _programService.Save(batchModel);
            return Ok();
        }
    }
}
