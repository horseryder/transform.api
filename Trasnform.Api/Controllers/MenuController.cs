﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Transform.Models.Menu;
using Transform.Services.Services.MenuService;

namespace Trasnform.Api.Controllers
{
    [Route("api/[controller]")]
    public class MenuController : BaseController
    {
        private readonly IMenuService _menuService;

        public MenuController(IMenuService menuService)
        {
            _menuService = menuService;
        }

        [HttpPost("SaveMenu")]
        public async Task<IActionResult> SaveMenu([FromBody] MenusModel menusModel)
        {
            return Ok(await _menuService.SaveMenu(menusModel));
        }

        [HttpGet]
        public IActionResult GetMenu()
        {
            return Ok( _menuService.GetMenu());
        }
    }
}