﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Transform.Models.EmployeesPermission;
using Transform.Models.Users;
using Transform.Services.Services.EmployeeService;

namespace Trasnform.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : BaseController
    {
        private readonly IEmployeeService _employeeService;
        public EmployeeController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        [HttpGet]
        public IActionResult GetEmployeesList()
        {
            return Ok(_employeeService.GetUserEmployeesList());
        }

        [HttpGet("{userId}")]
        public async Task<IActionResult> GetUserInfo(int userId)
        {
            return Ok(await _employeeService.GetUserEmployees(userId));
        }

        [HttpPost("details")]
        public async Task<IActionResult> SaveDetails([FromBody] UsersModel userSetupModel)
        {
            return Ok(await _employeeService.SaveEmployeeDetails(userSetupModel));
        }

        [HttpPost("permissions")]
        public async Task<IActionResult> SavePermissions([FromBody] EmployeesPermissionModel userEmployeesModel)
        {
            return Ok(await _employeeService.SaveEmployeePermissions(userEmployeesModel));
        }

        [HttpGet("clientsbyNames/{searchText}")]
        public IActionResult GetClientsbyNames(string searchText)
        {
            return Ok(_employeeService.GetClientsbyName(searchText));
        }
    }
}