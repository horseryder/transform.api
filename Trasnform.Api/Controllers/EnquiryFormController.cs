﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Transform.Services.Services.EnquiryFormService;
using Transform.Models.EnquiryForm;

namespace Trasnform.Api.Controllers
{
    [Route("api/[controller]")]
    public class EnquiryFormController : Controller
    {
        private readonly IEnquiryFormService _enquiryFormService;
        public EnquiryFormController(IEnquiryFormService enquiryFormService)
        {
            _enquiryFormService = enquiryFormService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_enquiryFormService.Get());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await _enquiryFormService.Get(id));
        }

        [HttpPost]
        public async Task<IActionResult> Save([FromBody]EnquiryFormModel enquiryFormModel)
        {
            await _enquiryFormService.Save(enquiryFormModel);
            return Ok();
        }
    }
}
