﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Transform.Models.Client;
using Transform.Models.Users;
using Transform.Services.Services.ClientService;

namespace Trasnform.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : BaseController
    {
        private readonly IClientService _clientService;
        public ClientController(IClientService clientService)
        {
            _clientService = clientService;
        }

        [HttpGet]
        public IActionResult GetEmployeesList()
        {
            return Ok(_clientService.GetUserClientList());
        }

        [HttpGet("{userId}")]
        public async Task<IActionResult> GetUserInfo(int userId)
        {
            return Ok(await _clientService.GetUserClients(userId));
        }

        [HttpPost("details")]
        public async Task<IActionResult> SaveDetails([FromBody] UsersModel userSetupModel)
        {
            return Ok(await _clientService.SaveClientDetails(userSetupModel));
        }

        [HttpPost("packages")]
        public async Task<IActionResult> SavePermissions([FromBody] ClientsPackageModel clientsPackageModel)
        {
            return Ok(await _clientService.SaveClientsPackage(clientsPackageModel));
        }

        [HttpPost("payment")]
        public async Task<IActionResult> SavePaymentDetails([FromBody] ClientsPackageModel clientsPackageModel)
        {
            await _clientService.SaveClientsPaymentInfo(clientsPackageModel);
            return Ok();
        }
    }
}
