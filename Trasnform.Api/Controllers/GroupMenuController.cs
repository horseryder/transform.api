﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Transform.Models.GroupMenu;
using Transform.Services.Services.GroupMenuService;

namespace Trasnform.Api.Controllers
{
    [Route("api/[controller]")]
    public class GroupMenuController : BaseController
    {
        private readonly IGroupMenuService _groupMenus;

        public GroupMenuController(IGroupMenuService groupMenus)
        {
            _groupMenus = groupMenus;
        }

        [HttpGet("admin/{groupId}")]
        public async Task<IActionResult> GetAdminGroupMenu(int groupId)
        {
            return Ok(await _groupMenus.GetAdminGroupMenu(groupId));
        }

        [HttpGet("user/{groupId}")]
        public async Task<IActionResult> GetUserGroupMenu(int groupId)
        {
            return Ok(await _groupMenus.GetUserGroupMenu(groupId));
        }

        [HttpPost("admin/{groupId}")]
        public async Task<IActionResult> SaveAdminGroupMenus([FromBody] List<GroupMenuEntityModel> groupMenuEntityModel, int groupId)
        {
            await _groupMenus.SaveAdminGroupMenus(groupMenuEntityModel, groupId);
            return Ok();
        }

        [HttpPost("user/{groupId}")]
        public async Task<IActionResult> SaveUserGroupMenus([FromBody] List<GroupMenuEntityModel> groupMenuEntityModel, int groupId)
        {
            await _groupMenus.SaveUserGroupMenus(groupMenuEntityModel, groupId);
            return Ok();
        }

        [HttpGet("assignUserGroup/{groupId}")]
        public async Task<IActionResult> GetUserAssignGroupMenu(int groupId)
        {
            return Ok(await _groupMenus.GetUserGroupMenu(groupId));
        }
    }
}