﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Transform.Models.Users;
using Transform.Services.Services.UsersService;

namespace Trasnform.Api.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : BaseController
    {
        private readonly IUsersService _usersService;
        public UsersController(IUsersService usersService)
        {
            _usersService = usersService;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Login([FromBody] AuthenticateModel authenticateModel)
        {
            return Ok(_usersService.Login(authenticateModel));
        }

        [AllowAnonymous]
        [HttpPost("admin/authenticate")]
        public IActionResult AdminLogin([FromBody] AuthenticateModel authenticateModel)
        {
            return Ok(_usersService.AdminLogin(authenticateModel));
        }

        [HttpPost]
        public async Task<IActionResult> Save([FromBody] UserSetupModel userSetupModel)
        {
            await _usersService.Save(userSetupModel);
            return Ok();
        }

        [HttpGet("menus")]
        public IActionResult GetUserMenus()
        {
            return Ok(_usersService.GetUserMenu());
        }

        [HttpGet("info")]
        public async Task<IActionResult> GetUserLoggedInfo()
        {
            return Ok(await _usersService.GetLoggedUserInfo());
        }

        [HttpPost("profile")]
        public async Task<IActionResult> UpdateProfileInfo([FromBody] UsersModel userSetupModel)
        {
            return Ok(await _usersService.UpdateProfileInfo(userSetupModel));
        }

        [HttpPost("changepassword")]
        public async Task<IActionResult> ChangePassword([FromBody] UserPasswordModel userPasswordModel)
        {
            await _usersService.ChangePassword(userPasswordModel);
            return Ok();
        }
    }
}