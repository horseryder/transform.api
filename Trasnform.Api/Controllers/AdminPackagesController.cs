﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Transform.Models.AdminPackages;
using Transform.Services.Services.AdminPackagesService;

namespace Trasnform.Api.Controllers
{
    [Route("api/[controller]")]
    public class AdminPackagesController : BaseController
    {
        private readonly IAdminPackagesService _adminPackagesService;

        public AdminPackagesController(IAdminPackagesService adminPackagesService)
        {
            _adminPackagesService = adminPackagesService;
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_adminPackagesService.Get());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="adminPackagesModel"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Save(AdminPackagesModel adminPackagesModel)
        {
            await _adminPackagesService.Save(adminPackagesModel);
            return Ok();
        }
    }
}