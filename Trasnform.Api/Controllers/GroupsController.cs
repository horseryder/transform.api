﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Transform.Models.UserGroups;
using Transform.Services.Services.GroupService;

namespace Trasnform.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GroupsController : BaseController
    {
        private readonly IGroupService _groupService;

        public GroupsController(IGroupService groupService)
        {
            _groupService = groupService;
        }

        [HttpGet("admin")]
        public IActionResult GetAdminGroups()
        {
            return Ok(_groupService.GetAdminGroups());
        }

        [HttpGet("user")]
        public IActionResult GetUserGroups()
        {
            return Ok(_groupService.GetUserGroups());
        }

        [HttpPost("admin")]
        public async Task<IActionResult> SaveAdminGroups([FromBody] GroupsModel groupsModel)
        {
            await _groupService.SaveAdminGroups(groupsModel);
            return Ok();
        }

        [HttpPost("users")]
        public async Task<IActionResult> SaveUserGroups([FromBody] GroupsModel groupsModel)
        {
            await _groupService.SaveUserGroups(groupsModel);
            return Ok();
        }
    }
}