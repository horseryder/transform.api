﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Trasnform.Api.Infrastructure;

namespace Trasnform.Api.Controllers
{
    /// <summary>
    /// Base Controller
    /// </summary>
    [ApiController]
    public class BaseController : ControllerBase
    {
    }
}