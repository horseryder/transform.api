﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Transform.Services.Services.UsersAttendanceService;

namespace Trasnform.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersAttendanceController : ControllerBase
    {
        private readonly IUsersAttendanceService usersAttendanceService;

        public UsersAttendanceController(IUsersAttendanceService usersAttendanceService)
        {
            this.usersAttendanceService = usersAttendanceService;
        }

        [HttpPost("{userId}")]
        public async Task<IActionResult> Save(int userId)
        {
            await usersAttendanceService.UserAttendance(userId);
            return Ok();
        }
    }
}
