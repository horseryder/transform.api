﻿using System;
using System.Collections.Generic;
using System.Text;
using Transform.Notifications.Models;

namespace Transform.Notifications.Configurations
{
    public class EmailConfiguration
    {
        public string SmtpServer { get; set; }

        public string SmtpEmail { get; set; }

        public string SmtpPassword { get; set; }

        public int SmtpPort { get; set; }

        public string DisplayName { get; set; }

        public bool DemoMode { get; set; }

        public string[] DemoEmails { get; set; }

        public int AttachmentSizeLimit { get; set; }

        public MailRetry Retry { get; set; }
    }
}
