﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Transform.Notifications.Models;

namespace Transform.Notifications.Services
{
    public interface INotificationService
    {
        Task SendEmailAsync(EmailModel model);
    }
}
