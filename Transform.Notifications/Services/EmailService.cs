﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Transform.Common.Extensions;
using Transform.Models;
using Transform.Notifications.Configurations;
using Transform.Notifications.Constants;
using Transform.Notifications.Models;

namespace Transform.Notifications.Services
{
    public class EmailService
    {
        private readonly EmailConfiguration emailConfiguration;

        public EmailService(EmailConfiguration emailConfiguration)
        {
            this.emailConfiguration = emailConfiguration;
        }

        public async Task SendAsync(EmailModel model)
        {
            var mailMessage = new MailMessage();

            if (!model.ToMailIds.Any())
            {
                throw new ValidationException("Please provide recipients");
            }

            if (this.emailConfiguration.DemoMode)
            {
                model.ToMailIds = this.emailConfiguration.DemoEmails;
            }

            foreach (string toMail in model.ToMailIds)
            {
                if (Regex.IsMatch(toMail.Trim(), EmailConstants.EmailPattern))
                {
                    mailMessage.To.Add(toMail.Trim());
                }
                else
                {
                    throw new ValidationException("RecipientEmailNotValidFormat");
                }
            }

            mailMessage.Subject = model.Subject.IsMissing() ? EmailConstants.DefaultSubject : model.Subject;
            mailMessage.Body = model.EmailTemplate;
            mailMessage.IsBodyHtml = true;

            var attachementsList = new List<string>();

            if (model.AttachmentModel.MandatoryAttachments != null && model.AttachmentModel.MandatoryAttachments.Any())
            {
                attachementsList.AddRange(model.AttachmentModel.MandatoryAttachments);
            }

            if (model.AttachmentModel.IncludeAttachments)
            {
                attachementsList.AddRange(model.AttachmentModel.Attachments);
            }

            if (attachementsList.Any())
            {
                var attachments = new List<Attachment>();

                foreach (var documentPath in attachementsList)
                {
                    try
                    {
                        var attachment = new Attachment(documentPath, MediaTypeNames.Application.Octet);
                        attachments.Add(attachment);
                    }
                    catch
                    {
                        //this.Logger.LogWarning($"Email attachment not found, Path: {documentPath} {Environment.NewLine} Process Details: ProfileId - {model.ProfileId}, FormCode - {model.FormCode}");
                    }
                }

                attachments.ForEach(attachment => mailMessage.Attachments.Add(attachment));
            }

            if (model.PdfAttachment != null && model.PdfAttachment.Length > 0)
            {
                var stream = new MemoryStream();
                stream.Write(model.PdfAttachment, 0, model.PdfAttachment.Length);
                stream.Position = 0;
                mailMessage.Attachments.Add(new Attachment(stream, $"File.pdf", MediaTypeNames.Application.Pdf));
            }

            if (model.ImageAttachment != null && model.ImageAttachment.Length > 0)
            {
                foreach (var image in model.ImageAttachment)
                {
                    if (!string.IsNullOrEmpty(image))
                    {
                        byte[] imageBytes = Convert.FromBase64String(image);
                        MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
                        ms.Write(imageBytes, 0, imageBytes.Length);
                        ms.Position = 0;
                        mailMessage.Attachments.Add(new Attachment(ms, "image.jpg", "image/jpeg"));
                    }
                }
            }

            try
            {
                using (var smtpClient = this.ValidateAndGetSmtpClient(mailMessage, model))
                {
                    if (smtpClient == null || mailMessage.From == null)
                    {
                        throw new ValidationException("Smtp settings are not configured properly");
                    }
                    else
                    {
                        await smtpClient.SendMailAsync(mailMessage);
                        //this.Logger.LogInformation($"Success sending an email to {statusLog.ToEmail}");
                    }
                }
            }
            //catch ()
            //{
            //    this.Logger.LogError($"An exception occured while sending an email to {statusLog.ToEmail} \n {ex.ToJson()}");
            //    throw;
            //}
            finally
            {
                //this.Logger.LogInformation($"Inserting email status in EmailStatusLog table with data: {statusLog.ToJson()} ");
            }
        }

        private SmtpClient ValidateAndGetSmtpClient(MailMessage mailMessage, EmailModel model)
        {
            SmtpClient smtpClient = null;

            void BuildSmtpClient(SmtpSettings smtpSettings)
            {
                if (smtpSettings.SmtpPort.IsPresent())
                {
                    if (smtpSettings.SmtpEmail.IsPresent() && smtpSettings.SmtpPassword.IsPresent())
                    {
                        smtpClient = new SmtpClient(smtpSettings.SmtpServer, Convert.ToInt32(smtpSettings.SmtpPort))
                        {
                            Credentials = new NetworkCredential(smtpSettings.SmtpEmail, smtpSettings.SmtpPassword),
                            EnableSsl = false
                        };
                    }
                    else
                    {
                        smtpClient = new SmtpClient(smtpSettings.SmtpServer, Convert.ToInt32(smtpSettings.SmtpPort));
                    }
                }
                else
                {
                    if (smtpSettings.SmtpEmail.IsPresent() && smtpSettings.SmtpPassword.IsPresent())
                    {
                        smtpClient = new SmtpClient(smtpSettings.SmtpServer)
                        {
                            Credentials = new NetworkCredential(smtpSettings.SmtpEmail, smtpSettings.SmtpPassword),
                            EnableSsl = false,
                        };
                    }
                    else
                    {
                        smtpClient = new SmtpClient(smtpSettings.SmtpServer);
                    }
                }

                // Priority for organization / appsettings and then for user input
                if (smtpSettings.SmtpEmail.IsPresent())
                {
                    mailMessage.From = new MailAddress(smtpSettings.SmtpEmail, smtpSettings.DisplayName);
                }
                else if (model.FromMailId.IsPresent())
                {
                    mailMessage.From = new MailAddress(model.FromMailId, model.From);
                }
            }

            // Priority for organization settings
            if (model.SmtpSettings != null && model.SmtpSettings.SmtpServer.IsPresent())
            {
                //this.Logger.LogInformation("Mail settings are in custom mode, Using Organizational SitePreferences");

                BuildSmtpClient(model.SmtpSettings);
            }
            else if (this.emailConfiguration.SmtpServer.IsPresent())
            {// Fallback to appsettings if organization settings does not exists
                //this.Logger.LogInformation($"Using SMTP server from appsettings: {this.emailConfiguration.SmtpServer}");

                BuildSmtpClient(new SmtpSettings
                {
                    SmtpServer = this.emailConfiguration.SmtpServer,
                    SmtpEmail = this.emailConfiguration.SmtpEmail,
                    SmtpPassword = this.emailConfiguration.SmtpPassword,
                    SmtpPort = this.emailConfiguration.SmtpPort.ToString(),
                    DisplayName = this.emailConfiguration.DisplayName
                });
            }

            return smtpClient;
        }
    }
}
