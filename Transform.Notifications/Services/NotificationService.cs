﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Transform.Notifications.Models;

namespace Transform.Notifications.Services
{
    public class NotificationService : INotificationService
    {
        private readonly EmailService emailService;

        public NotificationService(EmailService emailService)
        {
            this.emailService = emailService;
        }


        public async Task SendEmailAsync(EmailModel model)
        {
            await this.emailService.SendAsync(model);
        }
    }
}
