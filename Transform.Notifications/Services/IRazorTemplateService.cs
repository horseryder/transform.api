﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Transform.Notifications.Services
{
    public interface IRazorTemplateService
    {
        Task<string> GetHtmlAsync<TModel>(string viewName, TModel model, ActionContext context = null);
    }
}
