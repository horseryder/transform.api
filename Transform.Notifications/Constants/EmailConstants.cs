﻿namespace Transform.Notifications.Constants
{
    public class EmailConstants
    {
        public const string EmailPattern = @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
                                          + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
                                           [0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
                                          + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
                                           [0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                                          + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";

        public const string DefaultSubject = "User Mail";
    }
}
