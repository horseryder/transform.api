﻿
namespace Transform.Notifications.Models
{
    using Transform.Models;


    public class EmailModel
    {
        public EmailModel()
        {
            this.AttachmentModel = new AttachmentsModel();
        }

        public string From { get; set; }

        public string FromMailId { get; set; }

        public string[] ToMailIds { get; set; }

        public string EmailTemplate { get; set; }

        public string Description { get; set; }

        public string Subject { get; set; }

        public AttachmentsModel AttachmentModel { get; set; }

        public SmtpSettings SmtpSettings { get; set; }

        public byte[] PdfAttachment { get; set; }

        public string[] ImageAttachment { get; set; }
    }
}
