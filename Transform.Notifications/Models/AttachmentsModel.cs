﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transform.Notifications.Models
{
    public class AttachmentsModel
    {
        public AttachmentsModel()
        {
            this.Attachments = new List<string>();
            this.MandatoryAttachments = new List<string>();
        }

        public bool IncludeAttachments { get; set; }

        // Attached to email only if IncludeAttachments is set
        public IEnumerable<string> Attachments { get; set; }

        // Attached to email no matter what
        public IEnumerable<string> MandatoryAttachments { get; set; }
    }
}
