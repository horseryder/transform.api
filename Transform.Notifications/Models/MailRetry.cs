﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Transform.Notifications.Models
{
    public class MailRetry
    {
        public int Count { get; set; }

        public int WaitTime { get; set; }
    }
}
